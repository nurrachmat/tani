-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 10, 2021 at 06:04 AM
-- Server version: 10.2.33-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maua9469_tani`
--
CREATE DATABASE IF NOT EXISTS `maua9469_tani` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `maua9469_tani`;

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `isi_text` longtext DEFAULT NULL,
  `isi_gambar` varchar(45) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `user_id_pengirim` int(11) NOT NULL,
  `user_id_penerima` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `nama_kategori` varchar(45) NOT NULL,
  `status` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `parent`, `nama_kategori`, `status`) VALUES
(1, NULL, 'Ternak Hidup', 'Y'),
(2, NULL, 'Pangan Asal Hewan', 'Y'),
(3, NULL, 'Hewan Kesayangan', 'Y'),
(4, NULL, 'Tanaman Hias', 'Y'),
(5, NULL, 'Pangan Nabati', 'Y'),
(6, 5, 'Beras/Jagung/Ketan', 'Y'),
(7, 5, 'Umbi', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_user`
--

CREATE TABLE `kategori_user` (
  `id` int(11) NOT NULL,
  `nama_kategori_user` varchar(45) DEFAULT NULL,
  `level_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori_user`
--

INSERT INTO `kategori_user` (`id`, `nama_kategori_user`, `level_user_id`) VALUES
(1, 'Admin', 1),
(2, 'Dinas Pertanian', 2),
(3, 'Peternak', 3),
(4, 'Petani', 3),
(5, 'Toko', 3),
(6, 'Praktik Dokter/Klinik/RS', 3),
(7, 'Dinas', 3),
(8, 'Pembeli', 4),
(9, 'Pelajar/Mahasiswa', 4),
(10, 'Institusi', 4);

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id` int(11) NOT NULL,
  `nama_kecamatan` varchar(100) NOT NULL,
  `kotakabupaten_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id`, `nama_kecamatan`, `kotakabupaten_id`) VALUES
(1, 'Kecamatan Kalidoni', 1),
(2, 'Kecamatan Seberang Ulu 1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kotakabupaten`
--

CREATE TABLE `kotakabupaten` (
  `id` int(11) NOT NULL,
  `nama_kotakabupaten` varchar(100) NOT NULL,
  `provinsi_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kotakabupaten`
--

INSERT INTO `kotakabupaten` (`id`, `nama_kotakabupaten`, `provinsi_id`) VALUES
(1, 'Kota Palembang', 1),
(2, 'Kota Prabumulih', 1),
(3, 'Kabupaten Banyuasin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `level_user`
--

CREATE TABLE `level_user` (
  `id` int(11) NOT NULL,
  `nama_level` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `level_user`
--

INSERT INTO `level_user` (`id`, `nama_level`) VALUES
(1, 'Admin'),
(2, 'Dinas Pertanian'),
(3, 'Host'),
(4, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `deskripsi` longtext NOT NULL,
  `harga` varchar(45) NOT NULL,
  `kondisi` char(1) NOT NULL,
  `status` char(1) NOT NULL,
  `created_at` date NOT NULL,
  `modified_at` date DEFAULT NULL,
  `favorit` char(1) DEFAULT NULL,
  `total_dilihat` varchar(45) DEFAULT NULL,
  `kategori_id` int(11) NOT NULL,
  `kecamatan_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `produk_foto`
--

CREATE TABLE `produk_foto` (
  `id` int(11) NOT NULL,
  `foto` varchar(45) NOT NULL,
  `featured_img` char(1) DEFAULT NULL,
  `produk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `id` int(11) NOT NULL,
  `nama_provinsi` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES
(1, 'Sumatera Selatan'),
(2, 'Lampung');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `hp` varchar(45) NOT NULL,
  `deskripsi` longtext NOT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `status` char(1) NOT NULL,
  `token` varchar(45) DEFAULT NULL,
  `created_at` date NOT NULL,
  `modified_at` date DEFAULT NULL,
  `kategori_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `hp`, `deskripsi`, `foto`, `status`, `token`, `created_at`, `modified_at`, `kategori_user_id`) VALUES
(1, 'admin@test.com', '7c222fb2927d828af22f592134e8932480637c0d', '081377976824', '', NULL, 'Y', NULL, '2021-02-10', NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`,`user_id_pengirim`),
  ADD KEY `fk_chat_user1_idx` (`user_id_pengirim`),
  ADD KEY `fk_chat_user2_idx` (`user_id_penerima`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_user`
--
ALTER TABLE `kategori_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_kategori_user_level_user_idx` (`level_user_id`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_kecamatan_kotakabupaten1_idx` (`kotakabupaten_id`);

--
-- Indexes for table `kotakabupaten`
--
ALTER TABLE `kotakabupaten`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_kotakabupaten_provinsi1_idx` (`provinsi_id`);

--
-- Indexes for table `level_user`
--
ALTER TABLE `level_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_produk_kategori1_idx` (`kategori_id`),
  ADD KEY `fk_produk_kecamatan1_idx` (`kecamatan_id`),
  ADD KEY `fk_produk_user1_idx` (`user_id`);

--
-- Indexes for table `produk_foto`
--
ALTER TABLE `produk_foto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_produk_foto_produk1_idx` (`produk_id`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_kategori_user1_idx` (`kategori_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kategori_user`
--
ALTER TABLE `kategori_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kotakabupaten`
--
ALTER TABLE `kotakabupaten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `level_user`
--
ALTER TABLE `level_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produk_foto`
--
ALTER TABLE `produk_foto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `fk_chat_user1` FOREIGN KEY (`user_id_pengirim`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_chat_user2` FOREIGN KEY (`user_id_penerima`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kategori_user`
--
ALTER TABLE `kategori_user`
  ADD CONSTRAINT `fk_kategori_user_level_user` FOREIGN KEY (`level_user_id`) REFERENCES `level_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD CONSTRAINT `fk_kecamatan_kotakabupaten1` FOREIGN KEY (`kotakabupaten_id`) REFERENCES `kotakabupaten` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kotakabupaten`
--
ALTER TABLE `kotakabupaten`
  ADD CONSTRAINT `fk_kotakabupaten_provinsi1` FOREIGN KEY (`provinsi_id`) REFERENCES `provinsi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `fk_produk_kategori1` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produk_kecamatan1` FOREIGN KEY (`kecamatan_id`) REFERENCES `kecamatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produk_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `produk_foto`
--
ALTER TABLE `produk_foto`
  ADD CONSTRAINT `fk_produk_foto_produk1` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_kategori_user1` FOREIGN KEY (`kategori_user_id`) REFERENCES `kategori_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
