<?php

class Kesehatanhewan_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*
     * Get kesehatan_hewan by id
     */
	function get_kesehatan_hewan($params)
	{
		return $this->db->get_where('kesehatan_hewan', $params)->row_array();
	}

	/*
     * Get all kesehatan_hewan
     */
	function get_all_kesehatan_hewan($params = false)
	{
		$this->db->select('kesehatan_hewan.*, kecamatan.nama_kecamatan');
		$this->db->join('kecamatan', 'kecamatan.id = kesehatan_hewan.kecamatan_id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('id', 'desc');
		return $this->db->get('kesehatan_hewan')->result_array();
	}

	/*
     * function to add new kesehatan_hewan
     */
	function add_kesehatan_hewan($params)
	{
		$this->db->insert('kesehatan_hewan', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update kesehatan_hewan
     */
	function update_kesehatan_hewan($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('kesehatan_hewan', $params);
	}

	/*
     * function to delete kesehatan_hewan
     */
	function delete_kesehatan_hewan($id)
	{
		return $this->db->delete('kesehatan_hewan', array('id' => $id));
	}
}
