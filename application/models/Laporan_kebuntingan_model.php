<?php

class Laporan_kebuntingan_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*
     * Get laporan_kebuntingan by id
     */
	function get_laporan_kebuntingan($params)
	{
		return $this->db->get_where('laporan_kebuntingan', $params)->row_array();
	}

	/*
     * Get all laporan_kebuntingan
     */
	function get_all_laporan_kebuntingan($params = false)
	{
		$this->db->select('laporan_kebuntingan.*');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('id', 'desc');
		return $this->db->get('laporan_kebuntingan')->result_array();
	}

	/*
     * function to add new laporan_kebuntingan
     */
	function add_laporan_kebuntingan($params)
	{
		$this->db->insert('laporan_kebuntingan', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update laporan_kebuntingan
     */
	function update_laporan_kebuntingan($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('laporan_kebuntingan', $params);
	}

	/*
     * function to delete laporan_kebuntingan
     */
	function delete_laporan_kebuntingan($id)
	{
		return $this->db->delete('laporan_kebuntingan', array('id' => $id));
	}
}
