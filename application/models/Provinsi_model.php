<?php

class Provinsi_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get provinsi by id
     */
    function get_provinsi($id)
    {
        return $this->db->get_where('provinsi', array('id' => $id))->row_array();
    }

    /*
     * Get all provinsi
     */
    function get_all_provinsi()
    {
        $this->db->order_by('nama_provinsi', 'asc');
        return $this->db->get('provinsi')->result_array();
    }

    /*
     * function to add new provinsi
     */
    function add_provinsi($params)
    {
        $this->db->insert('provinsi', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update provinsi
     */
    function update_provinsi($id, $params)
    {
        $this->db->where('id', $id);
        return $this->db->update('provinsi', $params);
    }

    /*
     * function to delete provinsi
     */
    function delete_provinsi($id)
    {
        return $this->db->delete('provinsi', array('id' => $id));
    }
}
