<?php

class Vaksinasijembrana_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*
     * Get vaksinasijembrana by id
     */
	function get_vaksinasijembrana($params)
	{
		return $this->db->get_where('vaksinasijembrana', $params)->row_array();
	}

	/*
     * Get all vaksinasijembrana
     */
	function get_all_vaksinasijembrana($params = false)
	{
		$this->db->select('vaksinasijembrana.*, kecamatan.nama_kecamatan');
		$this->db->join('kecamatan', 'kecamatan.id = vaksinasijembrana.kecamatan_id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('id', 'desc');
		return $this->db->get('vaksinasijembrana')->result_array();
	}

	/*
     * function to add new vaksinasijembrana
     */
	function add_vaksinasijembrana($params)
	{
		$this->db->insert('vaksinasijembrana', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update vaksinasijembrana
     */
	function update_vaksinasijembrana($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('vaksinasijembrana', $params);
	}

	/*
     * function to delete vaksinasijembrana
     */
	function delete_vaksinasijembrana($id)
	{
		return $this->db->delete('vaksinasijembrana', array('id' => $id));
	}
}
