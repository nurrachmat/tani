<?php

class Populasi_ternak_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*
     * Get populasi_ternak by id
     */
	function get_populasi_ternak($params)
	{
		return $this->db->get_where('populasi_ternak', $params)->row_array();
	}

	/*
     * Get all populasi_ternak
     */
	function get_all_populasi_ternak($params = false)
	{
		$this->db->select('populasi_ternak.*, kecamatan.nama_kecamatan');
		$this->db->join('kecamatan', 'kecamatan.id = populasi_ternak.kecamatan_id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('id', 'desc');
		return $this->db->get('populasi_ternak')->result_array();
	}

	/*
     * function to add new populasi_ternak
     */
	function add_populasi_ternak($params)
	{
		$this->db->insert('populasi_ternak', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update populasi_ternak
     */
	function update_populasi_ternak($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('populasi_ternak', $params);
	}

	/*
     * function to delete populasi_ternak
     */
	function delete_populasi_ternak($id)
	{
		return $this->db->delete('populasi_ternak', array('id' => $id));
	}
}
