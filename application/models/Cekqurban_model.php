<?php

class Cekqurban_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*
     * Get periksa_qurban by id
     */
	function get_periksa_qurban($params)
	{
		return $this->db->get_where('periksa_qurban', $params)->row_array();
	}

	/*
     * Get all periksa_qurban
     */
	function get_all_periksa_qurban($params = false)
	{
		$this->db->select('periksa_qurban.*, kecamatan.nama_kecamatan');
		$this->db->join('kecamatan', 'kecamatan.id = periksa_qurban.kecamatan_id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('id', 'desc');
		return $this->db->get('periksa_qurban')->result_array();
	}

	/*
     * function to add new periksa_qurban
     */
	function add_periksa_qurban($params)
	{
		$this->db->insert('periksa_qurban', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update periksa_qurban
     */
	function update_periksa_qurban($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('periksa_qurban', $params);
	}

	/*
     * function to delete periksa_qurban
     */
	function delete_periksa_qurban($id)
	{
		return $this->db->delete('periksa_qurban', array('id' => $id));
	}
}
