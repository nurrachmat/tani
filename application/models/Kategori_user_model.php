<?php

class Kategori_user_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get kategori_user by id
     */
    function get_kategori_user($id)
    {
        return $this->db->get_where('kategori_user', array('id' => $id))->row_array();
    }

    /*
     * Get all kategori_user
     */
    function get_all_kategori_user()
    {
        $this->db->select('k.*, p.nama_level');
        $this->db->from('kategori_user k');
        $this->db->join('level_user p', 'k.level_user_id = p.id');
        $this->db->order_by('k.id', 'asc');
        return $this->db->get()->result_array();
    }

    function find_kategori_user($params)
    {
        $this->db->select('*');
        $this->db->from('kategori_user');
        $this->db->where($params);
        return $this->db->get()->result_array();
    }

    /*
     * function to add new kategori_user
     */
    function add_kategori_user($params)
    {
        $this->db->insert('kategori_user', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update kategori_user
     */
    function update_kategori_user($id, $params)
    {
        $this->db->where('id', $id);
        return $this->db->update('kategori_user', $params);
    }

    /*
     * function to delete kategori_user
     */
    function delete_kategori_user($id)
    {
        return $this->db->delete('kategori_user', array('id' => $id));
    }
}
