<?php

class Produk_fav_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function findRow_fav($params)
	{
		return $this->db->get_where('produk_favorit', $params)->row_array();
	}

	function find_favorit($params = false)
	{
		$this->db->select("p.*, k.nama_kategori, kc.nama_kecamatan, kk.nama_kotakabupaten, pr.nama_provinsi, u.nama as nama_penjual, u.hp, pf.foto, pf.featured_img, FORMAT(p.harga, 2) as harga_rupiah");
		$this->db->from('produk_favorit pdf');
		$this->db->join('produk p', 'pdf.produk_id = p.id');
		$this->db->join('produk_foto pf', 'pf.produk_id = p.id', 'left');
		$this->db->join('kategori k', 'k.id = p.kategori_id');
		$this->db->join('kecamatan kc', 'kc.id = p.kecamatan_id');
		$this->db->join('kotakabupaten kk', 'kk.id = kc.kotakabupaten_id');
		$this->db->join('provinsi pr', 'pr.id = kk.provinsi_id');
		$this->db->join('user u', 'u.id = p.user_id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->group_by('p.id');
		$this->db->order_by('p.id', 'asc');
		return $this->db->get();
	}

	function add_fav($val)
	{
		$this->db->insert('produk_favorit', $val);
		return $this->db->insert_id();
	}
	/*
     * function to update fav
     */
	function update_fav($id, $val)
	{
		$this->db->where('id', $id);
		return $this->db->update('produk_favorit', $val);
	}

	/*
     * function to delete fav
     */
	function delete_fav($params)
	{
		return $this->db->delete('produk_favorit', $params);
	}
}
