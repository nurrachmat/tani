<?php

class Chat_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function find_chat($params = false, $params2 = false)
	{
		$this->db->select('c.*, p.nama_produk, p.user_id as user_id_produk');
		$this->db->from('chat c');
		$this->db->join('produk p', 'c.produk_id = p.id');
		if ($params != false) {
			$this->db->where($params);
		}
		if ($params2 != false) {
			$this->db->or_where($params2);
		}
		// $this->db->group_by('c.produk_id');
		$this->db->order_by('c.tanggal', 'desc');
		return $this->db->get()->result_array();
	}

	function findRow_chat($params = false)
	{
		$this->db->select('c.*, b.nama, p.nama_produk, p.user_id as user_id_produk');
		$this->db->from('chat c');
		$this->db->join('user b', 'b.id = c.user_id_pembeli');
		$this->db->join('produk p', 'c.produk_id = p.id');
		if ($params != false) {
			$this->db->where($params);
		}
		// $this->db->group_by('c.produk_id');
		$this->db->order_by('c.tanggal', 'desc');
		return $this->db->get()->row_array();
	}

	function find_chat_detail($params = false, $params2 = false)
	{
		$this->db->select('cd.*');
		$this->db->from('chat c');
		// $this->db->join('produk p', 'c.produk_id = p.id');
		$this->db->join('chat_detail cd', 'cd.chat_id = c.id');
		if ($params != false) {
			$this->db->where($params);
		}
		if ($params2 != false) {
			$this->db->or_where($params2);
		}
		$this->db->order_by('c.tanggal', 'desc');
		return $this->db->get()->result_array();
	}

	/*
     * Get chat by id
     */
	function get_chat($id)
	{
		return $this->db->get_where('chat', array('id' => $id))->row_array();
	}


	/*
     * function to add new chat
     */
	function add_chat($params)
	{
		$this->db->insert('chat', $params);
		return $this->db->insert_id();
	}

	function add_chat_detail($params)
	{
		$this->db->insert('chat_detail', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update chat
     */
	function update_chat($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('chat', $params);
	}

	/*
     * function to delete chat
     */
	function delete_chat($id)
	{
		return $this->db->delete('chat', array('id' => $id));
	}
}
