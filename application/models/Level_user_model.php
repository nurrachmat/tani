<?php

class Level_user_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get level_user by id
     */
    function get_level_user($id)
    {
        return $this->db->get_where('level_user', array('id' => $id))->row_array();
    }

    /*
     * Get all level_user
     */
    function get_all_level_user()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('level_user')->result_array();
    }

    /*
     * function to add new level_user
     */
    // function add_level_user($params)
    // {
    //     $this->db->insert('level_user', $params);
    //     return $this->db->insert_id();
    // }

    /*
     * function to update level_user
     */
    function update_level_user($id, $params)
    {
        $this->db->where('id', $id);
        return $this->db->update('level_user', $params);
    }

    /*
     * function to delete level_user
     */
    // function delete_level_user($id)
    // {
    //     return $this->db->delete('level_user', array('id' => $id));
    // }
}
