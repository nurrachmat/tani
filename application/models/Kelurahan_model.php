<?php

class Kelurahan_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*
     * Get kelurahan by id
     */
	function get_kelurahan($id)
	{
		return $this->db->get_where('kelurahan', array('id' => $id))->row_array();
	}

	/*
     * Get all kelurahan
     */
	function get_all_kelurahan($params = false)
	{
		$this->db->select('k.*, kk.nama_kecamatan, p.nama_kotakabupaten, pv.nama_provinsi');
		$this->db->from('kelurahan k');
		$this->db->join('kecamatan kk', 'k.kecamatan_id = kk.id');
		$this->db->join('kotakabupaten p', 'kk.kotakabupaten_id = p.id');
		$this->db->join('provinsi pv', 'p.provinsi_id = pv.id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('k.id', 'asc');
		return $this->db->get()->result_array();
	}

	/*
     * function to add new kelurahan
     */
	function add_kelurahan($params)
	{
		$this->db->insert('kelurahan', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update kelurahan
     */
	function update_kelurahan($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('kelurahan', $params);
	}

	/*
     * function to delete kelurahan
     */
	function delete_kelurahan($id)
	{
		return $this->db->delete('kelurahan', array('id' => $id));
	}
}
