<?php

class Kotakabupaten_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get kotakabupaten by id
     */
    function get_kotakabupaten($id)
    {
        return $this->db->get_where('kotakabupaten', array('id' => $id))->row_array();
    }

    /*
     * Get all kotakabupaten
     */
    function get_all_kotakabupaten($params = false)
    {
        $this->db->select('k.*, p.nama_provinsi');
        $this->db->from('kotakabupaten k');
        $this->db->join('provinsi p', 'k.provinsi_id = p.id');
        if ($params != false) {
            $this->db->where($params);
        }
        $this->db->order_by('k.id', 'asc');
        return $this->db->get()->result_array();
    }

    /*
     * function to add new kotakabupaten
     */
    function add_kotakabupaten($params)
    {
        $this->db->insert('kotakabupaten', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update kotakabupaten
     */
    function update_kotakabupaten($id, $params)
    {
        $this->db->where('id', $id);
        return $this->db->update('kotakabupaten', $params);
    }

    /*
     * function to delete kotakabupaten
     */
    function delete_kotakabupaten($id)
    {
        return $this->db->delete('kotakabupaten', array('id' => $id));
    }
}
