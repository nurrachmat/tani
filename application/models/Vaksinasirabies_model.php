<?php

class vaksinasirabies_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*
     * Get vaksinasirabies by id
     */
	function get_vaksinasirabies($params)
	{
		return $this->db->get_where('vaksinasirabies', $params)->row_array();
	}

	/*
     * Get all vaksinasirabies
     */
	function get_all_vaksinasirabies($params = false)
	{
		$this->db->select('vaksinasirabies.*, kecamatan.nama_kecamatan');
		$this->db->join('kecamatan', 'kecamatan.id = vaksinasirabies.kecamatan_id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('id', 'desc');
		return $this->db->get('vaksinasirabies')->result_array();
	}

	/*
     * function to add new vaksinasirabies
     */
	function add_vaksinasirabies($params)
	{
		$this->db->insert('vaksinasirabies', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update vaksinasirabies
     */
	function update_vaksinasirabies($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('vaksinasirabies', $params);
	}

	/*
     * function to delete vaksinasirabies
     */
	function delete_vaksinasirabies($id)
	{
		return $this->db->delete('vaksinasirabies', array('id' => $id));
	}
}
