<?php

class Kasus_rabies_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*
     * Get kasus_rabies by id
     */
	function get_kasus_rabies($params)
	{
		return $this->db->get_where('kasus_rabies', $params)->row_array();
	}

	/*
     * Get all kasus_rabies
     */
	function get_all_kasus_rabies($params = false)
	{
		$this->db->select('kasus_rabies.*, kecamatan.nama_kecamatan');
		$this->db->join('kecamatan', 'kecamatan.id = kasus_rabies.kecamatan_id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('id', 'desc');
		return $this->db->get('kasus_rabies')->result_array();
	}

	/*
     * function to add new kasus_rabies
     */
	function add_kasus_rabies($params)
	{
		$this->db->insert('kasus_rabies', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update kasus_rabies
     */
	function update_kasus_rabies($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('kasus_rabies', $params);
	}

	/*
     * function to delete kasus_rabies
     */
	function delete_kasus_rabies($id)
	{
		return $this->db->delete('kasus_rabies', array('id' => $id));
	}
}
