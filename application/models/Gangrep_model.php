<?php

class Gangrep_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*
     * Get gangrep by id
     */
	function get_gangrep($params)
	{
		return $this->db->get_where('gangrep', $params)->row_array();
	}

	/*
     * Get all gangrep
     */
	function get_all_gangrep($params = false)
	{
		$this->db->select('gangrep.*, kecamatan.nama_kecamatan');
		$this->db->join('kecamatan', 'kecamatan.id = gangrep.kecamatan_id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('id', 'desc');
		return $this->db->get('gangrep')->result_array();
	}

	/*
     * function to add new gangrep
     */
	function add_gangrep($params)
	{
		$this->db->insert('gangrep', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update gangrep
     */
	function update_gangrep($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('gangrep', $params);
	}

	/*
     * function to delete gangrep
     */
	function delete_gangrep($id)
	{
		return $this->db->delete('gangrep', array('id' => $id));
	}
}
