<?php

class Users_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*
     * Get users by id
     */
	function get_users($id)
	{
		return $this->db->get_where('user', array('id' => $id))->row_array();
	}

	function get_total_users()
	{
		$where = "";
		$array_where = "";
		if (user_data('level') == 2) {
			$where = "WHERE uss.kotakabupaten_id = ?";
			$array_where = user_data('kotakabupaten_id');
		}
		return $this->db->query("SELECT 
        sum(case when level = 1 then 1 else 0 end) as admin,
        sum(case when level = 2 then 1 else 0 end) as dinas,
        sum(case when level = 3 then 1 else 0 end) as host,
        sum(case when level = 4 then 1 else 0 end) as user
        FROM users u
        JOIN kategori_user ku
        ON u.kategori_user_id = ku.id " . $where, array($array_where))->row_array();
	}

	function find_simple_user($params)
	{
		$this->db->select('u.id, u.email, u.nama, u.hp, u.kategori_user_id, u.token, u.reg_id, u.foto, u.deskripsi, u.created_at, ku.level_user_id, ku.nama_kategori_user');
		$this->db->from('user u');
		$this->db->join('kategori_user ku', 'u.kategori_user_id=ku.id');
		$this->db->where($params);
		return $this->db->get()->row_array();
	}

	function find_user($params)
	{
		$this->db->select('u.id, u.email, u.nama,u.hp, ku.level_user_id, lu.nama_level, u.kategori_user_id, ku.nama_kategori_user, u.token, u.reg_id, u.foto, u.deskripsi, u.created_at');
		$this->db->from('user u');
		$this->db->join('kategori_user ku', 'ku.id=u.kategori_user_id');
		$this->db->join('level_user lu', 'lu.id=ku.level_user_id');
		$this->db->where($params);
		return $this->db->get()->row_array();
	}

	/*
     * Get all users
     */
	function get_all_users($level, $limit = null)
	{
		$this->db->select('u.*, k.nama_kategori_user');
		$this->db->from('user u');
		$this->db->join('kategori_user k', 'u.kategori_user_id = k.id');
		$this->db->order_by('k.id', 'asc');
		return $this->db->get()->result_array();
	}

	/*
     * function to add new users
     */
	function add_users($params)
	{
		$this->db->insert('user', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update users
     */
	function update_users($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('user', $params);
	}

	/*
     * function to update users
     */
	function update_users_parameter($params, $val)
	{
		$this->db->where($params);
		return $this->db->update('user', $val);
	}

	/*
     * function to delete users
     */
	function delete_users($id)
	{
		return $this->db->delete('user', array('id' => $id));
	}
}
