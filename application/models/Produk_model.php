<?php

class Produk_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function find_produk($params = false)
	{
		$this->db->select("p.*, k.nama_kategori, kc.nama_kecamatan, kk.nama_kotakabupaten, pr.nama_provinsi, u.nama as nama_penjual, u.hp, pf.foto, pf.featured_img, FORMAT(p.harga, 2) as harga_rupiah");
		$this->db->from('produk p');
		$this->db->join('produk_foto pf', 'pf.produk_id = p.id', 'left');
		$this->db->join('kategori k', 'k.id = p.kategori_id');
		$this->db->join('kecamatan kc', 'kc.id = p.kecamatan_id');
		$this->db->join('kotakabupaten kk', 'kk.id = kc.kotakabupaten_id');
		$this->db->join('provinsi pr', 'pr.id = kk.provinsi_id');
		$this->db->join('user u', 'u.id = p.user_id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->group_by('p.id');
		$this->db->order_by('p.id', 'asc');
		return $this->db->get();
	}

	function search_produk($keyword = false)
	{
		$this->db->select("p.*, k.nama_kategori, kc.nama_kecamatan, kk.nama_kotakabupaten, pr.nama_provinsi, u.nama as nama_penjual, u.hp, pf.foto, pf.featured_img, FORMAT(p.harga, 2) as harga_rupiah");
		$this->db->from('produk p');
		$this->db->join('produk_foto pf', 'pf.produk_id = p.id', 'left');
		$this->db->join('kategori k', 'k.id = p.kategori_id');
		$this->db->join('kecamatan kc', 'kc.id = p.kecamatan_id');
		$this->db->join('kotakabupaten kk', 'kk.id = kc.kotakabupaten_id');
		$this->db->join('provinsi pr', 'pr.id = kk.provinsi_id');
		$this->db->join('user u', 'u.id = p.user_id');
		if ($keyword != false) {
			$this->db->like('p.nama_produk', $keyword);
		}
		$this->db->group_by('p.id');
		$this->db->order_by('p.nama_produk', 'asc');
		return $this->db->get();
	}

	function find_produk_foto($params = false)
	{
		$this->db->select("p.*");
		$this->db->from('produk_foto p');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('p.id', 'asc');
		return $this->db->get()->result_array();
	}

	function getRow_produk($params = false)
	{
		$this->db->select('p.*, k.nama_kategori, kc.nama_kecamatan, kk.nama_kotakabupaten, pr.nama_provinsi, u.nama as nama_penjual, u.hp, produk_foto.foto');
		$this->db->from('produk p');
		$this->db->join("produk_foto", "produk_foto.produk_id = p.id AND produk_foto.featured_img = 'Y'");
		$this->db->join('kategori k', 'k.id = p.kategori_id');
		$this->db->join('kecamatan kc', 'kc.id = p.kecamatan_id');
		$this->db->join('kotakabupaten kk', 'kk.id = kc.kotakabupaten_id');
		$this->db->join('provinsi pr', 'pr.id = kk.provinsi_id');
		$this->db->join('user u', 'u.id = p.user_id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('p.id', 'asc');
		return $this->db->get()->row_array();
	}

	/*
     * Get produk by id
     */
	function get_produk($id)
	{
		$this->db->select('produk.*, user.nama');
		$this->db->join('user', 'user.id = produk.user_id');
		return $this->db->get_where('produk', array('produk.id' => $id))->row_array();
	}

	function get_produk_md5($id)
	{
		$this->db->select('produk.*, user.nama');
		$this->db->join('user', 'user.id = produk.user_id');
		return $this->db->get_where('produk', array('md5(produk.id)' => $id))->row_array();
	}

	/*
     * Get all produk
     */
	function get_all_produk($params = false)
	{
		$this->db->select('k.*, p.nama_produk as nama_produk_parent');
		$this->db->from('produk p');
		$this->db->join('kategori k', 'k.id = p.kategori_id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('p.id', 'asc');
		return $this->db->get()->result_array();
	}

	/*
     * function to add new produk
     */
	function add_produk($params)
	{
		$this->db->insert('produk', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update produk
     */
	function update_produk($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('produk', $params);
	}

	function getRow_produk_foto($params = false)
	{
		$this->db->select("*");
		$this->db->from('produk_foto');
		if ($params != false) {
			$this->db->where($params);
		}
		return $this->db->get()->row_array();
	}

	/*
     * function to add new foto produk
     */
	function add_foto_produk($params)
	{
		$this->db->insert('produk_foto', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update produk
     */
	function update_produk_foto($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('produk_foto', $params);
	}

	function update_fav_produk($id, $math = 'add')
	{
		$this->db->trans_begin();
		if ($math == 'add') {
			$Q = "UPDATE produk SET favorit = favorit+1 WHERE id = ? ";
		} else {
			$Q = "UPDATE produk SET favorit = favorit-1 WHERE id = ? ";
		}
		$this->db->query($Q, array($id));
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return true;
		}
	}

	/*
     * function to delete produk
     */
	function delete_produk($id)
	{
		return $this->db->delete('produk', array('id' => $id));
	}

	function getFoto_produk($params)
	{
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('foto', 'DESC');
		return $this->db->get('produk_foto')->result_array();
	}

	/*
     * function to add new produk foto
     */
	function add_produk_foto($params)
	{
		$this->db->insert('produk_foto', $params);
		return $this->db->insert_id();
	}

	/*
     * function to add new favorit
     */
	// function add_favorit($params)
	// {
	// 	$this->db->insert('produk_favorit', $params);
	// 	return $this->db->insert_id();
	// }

	/*
     * function to delete produk
     */
	// function delete_favorit($params)
	// {
	// 	return $this->db->delete('produk_favorit', $params);
	// }
}
