<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Favorit_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	function get_all_produk_count($params2 = array())
	{
		$this->db->from('produk_favorit');
		$this->db->join("produk", "produk_favorit.produk_id = produk.id");
		$this->db->join("produk_foto", "produk_foto.produk_id = produk.id AND produk_foto.featured_img = 'Y'");
		$this->db->join('kecamatan kc', 'kc.id = produk.kecamatan_id');
		if (!empty($_GET['serachproduct']) && $_GET['serachproduct'] != "") {
			$this->db->like("nama_produk", $_GET['serachproduct'], 'BOTH');
		}

		if (!empty($_GET['kategori']) && $_GET['kategori'] != "") {
			$this->db->like("kategori_id", $_GET['kategori']);
		}

		if (isset($params2) && !empty($params2)) {
			$this->db->where($params2);
		}

		return $this->db->count_all_results();
	}

	function get_all_produk($params = array(), $params2 = array())
	{
		$this->db->select("produk.*, produk_foto.foto, kc.nama_kecamatan");
		$this->db->join("produk", "produk_favorit.produk_id = produk.id");
		$this->db->join("produk_foto", "produk_foto.produk_id = produk.id AND produk_foto.featured_img = 'Y'");
		$this->db->join('kecamatan kc', 'kc.id = produk.kecamatan_id');

		if (isset($params2) && !empty($params2)) {
			$this->db->where($params2);
		}

		$this->db->order_by('nama_produk', 'ASC');

		if (!empty($_GET['kategori']) && $_GET['kategori'] != "") {
			$this->db->like("kategori_id", $_GET['kategori']);
		}

		if (!empty($_GET['serachproduct']) && $_GET['serachproduct'] != "") {
			$this->db->like("nama_produk", $_GET['serachproduct'], 'BOTH');
			$this->db->or_like("deskripsi", $_GET['serachproduct'], 'BOTH');
		}

		if (isset($params) && !empty($params)) {
			$this->db->limit($params['limit'], $params['offset']);
		}

		return $this->db->get('produk_favorit')->result_array();
	}
}

/* End of file Favorit.php */
/* Location: ./application/models/Favorit_model.php */
