<?php

class Laporan_kelahiran_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*
     * Get laporan_kelahiran by id
     */
	function get_laporan_kelahiran($params)
	{
		return $this->db->get_where('laporan_kelahiran', $params)->row_array();
	}

	/*
     * Get all laporan_kelahiran
     */
	function get_all_laporan_kelahiran($params = false)
	{
		$this->db->select('laporan_kelahiran.*');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('id', 'desc');
		return $this->db->get('laporan_kelahiran')->result_array();
	}

	/*
     * function to add new laporan_kelahiran
     */
	function add_laporan_kelahiran($params)
	{
		$this->db->insert('laporan_kelahiran', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update laporan_kelahiran
     */
	function update_laporan_kelahiran($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('laporan_kelahiran', $params);
	}

	/*
     * function to delete laporan_kelahiran
     */
	function delete_laporan_kelahiran($id)
	{
		return $this->db->delete('laporan_kelahiran', array('id' => $id));
	}
}
