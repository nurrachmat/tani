<?php

class Kategori_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function find_kategori($params = false)
	{
		$this->db->select('k.*, p.nama_kategori as nama_kategori_parent');
		$this->db->from('kategori k');
		$this->db->join('kategori p', 'k.parent = p.id', 'left');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('k.id', 'asc');
		return $this->db->get()->result_array();
	}

	/*
     * Get kategori by id
     */
	function get_kategori($id)
	{
		return $this->db->get_where('kategori', array('id' => $id))->row_array();
	}

	/*
     * Get all kategori
     */
	function get_all_kategori($params = false)
	{
		$this->db->select('k.*, p.nama_kategori as nama_kategori_parent');
		$this->db->from('kategori k');
		$this->db->join('kategori p', 'k.parent = p.id', 'left');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('k.id', 'asc');
		return $this->db->get()->result_array();
	}

	/*
     * function to add new kategori
     */
	function add_kategori($params)
	{
		$this->db->insert('kategori', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update kategori
     */
	function update_kategori($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('kategori', $params);
	}

	/*
     * function to delete kategori
     */
	function delete_kategori($id)
	{
		return $this->db->delete('kategori', array('id' => $id));
	}
}
