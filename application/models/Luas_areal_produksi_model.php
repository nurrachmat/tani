<?php

class Luas_areal_produksi_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/*
     * Get luas_areal_produksi by id
     */
	function get($params)
	{
		return $this->db->get_where('luas_areal_produksi', $params)->row_array();
	}

	/*
     * Get all luas_areal_produksi
     */
	function get_all($params = false)
	{
		$this->db->select('luas_areal_produksi.*, kelurahan.nama_kelurahan');
		$this->db->from('luas_areal_produksi');
		$this->db->join('kelurahan', 'kelurahan.id = luas_areal_produksi.kelurahan_id');
		$this->db->join('kecamatan', 'kecamatan.id = kelurahan.kecamatan_id');
		$this->db->join('kotakabupaten', 'kotakabupaten.id = kecamatan.kotakabupaten_id');
		if ($params != false) {
			$this->db->where($params);
		}
		$this->db->order_by('luas_areal_produksi.id', 'desc');
		return $this->db->get()->result_array();
	}

	function get_all_angka_tetap($tahun = false, $kotakabupaten_id = false)
	{
		return $this->db->query("SELECT lap.tahun, lap.komoditi, sum(lap.tbm) as tbm, sum(lap.tm) as tm, sum(lap.tt_tr) as tt_tr, sum(tbm+tm+tt_tr) as jumlah, sum(lap.produksi) as produksi, sum(lap.produktifitas) as produktifitas, sum(lap.krt) as krt, kk.nama_kotakabupaten 
		FROM luas_areal_produksi lap 
		JOIN kelurahan kel ON kel.id = lap.kelurahan_id
		JOIN kecamatan kec ON kec.id = kel.kecamatan_id
		JOIN kotakabupaten kk ON kk.id = kec.kotakabupaten_id
		WHERE lap.tahun=? and kk.id=?
		GROUP BY lap.komoditi, lap.tahun, kk.id", array($tahun, $kotakabupaten_id));
	}

	/*
     * function to add new luas_areal_produksi
     */
	function add($params)
	{
		$this->db->insert('luas_areal_produksi', $params);
		return $this->db->insert_id();
	}

	/*
     * function to update luas_areal_produksi
     */
	function update($id, $params)
	{
		$this->db->where('id', $id);
		return $this->db->update('luas_areal_produksi', $params);
	}

	/*
     * function to delete luas_areal_produksi
     */
	function delete($id)
	{
		return $this->db->delete('luas_areal_produksi', array('id' => $id));
	}
}
