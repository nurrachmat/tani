<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('authenticate')) {

	/**
	 * Cek status login User, jika tidak ada sesi login, maka keluarkan
	 */
	function authenticate()
	{
		$CI = &get_instance();
		//$CI->load->model('Users_model');
		$segmen = $CI->uri->segment(1, NULL);

		if ($segmen <> "auth") {
			if (is_null($CI->session->userdata('logged_in')) || $CI->session->userdata('logged_in') === false) {
				m_error('Anda harus login untuk mengakses halaman ini');
				redirect('auth/login', 'refresh');
			} else {
				has_privileges();
			}
		} else {
			// has_privileges();
		}
	}
}

function alert_success($msg, $dismis = FALSE)
{
	if ($msg != "") {
		if ($dismis)
			$ex = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
		else
			$ex = "";

		echo "<div class=\"alert alert-success " . (($dismis) ? 'alert-dismissible' : '') . "\" role=\"alert\">
                {$ex}
                    <i class='fa fa-info-circle'></i> {$msg}
                </div>";
	}
}

function alert_danger($msg, $dismis = FALSE)
{
	if ($msg != "") {
		if ($dismis)
			$ex = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
		else
			$ex = "";

		echo "<div class=\"alert alert-danger " . (($dismis) ? 'alert-dismissible' : '') . "\" role=\"alert\">
                {$ex}
                    <i class='fa fa-info-circle'></i> {$msg}
                </div>";
	}
}

if (!function_exists('m_success')) {

	/**
	 * Set flash data for success messages
	 * @param string $msg
	 */
	function m_success($msg = '')
	{
		$CI = &get_instance();
		if ($msg != '')
			$CI->session->set_flashdata('m_success', $msg);
	}
}

if (!function_exists('m_error')) {

	/**
	 * Set flash data for error messages
	 * @param string $msg
	 */
	function m_error($msg = '')
	{
		$CI = &get_instance();
		if ($msg != '')
			$CI->session->set_flashdata('m_error', $msg);
	}
}


if (!function_exists('status_softdelete')) {
	function status_softdelete($status = null)
	{
		if ($status == 'Y') {
			return "<label class='label label-danger'>Tidak</label>";
		} else {
			return "<label class='label label-success'>Aktif</label>";
		}
	}
}

if (!function_exists('level')) {
	function level($level = null)
	{
		if ($level == 1) {
			return "<label class='label label-danger'>Admin</label>";
		} else if ($level == 2) {
			return "<label class='label label-warning'>Dinas</label>";
		} else if ($level == 3) {
			return "<label class='label label-success'>Sekolah</label>";
		} else if ($level == 4) {
			return "<label class='label label-primary'>Alumni</label>";
		} else if ($level == 5) {
			return "<label class='label label-info'>Siswa</label>";
		}
	}
}


if (!function_exists('konversi_bulan')) {
	function konversi_bulan($bulan = null)
	{
		$array = [
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		];
		return $array[$bulan];
	}
}

if (!function_exists('konversi_tanggal')) {
	function konversi_tanggal($tanggal = null)
	{
		return substr($tanggal, 8, 2) . " " . konversi_bulan(substr($tanggal, 5, 2)) . " " . substr($tanggal, 0, 4);
	}
}

if (!function_exists('konversi_tanggal_minus')) {
	function konversi_tanggal_minus($tanggal = null)
	{
		return substr($tanggal, 8, 2) . "-" . substr($tanggal, 5, 2) . "-" . substr($tanggal, 0, 4);
	}
}

if (!function_exists('status_aktif')) {
	function status_aktif($status = null)
	{
		return ($status == 'Y' ? "<label class='badge badge-success'>Aktif</label>" : "<label class='badge badge-danger'>Tidak Aktif</label>");
	}
}


if (!function_exists('status_baca')) {
	function status_baca($status = null)
	{
		if ($status == 'Y') {
			return "<label class='badge badge-success'>Dibaca</label>";
		} else {
			return "<label class='badge badge-danger'>Belum</label>";
		}
	}
}

function user_data($key)
{
	$CI = &get_instance();
	return $CI->session->userdata($key);
}

if (!function_exists('get_tanggal')) {
	function get_tanggal($val = null)
	{
		return substr($val, 0, 10);
	}
}

if (!function_exists('get_jam')) {
	function get_jam($val = null)
	{
		return substr($val, 11, 8);
	}
}

if (!function_exists('get_module')) {
	/**
	 * Mendapatkan module yang sedang diakses oleh pengguna
	 * @return mixed
	 */
	function get_module()
	{
		$CI = &get_instance();
		return is_null($CI->session->userdata('module')) ? '' : $CI->session->userdata('module');
	}
}

if (!function_exists('has_privileges')) {

	/**
	 * Cek privileges untuk mengakses halaman tertentu
	 */
	function has_privileges()
	{
		$CI = &get_instance();
		$priv = $CI->session->userdata('level');
		$mod = $CI->uri->segment(1, NULL);
		$has_p = false;
		if ($priv == '1') {           //ADMIN
			$has_p =  ($mod == 'dinas');
		} elseif ($priv == '2') {           //DINAS
			$has_p =  ($mod == 'dinas');
		} elseif ($priv == '3') {           //HOST
			$has_p =  ($mod == 'public');
		} elseif ($priv == '4') {           //USER
			$has_p =  ($mod == 'public');
		}

		if ($has_p == false) redirect(get_module(), 'refresh');
	}
}

if (!function_exists('has_privileges_admin')) {

	/**
	 * Cek privileges admin untuk mengakses fitur tertentu seperti CRUD yang tidak diberikan akses ke user lainnya
	 */
	function has_privileges_admin()
	{
		$CI = &get_instance();
		$priv = $CI->session->userdata('level');
		$mod = $CI->uri->segment(1, NULL);
		$has_p = false;
		if (($priv == '1') or ($priv == '2')) {           //ADMIN & DINAS
			$has_p =  ($mod == 'dinas');
		}

		if ($has_p == false) {
			m_error('Anda tidak memiliki akses ke halaman sebelumnya');
			redirect(get_module() . "/dashboard", 'refresh');
		};
	}
}

if (!function_exists('has_privileges_public')) {

	/**
	 * Cek privileges admin untuk mengakses fitur tertentu seperti CRUD yang tidak diberikan akses ke user lainnya
	 */
	function has_privileges_public()
	{
		$CI = &get_instance();
		$priv = $CI->session->userdata('level');
		$mod = $CI->uri->segment(1, NULL);
		$has_p = false;
		if (($priv == '3') or ($priv == '4')) {           //HOST & USER
			$has_p =  ($mod == 'public');
		}

		if ($has_p == false) {
			m_error('Anda tidak memiliki akses ke halaman sebelumnya');
			redirect(get_module() . "/dashboard", 'refresh');
		};
	}
}

if (!function_exists('get_total_users')) {
	function get_total_users()
	{
		$CI = &get_instance();
		$CI->load->model('Users_model');
		return $CI->Users_model->get_total_users();
	}
}

if (!function_exists('format_angka')) {
	function format_angka($val = 0)
	{
		return number_format($val, 0, ',', '.');
	}
}

if (!function_exists('format_tgl')) {
	function format_tgl($tgl, $format = 'Y-m-d')
	{
		return date($format, strtotime($tgl));
	}
}

if (!function_exists('auth_host')) {
	function auth_host()
	{
		$CI = &get_instance();
		$segmen = $CI->uri->segment(1, NULL);

		if (is_null($CI->session->userdata('logged_in')) || $CI->session->userdata('logged_in') === false) {
			if ($segmen <> 'login') {
				m_error('Anda harus login untuk mengakses halaman ini');
				redirect('login', 'refresh');
			}
		} else {
			if ($segmen == 'login') {
				redirect('produk', 'refresh');
			}
		}
	}
}

if (!function_exists('kondisi_produk')) {
	function kondisi_produk($params = false)
	{
		$arr = array("N" => "Baru", "O" => "Bekas");
		if ($params != false) {
			return $arr[$params];
		} else {
			return $arr;
		}
	}
}
