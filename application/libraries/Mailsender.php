<?php
class mailsender
{
	public $ci      = null;
	public $subject = '';
	public $body    = '';
	public $mail_to = '';
	public $mail_cc = '';

	public function __construct()
	{
		$this->ci = &get_instance();  // call CI
	}

	// Pngumuman, Tugas, Broadcast Berita SIMPONI
	function set_vars($data = array())
	{
		if (is_array($data)) {
			isset($data['subject']) ? $this->subject = $data['subject'] : '';
			isset($data['body']) ? $this->body = $data['body'] : '';
			isset($data['mail_to']) ? $this->mail_to = $data['mail_to'] : '';
		}
	}

	function send_mail()
	{
		$this->ci->load->library('email');

		$subject = $this->subject;
		$pesan  = $this->body;
		$to     = $this->mail_to;

		$config['protocol'] = 'smtp';
		$config['smtp_host'] = EMAIL_HOST;
		$config['smtp_port'] = EMAIL_PORT;
		$config['smtp_timeout'] = '500';
		$config['smtp_user'] = EMAIL_ADDRESS_SENDER;
		$config['smtp_pass'] = EMAIL_PASSWORD_SENDER;
		$config['newline'] = "\r\n";
		$config['validation'] = TRUE; // bool whether to validate email or not

		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		///$this->ci->email->clear();
		$this->ci->email->initialize($config);
		$this->ci->email->from(EMAIL_ADDRESS_SENDER, EMAIL_NAME_SENDER);
		// $this->ci->email->reply_to('simponi@mdp.ac.id', 'SIMPONI');
		$this->ci->email->to($to);
		if ($this->mail_cc != '') {
			$this->ci->email->cc($this->mail_cc);
		}

		$this->ci->email->subject(APP_TITLE . " " . APP_OWNER . " - " . $subject);
		$this->ci->email->message($pesan);

		if ($this->ci->email->send()) {
			//return true;
			return true;
		} else {
			return false;
		}
	}
}
