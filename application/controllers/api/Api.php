<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        // Load the user model
        $this->load->model('Users_model');
        $this->load->model('Kategori_user_model');
        $this->load->model('Kategori_model');
        $this->load->model('Produk_model');
        $this->load->model('Provinsi_model');
        $this->load->model('Kotakabupaten_model');
        $this->load->model('Kecamatan_model');
    }

    public function provinsi()
    {
        $data['provinsi'] = $this->Provinsi_model->get_all_provinsi();
        $response = array();
        $response['success'] = TRUE;
        $response['message'] = 'Data provinsi';
        $response['data'] = $data['provinsi'];
        echo json_encode($response);
    }

    public function kotakabupaten()
    {
        // Get the post data
        $provinsi_id = $this->input->post('provinsi_id', TRUE);

        $params = array(
            'k.provinsi_id' => $provinsi_id
        );
        $data['kotakabupaten'] = $this->Kotakabupaten_model->get_all_kotakabupaten($params);
        $response = array();
        $response['success'] = TRUE;
        $response['message'] = 'Data kotakabupaten';
        $response['data'] = $data['kotakabupaten'];
        echo json_encode($response);
    }

    public function kecamatan()
    {
        // Get the post data
        $kotakabupaten_id = $this->input->post('kotakabupaten_id', TRUE);

        $params = array(
            'k.kotakabupaten_id' => $kotakabupaten_id
        );
        $data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan($params);
        $response = array();
        $response['success'] = TRUE;
        $response['message'] = 'Data kecamatan';
        $response['data'] = $data['kecamatan'];
        echo json_encode($response);
    }

    public function kategori_user()
    {
        $params = array(
            'level_user_id >' => 2
        );
        $data['kategori_user'] = $this->Kategori_user_model->find_kategori_user($params);
        $response = array();
        $response['success'] = TRUE;
        $response['message'] = 'Data kategori user';
        $response['data'] = $data['kategori_user'];
        echo json_encode($response);
    }

    public function kategori()
    {
        $params = array(
            'k.parent' => null
        );
        $data['kategori'] = $this->Kategori_model->find_kategori($params);
        $response = array();
        $response['success'] = TRUE;
        $response['message'] = 'Data kategori';
        $response['data'] = $data['kategori'];
        echo json_encode($response);
    }

    public function sub_kategori()
    {
        // Get the post data
        $parent = $this->input->post('parent', TRUE);

        // Validate the post data
        if (!empty($parent)) {
            $params = array(
                'k.parent' => $parent
            );
            $data['kategori'] = $this->Kategori_model->find_kategori($params);
            if ($data['kategori']) {
                $response = array();
                $response['success'] = TRUE;
                $response['message'] = 'Data sub kategori';
                $response['data'] = $data['kategori'];
            } else {
                $response = array();
                $response['success'] = TRUE;
                $response['message'] = 'Data sub kategori belum ada.';
            }
        } else {
            // Set the response and exit
            $response = array();
            $response['success'] = FALSE;
            $response['message'] = 'ID parent kategori wajib diisi.';
        }
        echo json_encode($response);
    }

    public function produk_all_kategori()
    {
        $data['produk'] = $this->Produk_model->find_produk()->result_array();
        if ($data['produk']) {
            $response = array();
            $response['success'] = TRUE;
            $response['message'] = 'Data semua produk';
            $response['data'] = $data['produk'];
        } else {
            $response = array();
            $response['success'] = TRUE;
            $response['message'] = 'Data semua produk belum ada.';
        }
        echo json_encode($response);
    }

    public function produk_by_kategori()
    {
        // Get the post data
        $kategori_id = $this->input->post('kategori_id', TRUE);

        // Validate the post data
        if (!empty($kategori_id)) {
            $params = array(
                'p.kategori_id' => $kategori_id
            );
            $data['produk'] = $this->Produk_model->find_produk($params)->result_array();
            if ($data['produk']) {
                $response = array();
                $response['success'] = TRUE;
                $response['message'] = 'Data produk berdasarkan kategori';
                $response['data'] = $data['produk'];
            } else {
                $response = array();
                $response['success'] = TRUE;
                $response['message'] = 'Data produk belum ada.';
            }
        } else {
            // Set the response and exit
            $response = array();
            $response['success'] = FALSE;
            $response['message'] = 'ID kategori wajib diisi.';
        }
        echo json_encode($response);
    }

    public function produk_detail()
    {
        // Get the post data
        $produk_id = $this->input->post('produk_id', TRUE);

        // Validate the post data
        if (!empty($produk_id)) {
            $params = array(
                'p.id' => $produk_id
            );
            $data['produk'] = $this->Produk_model->find_produk($params)->row_array();
            if ($data['produk']) {
                // update total_dilihat
                $update_produk = $this->Produk_model->update_produk($produk_id, array('total_dilihat' => $data['produk']['total_dilihat'] + 1));

                $response = array();
                $response['success'] = TRUE;
                $response['message'] = 'Data produk';
                $response['data'] = $data['produk'];
                $response['url_foto'] = site_url('uploads/produk/');
                $params_foto = array(
                    'p.produk_id' => $produk_id
                );
                $data['produk_foto'] = $this->Produk_model->find_produk_foto($params_foto);
                if ($data['produk_foto']) {
                    $response['foto'] = $data['produk_foto'];
                }
            } else {
                $response = array();
                $response['success'] = TRUE;
                $response['message'] = 'Data produk belum ada.';
            }
        } else {
            // Set the response and exit
            $response = array();
            $response['success'] = FALSE;
            $response['message'] = 'ID produk wajib diisi.';
        }
        echo json_encode($response);
    }

    public function loginByEmail()
    {
        // Get the post data
        $email = $this->input->post('email', TRUE);
        $password = $this->input->post('password', TRUE);

        // Validate the post data
        if (!empty($email) && !empty($password)) {

            // Check if any user exists with the given credentials
            $params = array(
                'u.email' => $email,
                'u.password' => sha1($password),
                'u.status' => 'Y',
                'ku.level_user_id >' => 2
            );
            $users_id = $this->Users_model->find_user($params);

            if ($users_id) {
                // Build a new key
                // $key = $this->_generate_key();
                $key = sha1(uniqid($users_id['email'], true));

                $params_token = array(
                    'token' => $key
                );

                $token = $this->Users_model->update_users($users_id['id'], $params_token);

                if ($token) {
                    $users_id = $this->Users_model->find_user($params);
                    // Set the response and exit
                    $response = array();
                    $response['success'] = TRUE;
                    $response['message'] = 'Login berhasil.';
                    $response['data'] = $users_id;
                }
            } else {
                // Set the response and exit
                $response = array();
                $response['success'] = FALSE;
                $response['message'] = 'Email atau password salah, atau status user dinonaktifkan';
            }
        } else {
            // Set the response and exit
            $response = array();
            $response['success'] = FALSE;
            $response['message'] = 'Email dan password wajib diisi.';
        }
        echo json_encode($response);
    }

    public function loginByPhone()
    {
        // Get the post data
        $hp = $this->input->post('hp', TRUE);
        $password = $this->input->post('password', TRUE);

        // Validate the post data
        if (!empty($hp) && !empty($password)) {

            // Check if any user exists with the given credentials
            $params = array(
                'u.hp' => $hp,
                'u.password' => sha1($password),
                'u.status' => 'Y',
                'ku.level_user_id >' => 2
            );
            $users_id = $this->Users_model->find_user($params);

            if ($users_id) {
                // Build a new key
                // $key = $this->_generate_key();
                $key = sha1(uniqid($users_id['hp'], true));

                $params_token = array(
                    'token' => $key
                );

                $token = $this->Users_model->update_users($users_id['id'], $params_token);

                if ($token) {
                    $users_id = $this->Users_model->find_user($params);
                    // Set the response and exit
                    $response = array();
                    $response['success'] = TRUE;
                    $response['message'] = 'Login berhasil.';
                    $response['data'] = $users_id;
                }
            } else {
                // Set the response and exit
                $response = array();
                $response['success'] = FALSE;
                $response['message'] = 'Hp atau password salah, atau status user dinonaktifkan';
            }
        } else {
            // Set the response and exit
            $response = array();
            $response['success'] = FALSE;
            $response['message'] = 'Hp dan password wajib diisi.';
        }
        echo json_encode($response);
    }

    public function register()
    {
        // Get the post data
        $email = strip_tags($this->input->post('email', TRUE));
        $password = strip_tags($this->input->post('password', TRUE));
        $nama = strip_tags($this->input->post('nama', TRUE));
        $deskripsi = strip_tags($this->input->post('deskripsi', TRUE));
        $hp = strip_tags($this->input->post('hp', TRUE));
        $status = 'Y';
        $created_at = date('Y-m-d');
        $kategori_user_id = strip_tags($this->input->post('kategori_user_id', TRUE));

        // Validate the post data
        if (!empty($nama) && !empty($hp) && !empty($kategori_user_id) && !empty($password)) {

            // Check if the given email already exists
            $params = array(
                'hp' => $hp,
            );
            $userCount = $this->Users_model->find_user($params);

            if ($userCount) {
                // Set the response and exit
                $response = array();
                $response['success'] = FALSE;
                $response['message'] = 'Hp sudah terdaftar.';
            } else {
                $key = sha1(uniqid($hp, true));
                // Insert user data
                $userData = array(
                    'nama' => $nama,
                    'email' => $email,
                    'password' => sha1($password),
                    'deskripsi' => $deskripsi,
                    'hp' => $hp,
                    'kategori_user_id' => $kategori_user_id,
                    'status' => $status,
                    'created_at' => $created_at,
                    'token' => $key
                );
                $users_id = $this->Users_model->add_users($userData);

                // Check if the user data is inserted
                if ($users_id) {

                    $params = array(
                        'u.hp' => $hp,
                        'u.password' => sha1($password)
                    );
                    $result_users_id = $this->Users_model->find_user($params);

                    // kirim email
                    $subject = "Registrasi";

                    $message = "Salam hormat, " . $nama . "<br><br>Silakan masuk ke aplikasi mobile " . APP_TITLE . " menggunakan informasi akun berikut: <br>Email : " . $email . "<br>Password : " . $password . "<br><br>Terima kasih atas perhatian dan kerjasamanya.";

                    $result = $this->_send_email($email, $nama, $subject, $message);
                    if ($result) {
                        m_success("Registrasi berhasil, periksa email Anda untuk melihat informasi akun login aplikasi " . APP_TITLE);
                    } else {
                        m_error("Informasi akun gagal dikirim. Ada masalah saat pengiriman email");
                        // send log ...
                    }

                    // Set the response and exit
                    $response = array();
                    $response['success'] = TRUE;
                    $response['message'] = 'Registrasi berhasil.';
                    $response['data'] = $result_users_id;
                } else {
                    // Set the response and exit
                    $response = array();
                    $response['success'] = FALSE;
                    $response['message'] = 'Terjadi masalah, mohon coba lagi.';
                }
            }
        } else {
            // Set the response and exit
            $response = array();
            $response['success'] = FALSE;
            $response['message'] = 'Mohon lengkapi data registrasi.';
        }
        echo json_encode($response);
    }

    public function isUserExists()
    {
        // Get the post data
        //$password = strip_tags($this->input->post('password', TRUE));
        $hp = strip_tags($this->input->post('hp', TRUE));
        $status = 'Y';
        $created_at = date('Y-m-d');

        // Validate the post data
        if (!empty($hp)) { //&& !empty($password)

            // Check if the given email already exists
            $params = array(
                'hp' => $hp,
            );
            $userCount = $this->Users_model->find_user($params);

            if ($userCount) {
                // Set the response and exit
                $response = array();
                $response['success'] = TRUE;
                $response['message'] = 'No HP sudah terdaftar.';
            } else {
                // Set the response and exit
                $response = array();
                $response['success'] = FALSE;
                $response['message'] = 'No HP belum terdaftar.';

                // Insert user data
                // $userData = array(
                //     // 'password' => sha1($password),
                //     'hp' => $hp,
                //     'status' => $status,
                //     'created_at' => $created_at,
                // );
                // $users_id = $this->Users_model->add_users($userData);

                // // Check if the user data is inserted
                // if ($users_id) {

                //     // Set the response and exit
                //     $response = array();
                //     $response['success'] = TRUE;
                //     $response['message'] = 'Registrasi berhasil.';
                //     $response['data'] = $users_id;
                // } else {
                //     // Set the response and exit
                //     $response = array();
                //     $response['success'] = FALSE;
                //     $response['message'] = 'Terjadi masalah, mohon coba lagi.';
                // }
            }
        } else {
            // Set the response and exit
            $response = array();
            $response['success'] = FALSE;
            $response['message'] = 'Mohon lengkapi data registrasi.';
        }
        echo json_encode($response);
    }

    public function reset_password()
    {
        // Get the post data
        $email = strip_tags($this->input->post('email', TRUE));

        // Validate the post data
        if (!empty($email)) {

            // Check if the given email already exists
            $params = array(
                'email' => $email,
            );
            $user = $this->Users_model->find_user($params);

            if ($user) {
                // generate new password 
                $password = random_string('alnum', 6);

                // update new password 
                $result = $this->Users_model->update_users($user['id'], array("password" => sha1($password)));

                if ($result) {
                    // kirim email
                    $subject = "Reset Password";

                    $message = "Salam hormat, " . $user['nama'] . "<br><br>Silakan masuk ke aplikasi mobile " . APP_TITLE . " menggunakan informasi akun berikut: <br>Email : " . $email . "<br>Password : " . $password . "<br><br>Terima kasih atas perhatian dan kerjasamanya.";

                    $result = $this->_send_email($email, $user['nama'], $subject, $message);
                    if ($result) {
                        // Set the response and exit
                        $response = array();
                        $response['success'] = TRUE;
                        $response['message'] = 'Password baru berhasil dikirim ke email : ' . $email;
                    } else {
                        // Set the response and exit
                        $response = array();
                        $response['success'] = FALSE;
                        $response['message'] = 'Password baru gagal dikirim ke email : ' . $email;
                    }
                } else {
                    // Set the response and exit
                    $response = array();
                    $response['success'] = FALSE;
                    $response['message'] = 'Password gagal diperbarui';
                }
            } else {
                // Set the response and exit
                $response = array();
                $response['success'] = FALSE;
                $response['message'] = 'Email belum terdaftar.';
            }
        } else {
            // Set the response and exit
            $response = array();
            $response['success'] = FALSE;
            $response['message'] = 'Mohon lengkapi data email.';
        }
        echo json_encode($response);
    }

    private function _send_email($email = null, $nama = null, $subject = null, $message = null)
    {
        // Konfigurasi email
        $config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            // 'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_host' => EMAIL_HOST,
            'smtp_user' => EMAIL_ADDRESS_SENDER,
            'smtp_pass' => EMAIL_PASSWORD_SENDER,
            'smtp_port' => EMAIL_PORT,
            'crlf'      => "\r\n",
            'newline'   => "\r\n"
        ];

        // Load library email dan konfigurasinya
        $this->load->library('email', $config);

        // Email dan nama pengirim
        $this->email->from(EMAIL_ADDRESS_SENDER, EMAIL_NAME_SENDER);

        // Email penerima
        $this->email->to($email); // Ganti dengan email tujuan kamu

        // Lampiran email, isi dengan url/path file
        //$this->email->attach('link');

        // Subject email
        $this->email->subject(APP_TITLE . " " . APP_OWNER . " - " . $subject);

        // Isi email
        $this->email->message($message);

        // Tampilkan pesan sukses atau error
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }
}
