<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Tani extends REST_Controller
{

	public function __construct()
	{
		parent::__construct();

		// verifikasi
		$filter = $this->db->get_where('user', ['hp' => $this->input->post('hp', TRUE), 'token' => $this->input->post('token', TRUE)])->num_rows();

		if ($filter === 0) {
			$filter1 = $this->db->get_where('user', ['hp' => $this->get('hp'), 'token' => $this->get('token')])->num_rows();
			if ($filter1 === 0) {
				$this->end_app();
			}
		}

		// Load the user model
		$this->load->model('Users_model');
		$this->load->model('Produk_model');
		$this->load->model('Produk_fav_model');
		$this->load->model('Chat_model');
	}

	private function end_app()
	{
		$this->response([
			'success'   => FALSE,
			'message' => 'Silakan login terlebih dahulu melalui aplikasi.'
		], REST_Controller::HTTP_OK);
	}

	/* Data profil */

	function profil_get()
	{
		if (isset($_GET) && count($_GET) > 0) {
			$hp = $this->input->get('hp');
			$token = $this->input->get('token');

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {
				$this->response([
					'success'   => TRUE,
					'message'   => 'Profil user tersedia',
					'data'      => $user
				], REST_Controller::HTTP_OK);
			} else {
				$this->response([
					'success'   => FALSE,
					'message' => 'Profil user tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success'   => FALSE,
				'message' => 'Tidak ada data'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function profil_post()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$email = $this->input->post('email', TRUE);
			$token = $this->input->post('token', TRUE);
			$reg_id = $this->input->post('reg_id', TRUE);
			$nama = strip_tags($this->input->post('nama', TRUE));
			$deskripsi = strip_tags($this->input->post('deskripsi', TRUE));
			$hp = strip_tags($this->input->post('hp', TRUE));
			// $password = strip_tags($this->input->post('password', TRUE));

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {
				// Update user data
				// if ($password == "") {
				//     $userData = array(
				//         'nama' => $nama,
				//         'deskripsi' => $deskripsi,
				//         // 'hp' => $hp,
				//         'reg_id' => $reg_id,
				//         'email' => $email
				//     );
				// } else {
				$userData = array(
					'nama' => $nama,
					// 'password' => sha1($password),
					'deskripsi' => $deskripsi,
					'hp' => $hp,
					'reg_id' => $reg_id,
					'email' => $email,
					'token' => $token
				);
				// }

				$users_id = $this->Users_model->update_users($user['id'], $userData);

				if ($users_id) {
					$this->response([
						'success' => TRUE,
						'message' => 'Data user berhasil diperbarui'
					], REST_Controller::HTTP_OK);
				} else {
					$this->response([
						'success' => FALSE,
						'message' => 'Data user gagal diperbarui'
					], REST_Controller::HTTP_OK);
				}
			} else {
				$this->response([
					'success' => FALSE,
					'message' => 'Data user tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success' => FALSE,
				'message' => 'Tidak ada POST'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function upload_foto_post()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$email = $this->input->post('email', TRUE);
			$hp = $this->input->post('hp', TRUE);
			$token = $this->input->post('token', TRUE);
			$reg_id = $this->input->post('reg_id', TRUE);
			$nama = $this->input->post('nama', TRUE);
			$deskripsi = $this->input->post('deskripsi', TRUE);

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {
				// Update user data

				// upload foto
				$config['upload_path']          = './uploads/foto/';
				$config['overwrite'] = TRUE;
				$config['allowed_types']        = 'jpg';
				$rename = date('Y-m-d') . "-" . $user['id'];
				$config['file_name']            = $rename . ".jpg";

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('image')) {
					$this->response([
						'success' => FALSE,
						'message' => 'Foto gagal diupload. Silakan upload ulang.'
					], REST_Controller::HTTP_OK);
				} else {
					$target_dir = "uploads/foto";
					$target_file = $target_dir . "/" . $config['file_name'];
					$url_foto = site_url() . $target_file;

					$userData = array(
						'url_foto' => $url_foto,
						'foto' => $config['file_name'],
						'nama' => $nama,
						'deskripsi' => $deskripsi,
						'hp' => $hp,
						'reg_id' => $reg_id,
						'email' => $email,
						'token' => $token
					);

					$users_id = $this->Users_model->update_users($user['id'], $userData);

					if ($users_id) {
						$this->response([
							'success'   => TRUE,
							'message'   => 'Foto berhasil disimpan',
						], REST_Controller::HTTP_OK);
					} else {
						$this->response([
							'success' => FALSE,
							'message' => 'Foto gagal disimpan'
						], REST_Controller::HTTP_OK);
					}
				}
			} else {
				$this->response([
					'success' => FALSE,
					'message' => 'Data user tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success' => FALSE,
				'message' => 'Tidak ada POST'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function ubahpassword_post()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$hp = strip_tags($this->input->post('hp', TRUE));
			$token = $this->input->post('token', TRUE);
			$password_lama = strip_tags($this->input->post('password_lama', TRUE));
			$password_baru = strip_tags($this->input->post('password_baru', TRUE));

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token, 'password' => sha1($password_lama)));

			if (isset($user['hp'])) {
				$userData = array(
					'password' => sha1($password_baru)
				);

				$users_id = $this->Users_model->update_users($user['id'], $userData);

				if ($users_id) {
					$this->response([
						'success' => TRUE,
						'message' => 'Password berhasil diperbarui'
					], REST_Controller::HTTP_OK);
				} else {
					$this->response([
						'success' => FALSE,
						'message' => 'Password gagal diperbarui'
					], REST_Controller::HTTP_OK);
				}
			} else {
				$this->response([
					'success' => FALSE,
					'message' => 'Data user tidak ditemukan/password salah'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success' => FALSE,
				'message' => 'Tidak ada POST'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function produk_post()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$hp = $this->input->post('hp', TRUE);
			$token = $this->input->post('token', TRUE);
			$nama_produk = $this->input->post('nama_produk', TRUE);
			$deskripsi = $this->input->post('deskripsi', TRUE);
			$harga = $this->input->post('harga', TRUE);
			$kondisi = $this->input->post('kondisi', TRUE);
			$status = 'Y';
			$created_at = date('Y-m-d');
			$kategori_id = $this->input->post('kategori_id', TRUE);
			$kecamatan_id = $this->input->post('kecamatan_id', TRUE);

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {
				$userData = array(
					'nama_produk' => $nama_produk,
					'deskripsi' => $deskripsi,
					'harga' => $harga,
					'kondisi' => $kondisi,
					'status' => $status,
					'created_at' => $created_at,
					'kategori_id' => $kategori_id,
					'kecamatan_id' => $kecamatan_id,
					'user_id' => $user['id']
				);
				$produk_id = $this->Produk_model->add_produk($userData);

				if ($produk_id) {
					// awal upload gambar 
					$isAllSuccess = TRUE;
					$errCountGambar = 0;
					$errCountRecord = 0;

					$params = array('produk_id' => $produk_id);

					for ($i = 0; $i < count($_FILES['userFiles']['name']); $i++) {
						$_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
						$_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
						$_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
						$_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
						$_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];

						$params['foto'] = $produk_id . '-' . (DateTime::createFromFormat('U.u', microtime(true))->format("m-d-Y-H:i:s:u"));
						$params['featured_img'] =  $this->input->post('featured_img', TRUE);

						$config['upload_path']          = './uploads/produk';
						$config['overwrite']            = TRUE;
						$config['allowed_types']        = 'jpg|png|jpeg|JPG|PNG';
						$config['file_name']            = $params['foto'] . '.png';
						$config['max_size']             = 0;
						$config['max_width']            = 0;
						$config['max_height']           = 0;
						$config['max_filename']         = 0;
						$config['file_ext_tolower']     = TRUE;
						$config['remove_spaces']        = TRUE;
						$config['detect_mime']          = TRUE;
						$config['mod_mime_fix']         = TRUE;
						$config['overwrite']            = TRUE;

						$this->load->library('upload', $config);
						$this->upload->initialize($config);

						if (!$this->upload->do_upload('userFile')) {
							$isAllSuccess = FALSE;
							$errCountGambar++;
						} else if ($this->Produk_model->add_foto_produk($params)) {
							$isAllSuccess = $isAllSuccess && TRUE;
						} else {
							$isAllSuccess = FALSE;
							$errCountRecord++;
						}
					}

					if ($isAllSuccess) {
						$this->response([
							'success' => TRUE,
							'message' => 'Data produk berhasil disimpan'
						], REST_Controller::HTTP_OK);
					} else {
						$this->response([
							'success' => FALSE,
							'message' => $errCountGambar . ' Gagal upload gambar dan ' . $errCountRecord . ' gagal upload record'
						], REST_Controller::HTTP_OK);
					}
					// akhir upload gambar
				} else {
					$this->response([
						'success' => FALSE,
						'message' => 'Data produk gagal disimpan'
					], REST_Controller::HTTP_OK);
				}
			} else {
				$this->response([
					'success' => FALSE,
					'message' => 'Data user tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success' => FALSE,
				'message' => 'Tidak ada data'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function produk_get()
	{
		if (isset($_GET) && count($_GET) > 0) {
			$hp = $this->input->get('hp');
			$token = $this->input->get('token');

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {

				$all_produk = $this->Produk_model->find_produk(array('user_id' => $user['id']))->result_array();

				if ($all_produk) {
					$this->response([
						'success' => TRUE,
						'message' => 'Data produk tersedia',
						'data' => $all_produk
					], REST_Controller::HTTP_OK);
				} else {
					$this->response([
						'success' => FALSE,
						'message' => 'Data produk belum ada'
					], REST_Controller::HTTP_OK);
				}
			} else {
				$this->response([
					'success' => FALSE,
					'message' => 'Data user tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success' => FALSE,
				'message' => 'Tidak ada data'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function upload_foto_produk_post()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$hp = $this->input->post('hp');
			$token = $this->input->post('token');

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {
				$produk = $this->Produk_model->find_produk(array('user_id' => $user['id']));

				if ($produk > 0) {
					// upload foto
					$config['upload_path']          = './uploads/produk/';
					$config['allowed_types']        = 'jpg';
					$rename = date('Y-m-d') . "-" . $user['id'] . "-" . $produk['id'];
					$config['file_name']            = $rename . ".jpg";

					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('image')) {
						$this->response([
							'success' => FALSE,
							'message' => 'Foto produk gagal diupload. Silakan upload ulang.'
						], REST_Controller::HTTP_OK);
					} else {
						$target_dir = "uploads/produk";
						$target_file = $target_dir . "/" . $config['file_name'];
						$url_foto = site_url() . $target_file;

						$produkData = array(
							'url_foto' => $url_foto,
							'foto' => $config['file_name'],
							'featured_img' => $this->input->post('featured_img', TRUE),
							'produk_id' => $this->input->post('produk_id')
						);

						$produk_id = $this->Produk_model->add_foto_produk($produkData);

						if ($produk_id) {
							$this->response([
								'success'   => TRUE,
								'message'   => 'Foto produk berhasil disimpan',
							], REST_Controller::HTTP_OK);
						} else {
							$this->response([
								'success' => FALSE,
								'message' => 'Foto produk gagal disimpan'
							], REST_Controller::HTTP_OK);
						}
					}
				} else {
					$this->response([
						'success' => FALSE,
						'message' => 'Data produk tidak ditemukan'
					], REST_Controller::HTTP_OK);
				}
			} else {
				$this->response([
					'success' => FALSE,
					'message' => 'Data user tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success' => FALSE,
				'message' => 'Tidak ada data'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function chat_get()
	{
		if (isset($_GET) && count($_GET) > 0) {
			$hp = $this->input->get('hp');
			$token = $this->input->get('token');

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {
				$chat = $this->Chat_model->find_chat(array('c.user_id_penjual' => $user['id']), array('c.user_id_pembeli' => $user['id']));
				if ($chat) {
					$this->response([
						'success'   => TRUE,
						'message'   => 'Chat tersedia',
						'data'      => $chat
					], REST_Controller::HTTP_OK);
				} else {
					$this->response([
						'success'   => FALSE,
						'message' => 'Chat tidak ditemukan'
					], REST_Controller::HTTP_NOT_FOUND);
				}
			} else {
				$this->response([
					'success'   => FALSE,
					'message' => 'User tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success'   => FALSE,
				'message' => 'Tidak ada data'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function chat_detail_get()
	{
		if (isset($_GET) && count($_GET) > 0) {
			$hp = $this->input->get('hp');
			$token = $this->input->get('token');
			$chat_id = $this->input->get('chat_id');

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {
				$chat = $this->Chat_model->find_chat_detail(array('c.user_id_penjual' => $user['id'], 'c.id' => $chat_id), array('c.user_id_pembeli' => $user['id'], 'c.id' => $chat_id));
				if ($chat) {
					$this->response([
						'success'   => TRUE,
						'message'   => 'Chat detail tersedia',
						'data'      => $chat
					], REST_Controller::HTTP_OK);
				} else {
					$this->response([
						'success'   => FALSE,
						'message' => 'Chat tidak ditemukan'
					], REST_Controller::HTTP_NOT_FOUND);
				}
			} else {
				$this->response([
					'success'   => FALSE,
					'message' => 'User tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success'   => FALSE,
				'message' => 'Tidak ada data'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function chat_post()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$hp = $this->input->post('hp');
			$token = $this->input->post('token');
			$produk_id = $this->input->post('produk_id');
			$chat_id = $this->input->post('chat_id');

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {

				if (isset($chat_id)) {
					$chatData = array(
						'isi_text' => $this->input->post('isi_text', TRUE),
						'chat_id' => $this->input->post('chat_id', TRUE),
						'user_id' => $user['id']
					);

					$chat_id = $this->Chat_model->add_chat_detail($chatData);

					if ($chat_id) {
						$this->response([
							'success' => TRUE,
							'message' => 'Chat berhasil dikirim'
						], REST_Controller::HTTP_OK);
					} else {
						$this->response([
							'success' => FALSE,
							'message' => 'Chat gagal dikirim'
						], REST_Controller::HTTP_OK);
					}
				} else {
					$params = array(
						'p.id' => $produk_id
					);
					$data['produk'] = $this->Produk_model->find_produk($params)->row_array();
					$chatData = array(
						'user_id_pembeli' => $user['id'],
						'user_id_penjual' => $data['produk']['user_id'],
						'produk_id' => $data['produk']['id']
					);

					$chat_id = $this->Chat_model->add_chat($chatData);

					$chatdetailData = array(
						'isi_text' => $this->input->post('isi_text', TRUE),
						'chat_id' => $chat_id,
						'user_id' => $user['id']
					);

					$chatdetail_id = $this->Chat_model->add_chat_detail($chatdetailData);

					if ($chatdetail_id) {
						$this->response([
							'success' => TRUE,
							'message' => 'Chat berhasil dikirim'
						], REST_Controller::HTTP_OK);
					} else {
						$this->response([
							'success' => FALSE,
							'message' => 'Chat gagal dikirim'
						], REST_Controller::HTTP_OK);
					}
				}
			} else {
				$this->response([
					'success' => FALSE,
					'message' => 'Data user tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success' => FALSE,
				'message' => 'Tidak ada data'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function favorit_get()
	{
		if (isset($_GET) && count($_GET) > 0) {
			$hp = $this->input->get('hp');
			$token = $this->input->get('token');

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {
				$favorit = $this->Produk_fav_model->find_favorit(array('pdf.user_id' => $user['id']))->result_array();
				if ($favorit) {
					$this->response([
						'success'   => TRUE,
						'message'   => 'Favorit tersedia',
						'data'      => $favorit
					], REST_Controller::HTTP_OK);
				} else {
					$this->response([
						'success'   => FALSE,
						'message' => 'Belum ada produk favorit'
					], REST_Controller::HTTP_NOT_FOUND);
				}
			} else {
				$this->response([
					'success'   => FALSE,
					'message' => 'User tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success'   => FALSE,
				'message' => 'Tidak ada data'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function favorit_post()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$hp = $this->input->post('hp');
			$token = $this->input->post('token');
			$produk_id = $this->input->post('produk_id');

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {


				$favData = array(
					'produk_id' => $produk_id,
					'user_id' => $user['id']
				);

				$res_fav_id = $this->Produk_fav_model->findRow_fav($favData);
				if ($res_fav_id) {
					$this->response([
						'success' => FALSE,
						'message' => 'Produk sudah favorit Anda'
					], REST_Controller::HTTP_OK);
				} else {
					$fav_id = $this->Produk_fav_model->add_fav($favData);

					if ($fav_id) {
						$this->response([
							'success' => TRUE,
							'message' => 'Produk favorit berhasil disimpan'
						], REST_Controller::HTTP_OK);
					} else {
						$this->response([
							'success' => FALSE,
							'message' => 'Produk favorit gagal disimpan'
						], REST_Controller::HTTP_OK);
					}
				}
			} else {
				$this->response([
					'success' => FALSE,
					'message' => 'Data user tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success' => FALSE,
				'message' => 'Tidak ada data'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function isFavorit_post()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$hp = $this->input->post('hp');
			$token = $this->input->post('token');
			$produk_id = $this->input->post('produk_id');

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {
				$favData = array(
					'produk_id' => $produk_id,
					'user_id' => $user['id']
				);

				$res_fav_id = $this->Produk_fav_model->findRow_fav($favData);
				if ($res_fav_id) {
					$this->response([
						'success' => TRUE,
						'message' => 'Produk sudah favorit'
					], REST_Controller::HTTP_OK);
				} else {
					$this->response([
						'success' => FALSE,
						'message' => 'Produk belum favorit'
					], REST_Controller::HTTP_OK);
				}
			} else {
				$this->response([
					'success' => FALSE,
					'message' => 'Data user tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success' => FALSE,
				'message' => 'Tidak ada data'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function unfavorit_post()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$hp = $this->input->post('hp');
			$token = $this->input->post('token');
			$produk_id = $this->input->post('produk_id');

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {
				$favData = array(
					'produk_id' => $produk_id,
					'user_id' => $user['id']
				);

				$res_fav_id = $this->Produk_fav_model->findRow_fav($favData);
				if ($res_fav_id) {
					$fav_id = $this->Produk_fav_model->delete_fav($favData);

					if ($fav_id) {
						$this->response([
							'success' => TRUE,
							'message' => 'Produk favorit berhasil dihapus'
						], REST_Controller::HTTP_OK);
					} else {
						$this->response([
							'success' => FALSE,
							'message' => 'Produk favorit gagal dihapus'
						], REST_Controller::HTTP_OK);
					}
				} else {
					$this->response([
						'success' => FALSE,
						'message' => 'Produk favorit tidak ditemukan'
					], REST_Controller::HTTP_OK);
				}
			} else {
				$this->response([
					'success' => FALSE,
					'message' => 'Data user tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success' => FALSE,
				'message' => 'Tidak ada data'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}

	function cariProduk_post()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$keyword = $this->input->post('keyword');
			$hp = $this->input->post('hp');
			$token = $this->input->post('token');

			$user = $this->Users_model->find_user(array('hp' => $hp, 'token' => $token));

			if (isset($user['hp'])) {

				$all_produk = $this->Produk_model->search_produk($keyword)->result_array();

				if ($all_produk) {
					$this->response([
						'success' => TRUE,
						'message' => 'Data produk tersedia',
						'data' => $all_produk
					], REST_Controller::HTTP_OK);
				} else {
					$this->response([
						'success' => FALSE,
						'message' => 'Data produk belum ada'
					], REST_Controller::HTTP_OK);
				}
			} else {
				$this->response([
					'success' => FALSE,
					'message' => 'Data user tidak ditemukan'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response([
				'success' => FALSE,
				'message' => 'Tidak ada data'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}
}
