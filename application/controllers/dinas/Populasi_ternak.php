<?php

class Populasi_ternak extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		authenticate();
		has_privileges_admin();

		$this->load->model('Populasi_ternak_model');
		$this->load->model('Kecamatan_model');
	}

	/*
     * Listing of periksa_qurban
     */
	function index()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$tgl1 = $this->input->post('tgl1', true);
			$tgl2 = $this->input->post('tgl2', true);
			$kecamatan = $this->input->post('kecamatan', true);
		} else {
			$tgl1 = date('Y-m-01');
			$tgl2 = date('Y-m-d');
			$kecamatan = 'all';
		}

		if ($kecamatan == 'all') {
			$params = array(
				"tgl >=" => $tgl1,
				"tgl <=" => $tgl2,
			);
		} else {
			$params = array(
				"tgl >=" => $tgl1,
				"tgl <=" => $tgl2,
				"kecamatan_id" => $kecamatan,
			);
		}

		$data['populasi_ternak'] = $this->Populasi_ternak_model->get_all_populasi_ternak($params);
		$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();

		$data['_usedtable'] = TRUE;
		$data['_usedselect2'] = TRUE;
		$data['_view'] = 'dinas/v_populasi_ternak/index';
		$this->load->view('dinas/layouts/main', $data);
	}

	/*
     * Adding a new periksa_qurban
     */
	function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('tgl', 'Tgl', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
		$this->form_validation->set_rules('sapi_perah', 'Sapi Perah', 'required');
		$this->form_validation->set_rules('sapi_potong', 'Sapi Potong', 'required');
		$this->form_validation->set_rules('kerbau', 'Kerbau', 'required');
		$this->form_validation->set_rules('kambing', 'Kambing', 'required');
		$this->form_validation->set_rules('domba', 'Domba', 'required');
		$this->form_validation->set_rules('kuda', 'Kuda', 'required');
		$this->form_validation->set_rules('babi', 'Babi', 'required');
		$this->form_validation->set_rules('ayam_buras', 'Ayam Buras', 'required');
		$this->form_validation->set_rules('ayam_pedaging', 'Ayam Pedaging', 'required');
		$this->form_validation->set_rules('ayam_petelur', 'Ayam Petelur', 'required');
		$this->form_validation->set_rules('itik', 'Itik', 'required');

		if ($this->form_validation->run()) {
			$params = array(
				'tgl' => $this->input->post('tgl', true),
				'kecamatan_id' => $this->input->post('kecamatan', true),
				'sapi_perah' => $this->input->post('sapi_perah', true),
				'sapi_potong' => $this->input->post('sapi_potong', true),
				'kerbau' => $this->input->post('kerbau', true),
				'kambing' => $this->input->post('kambing', true),
				'domba' => $this->input->post('domba', true),
				'kuda' => $this->input->post('kuda', true),
				'babi' => $this->input->post('babi', true),
				'ayam_buras' => $this->input->post('ayam_buras', true),
				'ayam_pedaging' => $this->input->post('ayam_pedaging', true),
				'ayam_petelur' => $this->input->post('ayam_petelur', true),
				'itik' => $this->input->post('itik', true),
			);

			$populasi_ternak_id = $this->Populasi_ternak_model->add_populasi_ternak($params);
			redirect('dinas/populasi_ternak');
		} else {
			$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
			$data['_usedselect2'] = TRUE;
			$data['_usedtable'] = TRUE;
			$data['_view'] = 'dinas/v_populasi_ternak/add';
			$this->load->view('dinas/layouts/main', $data);
		}
	}

	/*
     * Editing a periksa_qurban
     */
	function edit($id)
	{
		// check if the periksa_qurban exists before trying to edit it
		$data['populasi_ternak'] = $this->Populasi_ternak_model->get_populasi_ternak(array('id' => $id));

		if (isset($data['populasi_ternak']['id'])) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
			$this->form_validation->set_rules('tgl', 'Tgl', 'required');
			$this->form_validation->set_rules('sapi_perah', 'Sapi Perah', 'required');
			$this->form_validation->set_rules('sapi_potong', 'Sapi Potong', 'required');
			$this->form_validation->set_rules('kerbau', 'Kerbau', 'required');
			$this->form_validation->set_rules('kambing', 'Kambing', 'required');
			$this->form_validation->set_rules('domba', 'Domba', 'required');
			$this->form_validation->set_rules('kuda', 'Kuda', 'required');
			$this->form_validation->set_rules('babi', 'Babi', 'required');
			$this->form_validation->set_rules('ayam_buras', 'Ayam Buras', 'required');
			$this->form_validation->set_rules('ayam_pedaging', 'Ayam Pedaging', 'required');
			$this->form_validation->set_rules('ayam_petelur', 'Ayam Petelur', 'required');
			$this->form_validation->set_rules('itik', 'Itik', 'required');

			if ($this->form_validation->run()) {
				$params = array(
					'tgl' => $this->input->post('tgl', true),
					'kecamatan_id' => $this->input->post('kecamatan', true),
					'sapi_perah' => $this->input->post('sapi_perah', true),
					'sapi_potong' => $this->input->post('sapi_potong', true),
					'kerbau' => $this->input->post('kerbau', true),
					'kambing' => $this->input->post('kambing', true),
					'domba' => $this->input->post('domba', true),
					'kuda' => $this->input->post('kuda', true),
					'babi' => $this->input->post('babi', true),
					'ayam_buras' => $this->input->post('ayam_buras', true),
					'ayam_pedaging' => $this->input->post('ayam_pedaging', true),
					'ayam_petelur' => $this->input->post('ayam_petelur', true),
					'itik' => $this->input->post('itik', true),
				);

				$this->Populasi_ternak_model->update_populasi_ternak($id, $params);
				redirect('dinas/populasi_ternak');
			} else {
				$data['_usedtable'] = TRUE;
				$data['_usedselect2'] = TRUE;
				$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
				$data['_view'] = 'dinas/v_populasi_ternak/edit';
				$this->load->view('dinas/layouts/main', $data);
			}
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/populasi_ternak', 'refresh');
		}
	}

	/*
     * Deleting periksa_qurban
     */
	function remove($id)
	{
		$populasi_ternak = $this->Populasi_ternak_model->get_populasi_ternak(array('id' => $id));

		// check if the populasi_ternak exists before trying to delete it
		if (isset($populasi_ternak['id'])) {
			$this->Populasi_ternak_model->delete_populasi_ternak($id);
			redirect('dinas/populasi_ternak');
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/populasi_ternak', 'refresh');
		}
	}
}
