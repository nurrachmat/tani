<?php

class Kategori_user extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        authenticate();
        has_privileges_admin();
        $this->load->model('Level_user_model');
        $this->load->model('Kategori_user_model');
    }

    /*
     * Listing of kategori_user
     */
    function index()
    {
        $data['_usedtable'] = TRUE;
        $data['kategori_user'] = $this->Kategori_user_model->get_all_kategori_user();

        $data['_view'] = 'dinas/kategori_user/index';
        $this->load->view('dinas/layouts/main', $data);
    }

    /*
     * Adding a new kategori_user
     */
    function add()
    {
        $data['_usedselect2'] = TRUE;
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nama_kategori_user', 'Nama Kategori User', 'required|is_unique[kategori_user.nama_kategori_user]');
        $this->form_validation->set_rules('level_user_id', 'Nama Level User', 'required');

        if ($this->form_validation->run()) {
            $params = array(
                'nama_kategori_user' => $this->input->post('nama_kategori_user'),
                'level_user_id' => $this->input->post('level_user_id'),
            );

            $kategori_user_id = $this->Kategori_user_model->add_kategori_user($params);
            if (isset($kategori_user_id)) {
                m_success("Data berhasil disimpan");
            } else {
                m_error("Data gagal disimpan");
            }
            redirect('dinas/kategori_user/index');
        } else {
            $data['all_level_user'] = $this->Level_user_model->get_all_level_user();
            $data['_view'] = 'dinas/kategori_user/add';
            $this->load->view('dinas/layouts/main', $data);
        }
    }

    /*
     * Editing a kategori_user
     */
    function edit($id)
    {
        $data['_usedselect2'] = TRUE;
        // check if the kategori_user exists before trying to edit it
        $data['kategori_user'] = $this->Kategori_user_model->get_kategori_user($id);

        if (isset($data['kategori_user']['id'])) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('nama_kategori_user', 'Nama Kategori User', 'required');
            $this->form_validation->set_rules('level_user_id', 'Nama Level User', 'required');

            if ($this->form_validation->run()) {
                $params = array(
                    'nama_kategori_user' => $this->input->post('nama_kategori_user'),
                    'level_user_id' => $this->input->post('level_user_id'),
                );

                $kategori_user_id = $this->Kategori_user_model->update_kategori_user($id, $params);
                if (isset($kategori_user_id)) {
                    m_success("Data berhasil disimpan");
                } else {
                    m_error("Data gagal disimpan");
                }
                redirect('dinas/kategori_user/index');
            } else {
                $data['all_level_user'] = $this->Level_user_model->get_all_level_user();
                $data['_view'] = 'dinas/kategori_user/edit';
                $this->load->view('dinas/layouts/main', $data);
            }
        } else
            show_error('The kategori_user you are trying to edit does not exist.');
    }

    /*
     * Deleting kategori_user
     */
    function remove($id)
    {
        $kategori_user = $this->Kategori_user_model->get_kategori_user($id);

        // check if the kategori_user exists before trying to delete it
        if (isset($kategori_user['id'])) {
            $result = $this->Kategori_user_model->delete_kategori_user($id);
            if ($result) {
                m_success("Data berhasil dihapus");
            } else {
                m_error("Data gagal dihapus");
            }
            redirect('dinas/kategori_user/index');
        } else
            show_error('The kategori_user you are trying to delete does not exist.');
    }
}
