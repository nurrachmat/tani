<?php

class Vaksinasirabies extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		authenticate();
		has_privileges_admin();

		$this->load->model('Vaksinasirabies_model');
		$this->load->model('Kecamatan_model');
	}

	/*
     * Listing of periksa_qurban
     */
	function index()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$tgl1 = $this->input->post('tgl1', true);
			$tgl2 = $this->input->post('tgl2', true);
			$kecamatan = $this->input->post('kecamatan', true);
		} else {
			$tgl1 = date('Y-m-01');
			$tgl2 = date('Y-m-d');
			$kecamatan = 'all';
		}

		if ($kecamatan == 'all') {
			$params = array(
				"tgl >=" => $tgl1,
				"tgl <=" => $tgl2,
			);
		} else {
			$params = array(
				"tgl >=" => $tgl1,
				"tgl <=" => $tgl2,
				"kecamatan_id" => $kecamatan,
			);
		}

		$data['vaksinasirabies'] = $this->Vaksinasirabies_model->get_all_vaksinasirabies($params);
		$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();

		$data['_usedtable'] = TRUE;
		$data['_usedselect2'] = TRUE;
		$data['_view'] = 'dinas/v_vaksinasirabies/index';
		$this->load->view('dinas/layouts/main', $data);
	}

	/*
     * Adding a new periksa_qurban
     */
	function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
		$this->form_validation->set_rules('tgl', 'Tgl', 'required');
		$this->form_validation->set_rules('pemilik', 'Pemilik', 'required');
		$this->form_validation->set_rules('no_batch', 'No Batch', 'required');
		$this->form_validation->set_rules('jenis_hpr', 'Jenis Hpr', 'required');
		$this->form_validation->set_rules('jk', 'Jk', 'required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required|integer');

		if ($this->form_validation->run()) {
			$params = array(
				'tgl' => $this->input->post('tgl', true),
				'kecamatan_id' => $this->input->post('kecamatan', true),
				'pemilik' => $this->input->post('pemilik', true),
				'no_batch' => $this->input->post('no_batch', true),
				'jenis_hpr' => $this->input->post('jenis_hpr', true),
				'jk' => $this->input->post('jk', true),
				'jumlah' => $this->input->post('jumlah', true),
				'ket' => $this->input->post('ket', true),
			);

			$vaksinasirabies_id = $this->Vaksinasirabies_model->add_vaksinasirabies($params);
			redirect('dinas/vaksinasirabies');
		} else {
			$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
			$data['_usedselect2'] = TRUE;
			$data['_usedtable'] = TRUE;
			$data['_view'] = 'dinas/v_vaksinasirabies/add';
			$this->load->view('dinas/layouts/main', $data);
		}
	}

	/*
     * Editing a periksa_qurban
     */
	function edit($id)
	{
		// check if the periksa_qurban exists before trying to edit it
		$data['vaksinasirabies'] = $this->Vaksinasirabies_model->get_vaksinasirabies(array('id' => $id));

		if (isset($data['vaksinasirabies']['id'])) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
			$this->form_validation->set_rules('tgl', 'Tgl', 'required');
			$this->form_validation->set_rules('pemilik', 'Pemilik', 'required');
			$this->form_validation->set_rules('no_batch', 'No Batch', 'required');
			$this->form_validation->set_rules('jenis_hpr', 'Jenis Hpr', 'required');
			$this->form_validation->set_rules('jk', 'Jk', 'required');
			$this->form_validation->set_rules('jumlah', 'Jumlah', 'required|integer');

			if ($this->form_validation->run()) {
				$params = array(
					'kecamatan_id' => $this->input->post('kecamatan', true),
					'tgl' => $this->input->post('tgl', true),
					'pemilik' => $this->input->post('pemilik', true),
					'no_batch' => $this->input->post('no_batch', true),
					'jenis_hpr' => $this->input->post('jenis_hpr', true),
					'jk' => $this->input->post('jk', true),
					'jumlah' => $this->input->post('jumlah', true),
					'ket' => $this->input->post('ket', true),
				);

				$this->Vaksinasirabies_model->update_vaksinasirabies($id, $params);
				redirect('dinas/vaksinasirabies');
			} else {
				$data['_usedtable'] = TRUE;
				$data['_usedselect2'] = TRUE;
				$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
				$data['_view'] = 'dinas/v_vaksinasirabies/edit';
				$this->load->view('dinas/layouts/main', $data);
			}
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/vaksinasirabies', 'refresh');
		}
	}

	/*
     * Deleting periksa_qurban
     */
	function remove($id)
	{
		$vaksinasirabies = $this->Vaksinasirabies_model->get_vaksinasirabies(array('id' => $id));

		// check if the vaksinasirabies exists before trying to delete it
		if (isset($vaksinasirabies['id'])) {
			$this->Vaksinasirabies_model->delete_vaksinasirabies($id);
			redirect('dinas/vaksinasirabies');
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/vaksinasirabies', 'refresh');
		}
	}
}
