<?php

class Dashboard extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        authenticate();
        has_privileges_admin();

        // $this->load->model('Users_model');
    }

    function index()
    {
        // $data['users'] = $this->Users_model->get_all_users(0, 8);
        $data['_view'] = 'dinas/dashboard';
        $this->load->view('dinas/layouts/main', $data);
    }
}
