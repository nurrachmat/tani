<?php

class Level_user extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        authenticate();
        has_privileges_admin();
        $this->load->model('Level_user_model');
    }

    /*
     * Listing of level_user
     */
    function index()
    {
        $data['_usedtable'] = TRUE;
        $data['level_user'] = $this->Level_user_model->get_all_level_user();

        $data['_view'] = 'dinas/level_user/index';
        $this->load->view('dinas/layouts/main', $data);
    }

    /*
     * Adding a new level_user
     */
    // function add()
    // {
    //     $this->load->library('form_validation');

    //     $this->form_validation->set_rules('nama_level_user', 'Nama level_user', 'required|is_unique[level_user.nama_level_user]');

    //     if ($this->form_validation->run()) {
    //         $params = array(
    //             'nama_level_user' => $this->input->post('nama_level_user'),
    //         );

    //         $level_user_id = $this->Level_user_model->add_level_user($params);
    //         if (isset($level_user_id)) {
    //             m_success("Data berhasil disimpan");
    //         } else {
    //             m_error("Data gagal disimpan");
    //         }
    //         redirect('dinas/level_user/index');
    //     } else {
    //         $data['_view'] = 'dinas/level_user/add';
    //         $this->load->view('dinas/layouts/main', $data);
    //     }
    // }

    /*
     * Editing a level_user
     */
    function edit($id)
    {
        // check if the level_user exists before trying to edit it
        $data['level_user'] = $this->Level_user_model->get_level_user($id);

        if (isset($data['level_user']['id'])) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('nama_level', 'Nama level_user', 'required');

            if ($this->form_validation->run()) {
                $params = array(
                    'nama_level' => $this->input->post('nama_level'),
                );

                $level_user_id = $this->Level_user_model->update_level_user($id, $params);
                if (isset($level_user_id)) {
                    m_success("Data berhasil disimpan");
                } else {
                    m_error("Data gagal disimpan");
                }
                redirect('dinas/level_user/index');
            } else {
                $data['_view'] = 'dinas/level_user/edit';
                $this->load->view('dinas/layouts/main', $data);
            }
        } else
            show_error('The level_user you are trying to edit does not exist.');
    }

    /*
     * Deleting level_user
     */
    // function remove($id)
    // {
    //     $level_user = $this->Level_user_model->get_level_user($id);

    //     // check if the level_user exists before trying to delete it
    //     if (isset($level_user['id'])) {
    //         $result = $this->Level_user_model->delete_level_user($id);
    //         if ($result) {
    //             m_success("Data berhasil dihapus");
    //         } else {
    //             m_error("Data gagal dihapus");
    //         }
    //         redirect('dinas/level_user/index');
    //     } else
    //         show_error('The level_user you are trying to delete does not exist.');
    // }
}
