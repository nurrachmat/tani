<?php

class Kasus_rabies extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		authenticate();
		has_privileges_admin();

		$this->load->model('Kasus_rabies_model');
		$this->load->model('Kecamatan_model');
	}

	/*
     * Listing of periksa_qurban
     */
	function index()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$tahun = $this->input->post('tahun', true);
			$kecamatan = $this->input->post('kecamatan', true);
		} else {
			$tahun = date('Y');
			$kecamatan = 'all';
		}

		if ($kecamatan == 'all') {
			$params = array(
				"tahun" => $tahun,
			);
		} else {
			$params = array(
				"tahun" => $tahun,
				"kecamatan_id" => $kecamatan,
			);
		}

		$data['kasus_rabies'] = $this->Kasus_rabies_model->get_all_kasus_rabies($params);
		$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();

		$data['_usedtable'] = TRUE;
		$data['_usedselect2'] = TRUE;
		$data['_view'] = 'dinas/v_kasus_rabies/index';
		$this->load->view('dinas/layouts/main', $data);
	}

	/*
     * Adding a new periksa_qurban
     */
	function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('tahun', 'Tahun', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
		$this->form_validation->set_rules('status_rabies', 'Status Rabies', 'required');
		$this->form_validation->set_rules('populasi_hpr', 'Populasi Hpr', 'required');
		$this->form_validation->set_rules('real_vaks', 'Real Vaks', 'required');
		$this->form_validation->set_rules('real_elim', 'Real Elim', 'required');
		$this->form_validation->set_rules('jml_spec', 'Jml Spec', 'required');
		$this->form_validation->set_rules('hsl_periksa', 'Hsl Periksa', 'required');
		$this->form_validation->set_rules('ghtr', 'Ghtr', 'required');
		$this->form_validation->set_rules('pemberian', 'Pemberian', 'required');
		$this->form_validation->set_rules('lyssa', 'Lyssa', 'required');

		if ($this->form_validation->run()) {
			$params = array(
				'tahun' => $this->input->post('tahun', true),
				'kecamatan_id' => $this->input->post('kecamatan', true),
				'status_rabies' => $this->input->post('status_rabies', true),
				'populasi_hpr' => $this->input->post('populasi_hpr', true),
				'real_vaks' => $this->input->post('real_vaks', true),
				'real_elim' => $this->input->post('real_elim', true),
				'jml_spec' => $this->input->post('jml_spec', true),
				'hsl_periksa' => $this->input->post('hsl_periksa', true),
				'ghtr' => $this->input->post('ghtr', true),
				'pemberian' => $this->input->post('pemberian', true),
				'lyssa' => $this->input->post('lyssa', true),
			);

			$kasus_rabies_id = $this->Kasus_rabies_model->add_kasus_rabies($params);
			redirect('dinas/kasus_rabies');
		} else {
			$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
			$data['_usedselect2'] = TRUE;
			$data['_usedtable'] = TRUE;
			$data['_view'] = 'dinas/v_kasus_rabies/add';
			$this->load->view('dinas/layouts/main', $data);
		}
	}

	/*
     * Editing a periksa_qurban
     */
	function edit($id)
	{
		// check if the periksa_qurban exists before trying to edit it
		$data['kasus_rabies'] = $this->Kasus_rabies_model->get_kasus_rabies(array('id' => $id));

		if (isset($data['kasus_rabies']['id'])) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
			$this->form_validation->set_rules('tahun', 'Tahun', 'required');
			$this->form_validation->set_rules('status_rabies', 'Status Rabies', 'required');
			$this->form_validation->set_rules('populasi_hpr', 'Populasi Hpr', 'required');
			$this->form_validation->set_rules('real_vaks', 'Real Vaks', 'required');
			$this->form_validation->set_rules('real_elim', 'Real Elim', 'required');
			$this->form_validation->set_rules('jml_spec', 'Jml Spec', 'required');
			$this->form_validation->set_rules('hsl_periksa', 'Hsl Periksa', 'required');
			$this->form_validation->set_rules('ghtr', 'Ghtr', 'required');
			$this->form_validation->set_rules('pemberian', 'Pemberian', 'required');
			$this->form_validation->set_rules('lyssa', 'Lyssa', 'required');

			if ($this->form_validation->run()) {
				$params = array(
					'tahun' => $this->input->post('tahun', true),
					'kecamatan_id' => $this->input->post('kecamatan', true),
					'status_rabies' => $this->input->post('status_rabies', true),
					'populasi_hpr' => $this->input->post('populasi_hpr', true),
					'real_vaks' => $this->input->post('real_vaks', true),
					'real_elim' => $this->input->post('real_elim', true),
					'jml_spec' => $this->input->post('jml_spec', true),
					'hsl_periksa' => $this->input->post('hsl_periksa', true),
					'ghtr' => $this->input->post('ghtr', true),
					'pemberian' => $this->input->post('pemberian', true),
					'lyssa' => $this->input->post('lyssa', true),
				);

				$this->Kasus_rabies_model->update_kasus_rabies($id, $params);
				redirect('dinas/kasus_rabies');
			} else {
				$data['_usedtable'] = TRUE;
				$data['_usedselect2'] = TRUE;
				$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
				$data['_view'] = 'dinas/v_kasus_rabies/edit';
				$this->load->view('dinas/layouts/main', $data);
			}
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/kasus_rabies', 'refresh');
		}
	}

	/*
     * Deleting periksa_qurban
     */
	function remove($id)
	{
		$kasus_rabies = $this->Kasus_rabies_model->get_kasus_rabies(array('id' => $id));

		// check if the kasus_rabies exists before trying to delete it
		if (isset($kasus_rabies['id'])) {
			$this->Kasus_rabies_model->delete_kasus_rabies($id);
			redirect('dinas/kasus_rabies');
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/kasus_rabies', 'refresh');
		}
	}
}
