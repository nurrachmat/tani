<?php

class Gangrep extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		authenticate();
		has_privileges_admin();

		$this->load->model('Gangrep_model');
		$this->load->model('Kecamatan_model');
	}

	/*
     * Listing of periksa_qurban
     */
	function index()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$tgl1 = $this->input->post('tgl1', true);
			$tgl2 = $this->input->post('tgl2', true);
			$kecamatan = $this->input->post('kecamatan', true);
		} else {
			$tgl1 = date('Y-m-01');
			$tgl2 = date('Y-m-d');
			$kecamatan = 'all';
		}

		if ($kecamatan == 'all') {
			$params = array(
				"tgl >=" => $tgl1,
				"tgl <=" => $tgl2,
			);
		} else {
			$params = array(
				"tgl >=" => $tgl1,
				"tgl <=" => $tgl2,
				"kecamatan_id" => $kecamatan,
			);
		}

		$data['gangrep'] = $this->Gangrep_model->get_all_gangrep($params);
		$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();

		$data['_usedtable'] = TRUE;
		$data['_usedselect2'] = TRUE;
		$data['_view'] = 'dinas/v_gangrep/index';
		$this->load->view('dinas/layouts/main', $data);
	}

	/*
     * Adding a new periksa_qurban
     */
	function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('tgl', 'Tgl', 'required');
		$this->form_validation->set_rules('diagnosa', 'Diagnosa', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
		$this->form_validation->set_rules('pemilik', 'Pemilik', 'required');
		$this->form_validation->set_rules('spesies', 'Spesies', 'required');
		$this->form_validation->set_rules('tgl_obat', 'Tgl Obat', 'required');
		$this->form_validation->set_rules('obat', 'Obat', 'required');

		if ($this->form_validation->run()) {
			$params = array(
				'tgl' => $this->input->post('tgl', true),
				'diagnosa' => $this->input->post('diagnosa', true),
				'kecamatan_id' => $this->input->post('kecamatan', true),
				'pemilik' => $this->input->post('pemilik', true),
				'spesies' => $this->input->post('spesies', true),
				'tgl_obat' => $this->input->post('tgl_obat', true),
				'obat' => $this->input->post('obat', true),
			);

			$gangrep_id = $this->Gangrep_model->add_gangrep($params);
			redirect('dinas/gangrep');
		} else {
			$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
			$data['_usedselect2'] = TRUE;
			$data['_usedtable'] = TRUE;
			$data['_view'] = 'dinas/v_gangrep/add';
			$this->load->view('dinas/layouts/main', $data);
		}
	}

	/*
     * Editing a periksa_qurban
     */
	function edit($id)
	{
		// check if the periksa_qurban exists before trying to edit it
		$data['gangrep'] = $this->Gangrep_model->get_gangrep(array('id' => $id));

		if (isset($data['gangrep']['id'])) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('tgl', 'Tgl', 'required');
			$this->form_validation->set_rules('diagnosa', 'Diagnosa', 'required');
			$this->form_validation->set_rules('kecamatan', 'Kecamatan Id', 'required');
			$this->form_validation->set_rules('pemilik', 'Pemilik', 'required');
			$this->form_validation->set_rules('spesies', 'Spesies', 'required');
			$this->form_validation->set_rules('tgl_obat', 'Tgl Obat', 'required');
			$this->form_validation->set_rules('obat', 'Obat', 'required');

			if ($this->form_validation->run()) {
				$params = array(
					'tgl' => $this->input->post('tgl', true),
					'diagnosa' => $this->input->post('diagnosa', true),
					'kecamatan_id' => $this->input->post('kecamatan', true),
					'pemilik' => $this->input->post('pemilik', true),
					'spesies' => $this->input->post('spesies', true),
					'tgl_obat' => $this->input->post('tgl_obat', true),
					'obat' => $this->input->post('obat', true),
				);

				$this->Gangrep_model->update_gangrep($id, $params);
				redirect('dinas/gangrep');
			} else {
				$data['_usedtable'] = TRUE;
				$data['_usedselect2'] = TRUE;
				$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
				$data['_view'] = 'dinas/v_gangrep/edit';
				$this->load->view('dinas/layouts/main', $data);
			}
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/gangrep', 'refresh');
		}
	}

	/*
     * Deleting periksa_qurban
     */
	function remove($id)
	{
		$gangrep = $this->Gangrep_model->get_gangrep(array('id' => $id));

		// check if the gangrep exists before trying to delete it
		if (isset($gangrep['id'])) {
			$this->Gangrep_model->delete_gangrep($id);
			redirect('dinas/gangrep');
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/gangrep', 'refresh');
		}
	}
}
