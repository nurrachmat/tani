<?php

class Users extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        authenticate();
        has_privileges_admin();
        $this->load->model('Users_model');
        $this->load->model('Kategori_user_model');
    }

    /*
     * Listing of users
     */
    function index($level = 0)
    {
        $data['_usedtable'] = TRUE;
        $data['level'] = $level;
        $data['users'] = $this->Users_model->get_all_users($level);
        $data['_view'] = 'dinas/users/index';
        $this->load->view('dinas/layouts/main', $data);
    }

    /*
     * Adding a new users
     */
    function add()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('hp', 'Hp', 'required');
        $this->form_validation->set_rules('kategori_user_id', 'Kategori User', 'required');
        // $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        // $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        // $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
        // $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        // $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');

        if ($this->form_validation->run()) {
            // upload 
            $config['upload_path']          = './uploads/foto/';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['max_size']             = 100000;
            $config['remove_spaces']        = TRUE;

            $new_name = time() . $_FILES['foto']['name'];
            $config['file_name'] = $new_name;
            $config['file_name'] = str_replace(' ', '_', $config['file_name']);

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto')) {
                $params = array(
                    'kategori_user_id' => $this->input->post('kategori_user_id'),
                    'email' => $this->input->post('email'),
                    'nama' => $this->input->post('nama'),
                    // 'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                    // 'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                    // 'tempat_lahir' => $this->input->post('tempat_lahir'),
                    // 'alamat' => $this->input->post('alamat'),
                    'password' => sha1($this->input->post('password')),
                    'hp' => $this->input->post('hp'),
                    'status' => 'Y',
                    'created_at' => date('Y-m-d'),
                    'foto' => $config['file_name']
                );
            } else {
                $params = array(
                    'kategori_user_id' => $this->input->post('kategori_user_id'),
                    'email' => $this->input->post('email'),
                    'nama' => $this->input->post('nama'),
                    // 'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                    // 'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                    // 'tempat_lahir' => $this->input->post('tempat_lahir'),
                    // 'alamat' => $this->input->post('alamat'),
                    'password' => sha1($this->input->post('password')),
                    'hp' => $this->input->post('hp'),
                    'status' => 'Y',
                    'created_at' => date('Y-m-d'),
                    // 'foto' => $config['file_name']
                );
            }

            $users_id = $this->Users_model->add_users($params);

            if (isset($users_id)) {
                m_success("Data user berhasil disimpan");
            } else {
                m_error("Data user gagal disimpan");
            }

            redirect('dinas/users/index/');
        } else {
            $data['_usedselect2'] = TRUE;
            $data['all_kategori_user'] = $this->Kategori_user_model->get_all_kategori_user();
            $data['_view'] = 'dinas/users/add';
            $this->load->view('dinas/layouts/main', $data);
        }
    }

    /*
     * Editing a users
     */
    function edit($id)
    {
        // check if the users exists before trying to edit it
        $data['users'] = $this->Users_model->get_users($id);

        if (isset($data['users']['id'])) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('nama', 'Nama', 'required');
            $this->form_validation->set_rules('hp', 'Hp', 'required');
            $this->form_validation->set_rules('kategori_user_id', 'Kategori User', 'required');
            // $this->form_validation->set_rules('alamat', 'Alamat', 'required');
            // $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
            // $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
            // $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
            // $this->form_validation->set_rules('password', 'Password', 'required');
            // $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');

            if ($this->form_validation->run()) {
                // upload 
                $config['upload_path']          = './uploads/foto/';
                $config['allowed_types']        = 'jpg|jpeg|png';
                $config['max_size']             = 100000;
                $config['remove_spaces']        = TRUE;

                $new_name = time() . $_FILES['foto']['name'];
                $config['file_name'] = $new_name;
                $config['file_name'] = str_replace(' ', '_', $config['file_name']);

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('foto')) {
                    if ($this->input->post('password') == "") {
                        $params = array(
                            'kategori_user_id' => $this->input->post('kategori_user_id'),
                            'email' => $this->input->post('email'),
                            'nama' => $this->input->post('nama'),
                            // 'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                            // 'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                            // 'tempat_lahir' => $this->input->post('tempat_lahir'),
                            // 'alamat' => $this->input->post('alamat'),
                            // 'password' => sha1($this->input->post('password')),
                            'hp' => $this->input->post('hp'),
                            'status' => $this->input->post('status'),
                            'created_at' => date('Y-m-d'),
                            'foto' => $config['file_name']
                        );
                    } else {
                        $params = array(
                            'kategori_user_id' => $this->input->post('kategori_user_id'),
                            'email' => $this->input->post('email'),
                            'nama' => $this->input->post('nama'),
                            // 'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                            // 'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                            // 'tempat_lahir' => $this->input->post('tempat_lahir'),
                            // 'alamat' => $this->input->post('alamat'),
                            'password' => sha1($this->input->post('password')),
                            'hp' => $this->input->post('hp'),
                            'status' => $this->input->post('status'),
                            'created_at' => date('Y-m-d'),
                            'foto' => $config['file_name']
                        );
                    }
                } else {
                    if ($this->input->post('password') == "") {
                        $params = array(
                            'kategori_user_id' => $this->input->post('kategori_user_id'),
                            'email' => $this->input->post('email'),
                            'nama' => $this->input->post('nama'),
                            // 'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                            // 'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                            // 'tempat_lahir' => $this->input->post('tempat_lahir'),
                            // 'alamat' => $this->input->post('alamat'),
                            // 'password' => sha1($this->input->post('password')),
                            'hp' => $this->input->post('hp'),
                            'status' => $this->input->post('status'),
                            'created_at' => date('Y-m-d'),
                            // 'foto' => $config['file_name']
                        );
                    } else {
                        $params = array(
                            'kategori_user_id' => $this->input->post('kategori_user_id'),
                            'email' => $this->input->post('email'),
                            'nama' => $this->input->post('nama'),
                            // 'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                            // 'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                            // 'tempat_lahir' => $this->input->post('tempat_lahir'),
                            // 'alamat' => $this->input->post('alamat'),
                            'password' => sha1($this->input->post('password')),
                            'hp' => $this->input->post('hp'),
                            'status' => $this->input->post('status'),
                            'created_at' => date('Y-m-d'),
                            // 'foto' => $config['file_name']
                        );
                    }
                }

                $users_id = $this->Users_model->update_users($id, $params);

                if (isset($users_id)) {
                    m_success("Data user berhasil disimpan");
                } else {
                    m_error("Data user gagal disimpan");
                }

                redirect('dinas/users/index/');
            } else {
                $data['_usedselect2'] = TRUE;
                $data['all_kategori_user'] = $this->Kategori_user_model->get_all_kategori_user();
                $data['_view'] = 'dinas/users/edit';
                $this->load->view('dinas/layouts/main', $data);
            }
        } else
            show_error('The users you are trying to edit does not exist.');
    }

    function softdelete($id, $status = null)
    {
        $data['users'] = $this->Users_model->get_users($id);

        // check if the ref_jabatan exists before trying to softdelete it
        if (isset($data['users']['id'])) {
            $params = array(
                'status' => $status,
                'modified_at' => date('Y-m-d')
            );

            $users_id = $this->Users_model->update_users($id, $params);
            if (isset($users_id)) {
                if ($status == 'Y') {
                    m_success("Data berhasil diaktifkan");
                } else {
                    m_success("Data berhasil dinonaktifkan");
                }
            } else {
                m_error("Data gagal dinonaktifkan/diaktifkan");
            }
            redirect('dinas/users/index/');
        } else
            show_error('Data yang Anda coba aktifkan/nonaktifkan tidak ada');
    }
}
