<?php

class Kelurahan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		authenticate();
		has_privileges_admin();
		$this->load->model('Kecamatan_model');
		$this->load->model('Kelurahan_model');
	}

	/*
     * Listing of kelurahan
     */
	function index()
	{
		$data['_usedtable'] = TRUE;
		$data['kelurahan'] = $this->Kelurahan_model->get_all_kelurahan();

		$data['_view'] = 'dinas/kelurahan/index';
		$this->load->view('dinas/layouts/main', $data);
	}

	/*
     * Adding a new kelurahan
     */
	function add()
	{
		$data['_usedselect2'] = TRUE;
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nama_kelurahan', 'Nama kelurahan', 'required|is_unique[kelurahan.nama_kelurahan]');
		$this->form_validation->set_rules('kecamatan_id', 'Nama kota/kabupaten', 'required');

		if ($this->form_validation->run()) {
			$params = array(
				'nama_kelurahan' => $this->input->post('nama_kelurahan'),
				'kecamatan_id' => $this->input->post('kecamatan_id'),
			);

			$kelurahan_id = $this->Kelurahan_model->add_kelurahan($params);
			if (isset($kelurahan_id)) {
				m_success("Data berhasil disimpan");
			} else {
				m_error("Data gagal disimpan");
			}
			redirect('dinas/kelurahan/index');
		} else {
			$data['all_kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
			$data['_view'] = 'dinas/kelurahan/add';
			$this->load->view('dinas/layouts/main', $data);
		}
	}

	/*
     * Editing a kelurahan
     */
	function edit($id)
	{
		$data['_usedselect2'] = TRUE;
		// check if the kelurahan exists before trying to edit it
		$data['kelurahan'] = $this->Kelurahan_model->get_kelurahan($id);

		if (isset($data['kelurahan']['id'])) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('nama_kelurahan', 'Nama kelurahan', 'required');
			$this->form_validation->set_rules('kecamatan_id', 'Nama kota/kabupaten', 'required');

			if ($this->form_validation->run()) {
				$params = array(
					'nama_kelurahan' => $this->input->post('nama_kelurahan'),
					'kecamatan_id' => $this->input->post('kecamatan_id'),
				);

				$kelurahan_id = $this->Kelurahan_model->update_kelurahan($id, $params);
				if (isset($kelurahan_id)) {
					m_success("Data berhasil disimpan");
				} else {
					m_error("Data gagal disimpan");
				}
				redirect('dinas/kelurahan/index');
			} else {
				$data['all_kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
				$data['_view'] = 'dinas/kelurahan/edit';
				$this->load->view('dinas/layouts/main', $data);
			}
		} else
			show_error('The kelurahan you are trying to edit does not exist.');
	}

	/*
     * Deleting kelurahan
     */
	function remove($id)
	{
		$kelurahan = $this->Kelurahan_model->get_kelurahan($id);

		// check if the kelurahan exists before trying to delete it
		if (isset($kelurahan['id'])) {
			$result = $this->Kelurahan_model->delete_kelurahan($id);
			if ($result) {
				m_success("Data berhasil dihapus");
			} else {
				m_error("Data gagal dihapus");
			}
			redirect('dinas/kelurahan/index');
		} else
			show_error('The kelurahan you are trying to delete does not exist.');
	}
}
