<?php

class Kotakabupaten extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        authenticate();
        has_privileges_admin();
        $this->load->model('Provinsi_model');
        $this->load->model('Kotakabupaten_model');
    }

    /*
     * Listing of kotakabupaten
     */
    function index()
    {
        $data['_usedtable'] = TRUE;
        $data['kotakabupaten'] = $this->Kotakabupaten_model->get_all_kotakabupaten();

        $data['_view'] = 'dinas/kotakabupaten/index';
        $this->load->view('dinas/layouts/main', $data);
    }

    /*
     * Adding a new kotakabupaten
     */
    function add()
    {
        $data['_usedselect2'] = TRUE;
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nama_kotakabupaten', 'Nama Kotakabupaten', 'required|is_unique[kotakabupaten.nama_kotakabupaten]');
        $this->form_validation->set_rules('provinsi_id', 'Nama Provinsi', 'required');

        if ($this->form_validation->run()) {
            $params = array(
                'nama_kotakabupaten' => $this->input->post('nama_kotakabupaten'),
                'provinsi_id' => $this->input->post('provinsi_id'),
            );

            $kotakabupaten_id = $this->Kotakabupaten_model->add_kotakabupaten($params);
            if (isset($kotakabupaten_id)) {
                m_success("Data berhasil disimpan");
            } else {
                m_error("Data gagal disimpan");
            }
            redirect('dinas/kotakabupaten/index');
        } else {
            $data['all_provinsi'] = $this->Provinsi_model->get_all_provinsi();
            $data['_view'] = 'dinas/kotakabupaten/add';
            $this->load->view('dinas/layouts/main', $data);
        }
    }

    /*
     * Editing a kotakabupaten
     */
    function edit($id)
    {
        $data['_usedselect2'] = TRUE;
        // check if the kotakabupaten exists before trying to edit it
        $data['kotakabupaten'] = $this->Kotakabupaten_model->get_kotakabupaten($id);

        if (isset($data['kotakabupaten']['id'])) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('nama_kotakabupaten', 'Nama Kotakabupaten', 'required');
            $this->form_validation->set_rules('provinsi_id', 'Nama Provinsi', 'required');

            if ($this->form_validation->run()) {
                $params = array(
                    'nama_kotakabupaten' => $this->input->post('nama_kotakabupaten'),
                    'provinsi_id' => $this->input->post('provinsi_id'),
                );

                $kotakabupaten_id = $this->Kotakabupaten_model->update_kotakabupaten($id, $params);
                if (isset($kotakabupaten_id)) {
                    m_success("Data berhasil disimpan");
                } else {
                    m_error("Data gagal disimpan");
                }
                redirect('dinas/kotakabupaten/index');
            } else {
                $data['all_provinsi'] = $this->Provinsi_model->get_all_provinsi();
                $data['_view'] = 'dinas/kotakabupaten/edit';
                $this->load->view('dinas/layouts/main', $data);
            }
        } else
            show_error('The kotakabupaten you are trying to edit does not exist.');
    }

    /*
     * Deleting kotakabupaten
     */
    function remove($id)
    {
        $kotakabupaten = $this->Kotakabupaten_model->get_kotakabupaten($id);

        // check if the kotakabupaten exists before trying to delete it
        if (isset($kotakabupaten['id'])) {
            $result = $this->Kotakabupaten_model->delete_kotakabupaten($id);
            if ($result) {
                m_success("Data berhasil dihapus");
            } else {
                m_error("Data gagal dihapus");
            }
            redirect('dinas/kotakabupaten/index');
        } else
            show_error('The kotakabupaten you are trying to delete does not exist.');
    }
}
