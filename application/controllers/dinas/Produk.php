<?php

class Produk extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		authenticate();
		has_privileges_admin();
		$this->load->model('Kecamatan_model');
		$this->load->model('Kategori_model');
		$this->load->model('Users_model');
		$this->load->model('Produk_model');
	}

	/*
     * Listing of produk
     */
	function index()
	{

		$data['_usedtable'] = TRUE;
		$data['produk'] = $this->Produk_model->find_produk()->result_array();

		$data['_view'] = 'dinas/produk/index';
		$this->load->view('dinas/layouts/main', $data);
	}

	/*
     * Deleting produk
     */
	function softdelete($id, $status = null)
	{
		$data['produk'] = $this->Produk_model->get_produk($id);

		// check if the ref_jabatan exists before trying to softdelete it
		if (isset($data['produk']['id'])) {
			$params = array(
				'status' => $status,
				'modified_at' => date('Y-m-d')
			);

			$produk_id = $this->Produk_model->update_produk($id, $params);
			if (isset($produk_id)) {
				if ($status == 'Y') {
					m_success("Data berhasil diaktifkan");
				} else {
					m_success("Data berhasil dinonaktifkan");
				}
			} else {
				m_error("Data gagal dinonaktifkan/diaktifkan");
			}
			redirect('dinas/produk/index/');
		} else
			show_error('Data yang Anda coba aktifkan/nonaktifkan tidak ada');
	}
}
