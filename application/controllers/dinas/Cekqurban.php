<?php

class Cekqurban extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		authenticate();
		has_privileges_admin();

		$this->load->model('Cekqurban_model');
		$this->load->model('Kecamatan_model');
	}

	/*
     * Listing of periksa_qurban
     */
	function index()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$tgl1 = $this->input->post('tgl1', true);
			$tgl2 = $this->input->post('tgl2', true);
			$kecamatan = $this->input->post('kecamatan', true);
		} else {
			$tgl1 = date('Y-m-01');
			$tgl2 = date('Y-m-d');
			$kecamatan = 'all';
		}

		if ($kecamatan == 'all') {
			$params = array(
				"tgl >=" => $tgl1,
				"tgl <=" => $tgl2,
			);
		} else {
			$params = array(
				"tgl >=" => $tgl1,
				"tgl <=" => $tgl2,
				"kecamatan_id" => $kecamatan,
			);
		}

		$data['cqurban'] = $this->Cekqurban_model->get_all_periksa_qurban($params);
		$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();

		$data['_usedtable'] = TRUE;
		$data['_usedselect2'] = TRUE;
		$data['_view'] = 'dinas/v_periksa_qurban/index';
		$this->load->view('dinas/layouts/main', $data);
	}

	/*
     * Adding a new periksa_qurban
     */
	function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('tgl', 'Tgl', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecapatan', 'required');

		if ($this->form_validation->run()) {
			$params = array(
				'kecamatan_id' => $this->input->post('kecamatan', true),
				'tgl' => $this->input->post('tgl', true),
				'nama' => $this->input->post('nama', true),
				'hp' => $this->input->post('hp', true),
				'sapi' => $this->input->post('sapi', true),
				'kerbau' => $this->input->post('kerbau', true),
				'kambing' => $this->input->post('kambing', true),
				'domba' => $this->input->post('domba', true),
				'ket' => $this->input->post('ket', true),
				'alamat' => $this->input->post('alamat', true),
			);

			$periksa_qurban_id = $this->Cekqurban_model->add_periksa_qurban($params);
			redirect('dinas/cekqurban/index');
		} else {
			$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
			$data['_usedselect2'] = TRUE;
			$data['_usedtable'] = TRUE;
			$data['_view'] = 'dinas/v_periksa_qurban/add';
			$this->load->view('dinas/layouts/main', $data);
		}
	}

	/*
     * Editing a periksa_qurban
     */
	function edit($id)
	{
		// check if the periksa_qurban exists before trying to edit it
		$data['cqurban'] = $this->Cekqurban_model->get_periksa_qurban(array('id' => $id));
		if (isset($data['cqurban']['id'])) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('tgl', 'Tgl', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required');
			$this->form_validation->set_rules('kecamatan', 'Kecapatan', 'required');

			if ($this->form_validation->run()) {
				$params = array(
					'kecamatan_id' => $this->input->post('kecamatan', true),
					'tgl' => $this->input->post('tgl', true),
					'nama' => $this->input->post('nama', true),
					'hp' => $this->input->post('hp', true),
					'sapi' => $this->input->post('sapi', true),
					'kerbau' => $this->input->post('kerbau', true),
					'kambing' => $this->input->post('kambing', true),
					'domba' => $this->input->post('domba', true),
					'ket' => $this->input->post('ket', true),
					'alamat' => $this->input->post('alamat', true),
				);

				$this->Cekqurban_model->update_periksa_qurban($id, $params);
				redirect('dinas/cekqurban/index');
			} else {
				$data['_usedtable'] = TRUE;
				$data['_usedselect2'] = TRUE;
				$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
				$data['_view'] = 'dinas/v_periksa_qurban/edit';
				$this->load->view('dinas/layouts/main', $data);
			}
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/cekqurban', 'refresh');
		}
	}

	/*
     * Deleting periksa_qurban
     */
	function remove($id)
	{
		$periksa_qurban = $this->Cekqurban_model->get_periksa_qurban(array('id' => $id));

		// check if the periksa_qurban exists before trying to delete it
		if (isset($periksa_qurban['id'])) {
			$this->Cekqurban_model->delete_periksa_qurban($id);
			redirect('dinas/cekqurban/index');
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/cekqurban', 'refresh');
		}
	}
}
