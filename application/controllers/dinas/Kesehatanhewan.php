<?php

class Kesehatanhewan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Kesehatanhewan_model');
		$this->load->model('Kecamatan_model');
	}

	/*
     * Listing of periksa_qurban
     */
	function index()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$tgl1 = $this->input->post('tgl1', true);
			$tgl2 = $this->input->post('tgl2', true);
			$kecamatan = $this->input->post('kecamatan', true);
		} else {
			$tgl1 = date('Y-m-01');
			$tgl2 = date('Y-m-d');
			$kecamatan = 'all';
		}

		if ($kecamatan == 'all') {
			$params = array(
				"tgl >=" => $tgl1,
				"tgl <=" => $tgl2,
			);
		} else {
			$params = array(
				"tgl >=" => $tgl1,
				"tgl <=" => $tgl2,
				"kecamatan_id" => $kecamatan,
			);
		}

		$data['khewan'] = $this->Kesehatanhewan_model->get_all_kesehatan_hewan($params);
		$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();

		$data['_usedtable'] = TRUE;
		$data['_usedselect2'] = TRUE;
		$data['_view'] = 'dinas/v_kesehatanhewan/index';
		$this->load->view('dinas/layouts/main', $data);
	}

	/*
     * Adding a new periksa_qurban
     */
	function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('tgl', 'Tgl', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
		$this->form_validation->set_rules('pemilik', 'Pemilik', 'required');
		$this->form_validation->set_rules('jenis', 'Jenis', 'required');
		$this->form_validation->set_rules('jml_hewan', 'Jml Hewan', 'required');
		$this->form_validation->set_rules('pengobatan', 'Pengobatan', 'required');

		if ($this->form_validation->run()) {
			$params = array(
				'tgl' => $this->input->post('tgl'),
				'kecamatan_id' => $this->input->post('kecamatan'),
				'pemilik' => $this->input->post('pemilik'),
				'jenis' => $this->input->post('jenis'),
				'jml_hewan' => $this->input->post('jml_hewan'),
				'gejala' => $this->input->post('gejala'),
				'diagnosa' => $this->input->post('diagnosa'),
				'pengobatan' => $this->input->post('pengobatan'),
				'ket' => $this->input->post('ket'),
			);

			$kesehatan_hewan_id = $this->Kesehatanhewan_model->add_kesehatan_hewan($params);
			redirect('dinas/kesehatanhewan');
		} else {
			$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
			$data['_usedselect2'] = TRUE;
			$data['_usedtable'] = TRUE;
			$data['_view'] = 'dinas/v_kesehatanhewan/add';
			$this->load->view('dinas/layouts/main', $data);
		}
	}

	/*
     * Editing a periksa_qurban
     */
	function edit($id)
	{
		// check if the periksa_qurban exists before trying to edit it
		$data['khewan'] = $this->Kesehatanhewan_model->get_kesehatan_hewan(array('id' => $id));

		if (isset($data['khewan']['id'])) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('tgl', 'Tgl', 'required');
			$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
			$this->form_validation->set_rules('pemilik', 'Pemilik', 'required');
			$this->form_validation->set_rules('jenis', 'Jenis', 'required');
			$this->form_validation->set_rules('jml_hewan', 'Jml Hewan', 'required');
			$this->form_validation->set_rules('pengobatan', 'Pengobatan', 'required');

			if ($this->form_validation->run()) {
				$params = array(
					'tgl' => $this->input->post('tgl'),
					'kecamatan_id' => $this->input->post('kecamatan'),
					'pemilik' => $this->input->post('pemilik'),
					'jenis' => $this->input->post('jenis'),
					'jml_hewan' => $this->input->post('jml_hewan'),
					'gejala' => $this->input->post('gejala'),
					'diagnosa' => $this->input->post('diagnosa'),
					'pengobatan' => $this->input->post('pengobatan'),
					'ket' => $this->input->post('ket'),
				);

				$this->Kesehatanhewan_model->update_kesehatan_hewan($id, $params);
				redirect('dinas/kesehatanhewan');
			} else {
				$data['_usedtable'] = TRUE;
				$data['_usedselect2'] = TRUE;
				$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
				$data['_view'] = 'dinas/v_kesehatanhewan/edit';
				$this->load->view('dinas/layouts/main', $data);
			}
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/kesehatanhewan', 'refresh');
		}
	}

	/*
     * Deleting periksa_qurban
     */
	function remove($id)
	{
		$kesehatan_hewan = $this->Kesehatanhewan_model->get_kesehatan_hewan(array('id' => $id));

		// check if the kesehatan_hewan exists before trying to delete it
		if (isset($kesehatan_hewan['id'])) {
			$this->Kesehatanhewan_model->delete_kesehatan_hewan($id);
			redirect('dinas/kesehatanhewan');
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/kesehatanhewan', 'refresh');
		}
	}
}
