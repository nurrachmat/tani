<?php

class Provinsi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        authenticate();
        has_privileges_admin();
        $this->load->model('Provinsi_model');
    }

    /*
     * Listing of provinsi
     */
    function index()
    {
        $data['_usedtable'] = TRUE;
        $data['provinsi'] = $this->Provinsi_model->get_all_provinsi();

        $data['_view'] = 'dinas/provinsi/index';
        $this->load->view('dinas/layouts/main', $data);
    }

    /*
     * Adding a new provinsi
     */
    function add()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nama_provinsi', 'Nama provinsi', 'required|is_unique[provinsi.nama_provinsi]');

        if ($this->form_validation->run()) {
            $params = array(
                'nama_provinsi' => $this->input->post('nama_provinsi'),
            );

            $provinsi_id = $this->Provinsi_model->add_provinsi($params);
            if (isset($provinsi_id)) {
                m_success("Data berhasil disimpan");
            } else {
                m_error("Data gagal disimpan");
            }
            redirect('dinas/provinsi/index');
        } else {
            $data['_view'] = 'dinas/provinsi/add';
            $this->load->view('dinas/layouts/main', $data);
        }
    }

    /*
     * Editing a provinsi
     */
    function edit($id)
    {
        // check if the provinsi exists before trying to edit it
        $data['provinsi'] = $this->Provinsi_model->get_provinsi($id);

        if (isset($data['provinsi']['id'])) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('nama_provinsi', 'Nama provinsi', 'required');

            if ($this->form_validation->run()) {
                $params = array(
                    'nama_provinsi' => $this->input->post('nama_provinsi'),
                );

                $provinsi_id = $this->Provinsi_model->update_provinsi($id, $params);
                if (isset($provinsi_id)) {
                    m_success("Data berhasil disimpan");
                } else {
                    m_error("Data gagal disimpan");
                }
                redirect('dinas/provinsi/index');
            } else {
                $data['_view'] = 'dinas/provinsi/edit';
                $this->load->view('dinas/layouts/main', $data);
            }
        } else
            show_error('The provinsi you are trying to edit does not exist.');
    }

    /*
     * Deleting provinsi
     */
    function remove($id)
    {
        $provinsi = $this->Provinsi_model->get_provinsi($id);

        // check if the provinsi exists before trying to delete it
        if (isset($provinsi['id'])) {
            $result = $this->Provinsi_model->delete_provinsi($id);
            if ($result) {
                m_success("Data berhasil dihapus");
            } else {
                m_error("Data gagal dihapus");
            }
            redirect('dinas/provinsi/index');
        } else
            show_error('The provinsi you are trying to delete does not exist.');
    }
}
