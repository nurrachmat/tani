<?php

class Vaksinasijembrana extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		authenticate();
		has_privileges_admin();

		$this->load->model('Vaksinasijembrana_model');
		$this->load->model('Kecamatan_model');
	}

	/*
     * Listing of periksa_qurban
     */
	function index()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$tgl1 = $this->input->post('tgl1', true);
			$tgl2 = $this->input->post('tgl2', true);
			$kecamatan = $this->input->post('kecamatan', true);
		} else {
			$tgl1 = date('Y-m-01');
			$tgl2 = date('Y-m-d');
			$kecamatan = 'all';
		}

		if ($kecamatan == 'all') {
			$params = array(
				"tgl >=" => $tgl1,
				"tgl <=" => $tgl2,
			);
		} else {
			$params = array(
				"tgl >=" => $tgl1,
				"tgl <=" => $tgl2,
				"kecamatan_id" => $kecamatan,
			);
		}

		$data['vaksinasijembrana'] = $this->Vaksinasijembrana_model->get_all_vaksinasijembrana($params);
		$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();

		$data['_usedtable'] = TRUE;
		$data['_usedselect2'] = TRUE;
		$data['_view'] = 'dinas/v_vaksinasijembrana/index';
		$this->load->view('dinas/layouts/main', $data);
	}

	/*
     * Adding a new periksa_qurban
     */
	function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('tgl', 'Tgl', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
		$this->form_validation->set_rules('pemilik', 'Pemilik', 'required');
		$this->form_validation->set_rules('jantan', 'Jantan', 'required');
		$this->form_validation->set_rules('betina', 'Betina', 'required');

		if ($this->form_validation->run()) {
			$params = array(
				'tgl' => $this->input->post('tgl', true),
				'kecamatan_id' => $this->input->post('kecamatan', true),
				'pemilik' => $this->input->post('pemilik', true),
				'jantan' => $this->input->post('jantan', true),
				'betina' => $this->input->post('betina', true),
			);

			$vaksinasijembrana_id = $this->Vaksinasijembrana_model->add_vaksinasijembrana($params);
			redirect('dinas/vaksinasijembrana');
		} else {
			$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
			$data['_usedselect2'] = TRUE;
			$data['_usedtable'] = TRUE;
			$data['_view'] = 'dinas/v_vaksinasijembrana/add';
			$this->load->view('dinas/layouts/main', $data);
		}
	}

	/*
     * Editing a periksa_qurban
     */
	function edit($id)
	{
		// check if the periksa_qurban exists before trying to edit it
		$data['vaksinasijembrana'] = $this->Vaksinasijembrana_model->get_vaksinasijembrana(array('id' => $id));

		if (isset($data['vaksinasijembrana']['id'])) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('tgl', 'Tgl', 'required');
			$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
			$this->form_validation->set_rules('pemilik', 'Pemilik', 'required');
			$this->form_validation->set_rules('jantan', 'Jantan', 'required');
			$this->form_validation->set_rules('betina', 'Betina', 'required');

			if ($this->form_validation->run()) {
				$params = array(
					'tgl' => $this->input->post('tgl', true),
					'kecamatan_id' => $this->input->post('kecamatan', true),
					'pemilik' => $this->input->post('pemilik', true),
					'jantan' => $this->input->post('jantan', true),
					'betina' => $this->input->post('betina', true),
				);

				$this->Vaksinasijembrana_model->update_vaksinasijembrana($id, $params);
				redirect('dinas/vaksinasijembrana');
			} else {
				$data['_usedtable'] = TRUE;
				$data['_usedselect2'] = TRUE;
				$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
				$data['_view'] = 'dinas/v_vaksinasijembrana/edit';
				$this->load->view('dinas/layouts/main', $data);
			}
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/vaksinasijembrana', 'refresh');
		}
	}

	/*
     * Deleting periksa_qurban
     */
	function remove($id)
	{
		$vaksinasijembrana = $this->Vaksinasijembrana_model->get_vaksinasijembrana(array('id' => $id));

		// check if the vaksinasijembrana exists before trying to delete it
		if (isset($vaksinasijembrana['id'])) {
			$this->Vaksinasijembrana_model->delete_vaksinasijembrana($id);
			redirect('dinas/vaksinasijembrana');
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/vaksinasijembrana', 'refresh');
		}
	}
}
