<?php

class Laporan_kelahiran extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		authenticate();
		has_privileges_admin();

		$this->load->model('Laporan_kelahiran_model');
		$this->load->model('Kecamatan_model');
	}

	/*
     * Listing of Laporan_kelahiran
     */
	function index()
	{
		$data['laporan_kelahiran'] = $this->Laporan_kelahiran_model->get_all_laporan_kelahiran();

		$data['_usedtable'] = TRUE;
		$data['_usedselect2'] = TRUE;
		$data['_view'] = 'dinas/v_laporan_kelahiran/index';
		$this->load->view('dinas/layouts/main', $data);
	}

	/*
     * Adding a new Laporan_kelahiran
     */
	function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('telppetugas', 'telppetugas', 'required');
		$this->form_validation->set_rules('nmpeternak', 'Nmpeternak', 'required');
		$this->form_validation->set_rules('idpeternak', 'Idpeternak', 'required');
		$this->form_validation->set_rules('hppeternak', 'Hppeternak', 'required|numeric|min_length[11]|max_length[14]');
		$this->form_validation->set_rules('idenhewan', 'Idenhewan', 'required');
		$this->form_validation->set_rules('nmpedet', 'Nmpedet', 'required');
		$this->form_validation->set_rules('jk', 'Jk', 'required');
		$this->form_validation->set_rules('tgllahir', 'Tgllahir', 'required');
		$this->form_validation->set_rules('lokasi', 'Lokasi', 'required');

		if ($this->form_validation->run()) {
			$params = array(
				'telppetugas' => $this->input->post('telppetugas', true),
				'nmpeternak' => $this->input->post('nmpeternak', true),
				'idpeternak' => $this->input->post('idpeternak', true),
				'hppeternak' => $this->input->post('hppeternak', true),
				'idenhewan' => $this->input->post('idenhewan', true),
				'nmpedet' => $this->input->post('nmpedet', true),
				'jk' => $this->input->post('jk', true),
				'tgllahir' => $this->input->post('tgllahir', true),
				'lokasi' => $this->input->post('lokasi', true),
			);

			$laporan_kelahiran_id = $this->Laporan_kelahiran_model->add_laporan_kelahiran($params);
			redirect('dinas/laporan_kelahiran');
		} else {
			$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
			$data['_usedselect2'] = TRUE;
			$data['_usedtable'] = TRUE;
			$data['_view'] = 'dinas/v_laporan_kelahiran/add';
			$this->load->view('dinas/layouts/main', $data);
		}
	}

	/*
     * Editing a Laporan_kelahiran
     */
	function edit($id)
	{
		// check if the Laporan_kelahiran exists before trying to edit it
		$data['laporan_kelahiran'] = $this->Laporan_kelahiran_model->get_laporan_kelahiran(array('id' => $id));

		if (isset($data['laporan_kelahiran']['id'])) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('telppetugas', 'telppetugas', 'required');
			$this->form_validation->set_rules('nmpeternak', 'Nmpeternak', 'required');
			$this->form_validation->set_rules('idpeternak', 'Idpeternak', 'required');
			$this->form_validation->set_rules('hppeternak', 'Hppeternak', 'required|numeric|min_length[11]|max_length[14]');
			$this->form_validation->set_rules('idenhewan', 'Idenhewan', 'required');
			$this->form_validation->set_rules('nmpedet', 'Nmpedet', 'required');
			$this->form_validation->set_rules('jk', 'Jk', 'required');
			$this->form_validation->set_rules('tgllahir', 'Tgllahir', 'required');
			$this->form_validation->set_rules('lokasi', 'Lokasi', 'required');

			if ($this->form_validation->run()) {
				$params = array(
					'telppetugas' => $this->input->post('telppetugas', true),
					'nmpeternak' => $this->input->post('nmpeternak', true),
					'idpeternak' => $this->input->post('idpeternak', true),
					'hppeternak' => $this->input->post('hppeternak', true),
					'idenhewan' => $this->input->post('idenhewan', true),
					'nmpedet' => $this->input->post('nmpedet', true),
					'jk' => $this->input->post('jk', true),
					'tgllahir' => $this->input->post('tgllahir', true),
					'lokasi' => $this->input->post('lokasi', true),
				);

				$this->Laporan_kelahiran_model->update_laporan_kelahiran($id, $params);
				redirect('dinas/laporan_kelahiran');
			} else {
				$data['_usedtable'] = TRUE;
				$data['_usedselect2'] = TRUE;
				$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
				$data['_view'] = 'dinas/v_laporan_kelahiran/edit';
				$this->load->view('dinas/layouts/main', $data);
			}
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/laporan_kelahiran', 'refresh');
		}
	}

	/*
     * Deleting Laporan_kelahiran
     */
	function remove($id)
	{
		$laporan_kelahiran = $this->Laporan_kelahiran_model->get_laporan_kelahiran(array('id' => $id));

		// check if the laporan_kelahiran exists before trying to delete it
		if (isset($laporan_kelahiran['id'])) {
			$this->Laporan_kelahiran_model->delete_laporan_kelahiran($id);
			redirect('dinas/laporan_kelahiran');
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/laporan_kelahiran', 'refresh');
		}
	}
}
