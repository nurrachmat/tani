<?php

class Luas_areal_produksi extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		authenticate();
		has_privileges_admin();
		$this->load->model('Luas_areal_produksi_model');
		$this->load->model('Kelurahan_model');
		$this->load->model('Kotakabupaten_model');
	}

	/*
     * Listing of luas_areal_produksi
     */
	function index()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$tahun = $this->input->post('tahun', true);
			$kotakabupaten = $this->input->post('kotakabupaten', true);
		} else {
			$tahun = date('Y');
			$kotakabupaten = 'all';
		}

		if ($kotakabupaten == 'all') {
			$params = array(
				"tahun" => $tahun,
			);
		} else {
			$params = array(
				"tahun" => $tahun,
				"kotakabupaten_id" => $kotakabupaten,
			);
		}

		$data['luas_areal_produksi'] = $this->Luas_areal_produksi_model->get_all($params);
		$data['kotakabupaten'] = $this->Kotakabupaten_model->get_all_kotakabupaten();

		$data['_usedtable'] = TRUE;
		$data['_usedselect2'] = TRUE;
		$data['_view'] = 'dinas/v_luas_areal_produksi/index';
		$this->load->view('dinas/layouts/main', $data);
	}

	function angka_tetap($tahun = null, $kotakabupaten_id = null)
	{
		$data['luas_areal_produksi_angka_tetap'] = $this->Luas_areal_produksi_model->get_all_angka_tetap($tahun, $kotakabupaten_id)->result_array();
		$data['tahun'] = $tahun;
		$data['kotakabupaten'] = $this->Kotakabupaten_model->get_kotakabupaten($kotakabupaten_id);

		$this->load->view('dinas/v_luas_areal_produksi/angka_tetap', $data);
	}

	/*
     * Adding a new luas_areal_produksi
     */
	function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('tahun', 'Tahun', 'required');
		$this->form_validation->set_rules('kelurahan', 'kelurahan', 'required');
		$this->form_validation->set_rules('komoditi', 'Komoditi', 'required');
		$this->form_validation->set_rules('tbm', 'TBM', 'required');
		$this->form_validation->set_rules('tm', 'TM', 'required');
		$this->form_validation->set_rules('tt_tr', 'TT/TR', 'required');
		$this->form_validation->set_rules('produksi', 'Produksi', 'required');
		$this->form_validation->set_rules('produktifitas', 'Produktifitas', 'required');
		$this->form_validation->set_rules('krt', 'KRT', 'required');
		$this->form_validation->set_rules('kt', 'KT', 'required');

		if ($this->form_validation->run()) {
			$params = array(
				'tahun' => $this->input->post('tahun'),
				'kelurahan_id' => $this->input->post('kelurahan'),
				'komoditi' => $this->input->post('komoditi'),
				'tbm' => $this->input->post('tbm'),
				'tm' => $this->input->post('tm'),
				'tt_tr' => $this->input->post('tt_tr'),
				'produksi' => $this->input->post('produksi'),
				'produktifitas' => $this->input->post('produktifitas'),
				'krt' => $this->input->post('krt'),
				'kt' => $this->input->post('kt'),
			);

			$luas_areal_produksi_id = $this->Luas_areal_produksi_model->add($params);
			if (isset($luas_areal_produksi_id)) {
				m_success("Data berhasil disimpan");
			} else {
				m_error("Data gagal disimpan");
			}
			redirect('dinas/luas_areal_produksi');
		} else {
			$data['kelurahan'] = $this->Kelurahan_model->get_all_kelurahan();
			$data['_usedselect2'] = TRUE;
			$data['_usedtable'] = TRUE;
			$data['_view'] = 'dinas/v_luas_areal_produksi/add';
			$this->load->view('dinas/layouts/main', $data);
		}
	}

	/*
     * Editing a luas_areal_produksi
     */
	function edit($id)
	{
		// check if the luas_areal_produksi exists before trying to edit it
		$data['luas_areal_produksi'] = $this->Luas_areal_produksi_model->get(array('id' => $id));

		if (isset($data['luas_areal_produksi']['id'])) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('tahun', 'Tahun', 'required');
			$this->form_validation->set_rules('kelurahan', 'kelurahan', 'required');
			$this->form_validation->set_rules('komoditi', 'Komoditi', 'required');
			$this->form_validation->set_rules('tbm', 'TBM', 'required');
			$this->form_validation->set_rules('tm', 'TM', 'required');
			$this->form_validation->set_rules('tt_tr', 'TT/TR', 'required');
			$this->form_validation->set_rules('produksi', 'Produksi', 'required');
			$this->form_validation->set_rules('produktifitas', 'Produktifitas', 'required');
			$this->form_validation->set_rules('krt', 'KRT', 'required');
			$this->form_validation->set_rules('kt', 'KT', 'required');

			if ($this->form_validation->run()) {
				$params = array(
					'tahun' => $this->input->post('tahun'),
					'kelurahan_id' => $this->input->post('kelurahan'),
					'komoditi' => $this->input->post('komoditi'),
					'tbm' => $this->input->post('tbm'),
					'tm' => $this->input->post('tm'),
					'tt_tr' => $this->input->post('tt_tr'),
					'produksi' => $this->input->post('produksi'),
					'produktifitas' => $this->input->post('produktifitas'),
					'krt' => $this->input->post('krt'),
					'kt' => $this->input->post('kt'),
				);

				$luas_areal_produksi_id = $this->Luas_areal_produksi_model->update($id, $params);
				if (isset($luas_areal_produksi_id)) {
					m_success("Data berhasil diubah");
				} else {
					m_error("Data gagal diubah");
				}
				redirect('dinas/luas_areal_produksi');
			} else {
				$data['_usedtable'] = TRUE;
				$data['_usedselect2'] = TRUE;
				$data['kelurahan'] = $this->Kelurahan_model->get_all_kelurahan();
				$data['_view'] = 'dinas/v_luas_areal_produksi/edit';
				$this->load->view('dinas/layouts/main', $data);
			}
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/luas_areal_produksi', 'refresh');
		}
	}

	/*
     * Deleting luas_areal_produksi
     */
	function remove($id)
	{
		$luas_areal_produksi = $this->Luas_areal_produksi_model->get(array('id' => $id));

		// check if the luas_areal_produksi exists before trying to delete it
		if (isset($luas_areal_produksi['id'])) {
			$luas_areal_produksi_id = $this->Luas_areal_produksi_model->delete($id);
			if (isset($luas_areal_produksi_id)) {
				m_success("Data berhasil dihapus");
			} else {
				m_error("Data gagal dihapus");
			}
			redirect('dinas/luas_areal_produksi');
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/luas_areal_produksi', 'refresh');
		}
	}
}
