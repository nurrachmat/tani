<?php

class Kategori extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        authenticate();
        has_privileges_admin();
        $this->load->model('Kategori_model');
    }

    /*
     * Listing of kategori
     */
    function index()
    {
        $data['_usedtable'] = TRUE;
        $data['kategori'] = $this->Kategori_model->get_all_kategori();

        $data['_view'] = 'dinas/kategori/index';
        $this->load->view('dinas/layouts/main', $data);
    }

    /*
     * Adding a new kategori
     */
    function add()
    {
        $data['_usedselect2'] = TRUE;
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nama_kategori', 'Nama kategori', 'required|is_unique[kategori.nama_kategori]');

        if ($this->form_validation->run()) {
            // upload 
            $config['upload_path']          = './uploads/kategori/';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['max_size']             = 100000;
            $config['remove_spaces']        = TRUE;

            $new_name = time() . $_FILES['foto']['name'];
            $config['file_name'] = $new_name;
            $config['file_name'] = str_replace(' ', '_', $config['file_name']);

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto')) {
                $params = array(
                    'nama_kategori' => $this->input->post('nama_kategori'),
                    'parent' => $this->input->post('parent'),
                    'status' => $this->input->post('status'),
                    'deskripsi' => $this->input->post('deskripsi'),
                    'foto' => $config['file_name']
                );
            } else {
                $params = array(
                    'nama_kategori' => $this->input->post('nama_kategori'),
                    'parent' => $this->input->post('parent'),
                    'status' => $this->input->post('status'),
                    'deskripsi' => $this->input->post('deskripsi'),
                );
            }

            $kategori_id = $this->Kategori_model->add_kategori($params);
            if (isset($kategori_id)) {
                m_success("Data berhasil disimpan");
            } else {
                m_error("Data gagal disimpan");
            }
            redirect('dinas/kategori/index');
        } else {
            $data['all_kategori'] = $this->Kategori_model->get_all_kategori();
            $data['_view'] = 'dinas/kategori/add';
            $this->load->view('dinas/layouts/main', $data);
        }
    }

    /*
     * Editing a kategori
     */
    function edit($id)
    {
        $data['_usedselect2'] = TRUE;
        // check if the kategori exists before trying to edit it
        $data['kategori'] = $this->Kategori_model->get_kategori($id);

        if (isset($data['kategori']['id'])) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('nama_kategori', 'Nama kategori', 'required');

            if ($this->form_validation->run()) {
                // upload 
                $config['upload_path']          = './uploads/kategori/';
                $config['allowed_types']        = 'jpg|jpeg|png';
                $config['max_size']             = 100000;
                $config['remove_spaces']        = TRUE;

                $new_name = time() . $_FILES['foto']['name'];
                $config['file_name'] = $new_name;
                $config['file_name'] = str_replace(' ', '_', $config['file_name']);

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('foto')) {
                    $params = array(
                        'nama_kategori' => $this->input->post('nama_kategori'),
                        'parent' => $this->input->post('parent'),
                        'deskripsi' => $this->input->post('deskripsi'),
                        'foto' => $config['file_name']
                    );
                } else {
                    $params = array(
                        'nama_kategori' => $this->input->post('nama_kategori'),
                        'parent' => $this->input->post('parent'),
                        'deskripsi' => $this->input->post('deskripsi'),
                    );
                }


                $kategori_id = $this->Kategori_model->update_kategori($id, $params);
                if (isset($kategori_id)) {
                    m_success("Data berhasil disimpan");
                } else {
                    m_error("Data gagal disimpan");
                }
                redirect('dinas/kategori/index');
            } else {
                $data['all_kategori'] = $this->Kategori_model->get_all_kategori();
                $data['_view'] = 'dinas/kategori/edit';
                $this->load->view('dinas/layouts/main', $data);
            }
        } else
            show_error('The kategori you are trying to edit does not exist.');
    }

    /*
     * Deleting kategori
     */
    function remove($id)
    {
        $kategori = $this->Kategori_model->get_kategori($id);

        // check if the kategori exists before trying to delete it
        if (isset($kategori['id'])) {
            $result = $this->Kategori_model->delete_kategori($id);
            if ($result) {
                m_success("Data berhasil dihapus");
            } else {
                m_error("Data gagal dihapus");
            }
            redirect('dinas/kategori/index');
        } else
            show_error('The kategori you are trying to delete does not exist.');
    }
}
