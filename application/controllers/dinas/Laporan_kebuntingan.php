<?php

class Laporan_kebuntingan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		authenticate();
		has_privileges_admin();

		$this->load->model('Laporan_kebuntingan_model');
		$this->load->model('Kecamatan_model');
	}

	/*
     * Listing of laporan_kebuntingan
     */
	function index()
	{
		$data['laporan_kebuntingan'] = $this->Laporan_kebuntingan_model->get_all_laporan_kebuntingan();

		$data['_usedtable'] = TRUE;
		$data['_usedselect2'] = TRUE;
		$data['_view'] = 'dinas/v_laporan_kebuntingan/index';
		$this->load->view('dinas/layouts/main', $data);
	}

	/*
     * Adding a new laporan_kebuntingan
     */
	function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('tglpkb', 'Tglpkb', 'required');
		$this->form_validation->set_rules('idpeternak', 'Idpeternak', 'required');
		$this->form_validation->set_rules('nmpeternak', 'Nmpeternak', 'required');
		$this->form_validation->set_rules('idenhewan', 'Idenhewan', 'required');
		$this->form_validation->set_rules('spesies', 'Spesies', 'required');
		$this->form_validation->set_rules('umurbunting', 'Umurbunting', 'required');
		$this->form_validation->set_rules('telppetugas', 'Telppetugas', 'required|min_length[10]');
		$this->form_validation->set_rules('ket', 'Ket', 'required');

		if ($this->form_validation->run()) {
			$params = array(
				'tglpkb' => $this->input->post('tglpkb', true),
				'idpeternak' => $this->input->post('idpeternak', true),
				'nmpeternak' => $this->input->post('nmpeternak', true),
				'idenhewan' => $this->input->post('idenhewan', true),
				'spesies' => $this->input->post('spesies', true),
				'umurbunting' => $this->input->post('umurbunting', true),
				'telppetugas' => $this->input->post('telppetugas', true),
				'ket' => $this->input->post('ket', true),
			);

			$laporan_kebuntingan_id = $this->Laporan_kebuntingan_model->add_laporan_kebuntingan($params);
			redirect('dinas/laporan_kebuntingan');
		} else {
			$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
			$data['_usedselect2'] = TRUE;
			$data['_usedtable'] = TRUE;
			$data['_view'] = 'dinas/v_laporan_kebuntingan/add';
			$this->load->view('dinas/layouts/main', $data);
		}
	}

	/*
     * Editing a laporan_kebuntingan
     */
	function edit($id)
	{
		// check if the laporan_kebuntingan exists before trying to edit it
		$data['laporan_kebuntingan'] = $this->Laporan_kebuntingan_model->get_laporan_kebuntingan(array('id' => $id));

		if (isset($data['laporan_kebuntingan']['id'])) {
			$this->load->library('form_validation');

			$this->load->library('form_validation');

			$this->form_validation->set_rules('tglpkb', 'Tglpkb', 'required');
			$this->form_validation->set_rules('idpeternak', 'Idpeternak', 'required');
			$this->form_validation->set_rules('nmpeternak', 'Nmpeternak', 'required');
			$this->form_validation->set_rules('idenhewan', 'Idenhewan', 'required');
			$this->form_validation->set_rules('spesies', 'Spesies', 'required');
			$this->form_validation->set_rules('umurbunting', 'Umurbunting', 'required');
			$this->form_validation->set_rules('telppetugas', 'Telppetugas', 'required|min_length[10]');
			$this->form_validation->set_rules('ket', 'Ket', 'required');

			if ($this->form_validation->run()) {
				$params = array(
					'tglpkb' => $this->input->post('tglpkb', true),
					'idpeternak' => $this->input->post('idpeternak', true),
					'nmpeternak' => $this->input->post('nmpeternak', true),
					'idenhewan' => $this->input->post('idenhewan', true),
					'spesies' => $this->input->post('spesies', true),
					'umurbunting' => $this->input->post('umurbunting', true),
					'telppetugas' => $this->input->post('telppetugas', true),
					'ket' => $this->input->post('ket', true),
				);

				$this->Laporan_kebuntingan_model->update_laporan_kebuntingan($id, $params);
				redirect('dinas/laporan_kebuntingan');
			} else {
				$data['_usedtable'] = TRUE;
				$data['_usedselect2'] = TRUE;
				$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
				$data['_view'] = 'dinas/v_laporan_kebuntingan/edit';
				$this->load->view('dinas/layouts/main', $data);
			}
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/laporan_kebuntingan', 'refresh');
		}
	}

	/*
     * Deleting laporan_kebuntingan
     */
	function remove($id)
	{
		$laporan_kebuntingan = $this->Laporan_kebuntingan_model->get_laporan_kebuntingan(array('id' => $id));

		// check if the laporan_kebuntingan exists before trying to delete it
		if (isset($laporan_kebuntingan['id'])) {
			$this->Laporan_kebuntingan_model->delete_laporan_kebuntingan($id);
			redirect('dinas/laporan_kebuntingan');
		} else {
			m_error('Data tidak ditemukan');
			redirect('dinas/laporan_kebuntingan', 'refresh');
		}
	}
}
