<?php

class Kecamatan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        authenticate();
        has_privileges_admin();
        $this->load->model('Kotakabupaten_model');
        $this->load->model('Kecamatan_model');
    }

    /*
     * Listing of kecamatan
     */
    function index()
    {
        $data['_usedtable'] = TRUE;
        $data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();

        $data['_view'] = 'dinas/kecamatan/index';
        $this->load->view('dinas/layouts/main', $data);
    }

    /*
     * Adding a new kecamatan
     */
    function add()
    {
        $data['_usedselect2'] = TRUE;
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nama_kecamatan', 'Nama kecamatan', 'required|is_unique[kecamatan.nama_kecamatan]');
        $this->form_validation->set_rules('kotakabupaten_id', 'Nama kota/kabupaten', 'required');

        if ($this->form_validation->run()) {
            $params = array(
                'nama_kecamatan' => $this->input->post('nama_kecamatan'),
                'kotakabupaten_id' => $this->input->post('kotakabupaten_id'),
            );

            $kecamatan_id = $this->Kecamatan_model->add_kecamatan($params);
            if (isset($kecamatan_id)) {
                m_success("Data berhasil disimpan");
            } else {
                m_error("Data gagal disimpan");
            }
            redirect('dinas/kecamatan/index');
        } else {
            $data['all_kotakabupaten'] = $this->Kotakabupaten_model->get_all_kotakabupaten();
            $data['_view'] = 'dinas/kecamatan/add';
            $this->load->view('dinas/layouts/main', $data);
        }
    }

    /*
     * Editing a kecamatan
     */
    function edit($id)
    {
        $data['_usedselect2'] = TRUE;
        // check if the kecamatan exists before trying to edit it
        $data['kecamatan'] = $this->Kecamatan_model->get_kecamatan($id);

        if (isset($data['kecamatan']['id'])) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('nama_kecamatan', 'Nama kecamatan', 'required');
            $this->form_validation->set_rules('kotakabupaten_id', 'Nama kota/kabupaten', 'required');

            if ($this->form_validation->run()) {
                $params = array(
                    'nama_kecamatan' => $this->input->post('nama_kecamatan'),
                    'kotakabupaten_id' => $this->input->post('kotakabupaten_id'),
                );

                $kecamatan_id = $this->Kecamatan_model->update_kecamatan($id, $params);
                if (isset($kecamatan_id)) {
                    m_success("Data berhasil disimpan");
                } else {
                    m_error("Data gagal disimpan");
                }
                redirect('dinas/kecamatan/index');
            } else {
                $data['all_kotakabupaten'] = $this->Kotakabupaten_model->get_all_kotakabupaten();
                $data['_view'] = 'dinas/kecamatan/edit';
                $this->load->view('dinas/layouts/main', $data);
            }
        } else
            show_error('The kecamatan you are trying to edit does not exist.');
    }

    /*
     * Deleting kecamatan
     */
    function remove($id)
    {
        $kecamatan = $this->Kecamatan_model->get_kecamatan($id);

        // check if the kecamatan exists before trying to delete it
        if (isset($kecamatan['id'])) {
            $result = $this->Kecamatan_model->delete_kecamatan($id);
            if ($result) {
                m_success("Data berhasil dihapus");
            } else {
                m_error("Data gagal dihapus");
            }
            redirect('dinas/kecamatan/index');
        } else
            show_error('The kecamatan you are trying to delete does not exist.');
    }
}
