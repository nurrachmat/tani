<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		auth_host();
		$this->load->model('Users_model');

		//Do your magic here
	}

	public function index()
	{
		$data['profile'] = $this->Users_model->find_simple_user(array("email" => user_data('email_user'), "status" => 'Y', 'level_user_id >=' => 3));
		if (isset($data['profile']['id'])) {
			$msg_vaild = array(
				'required' => '%s Harus diisi',
				'valid_email' => '%s Harus format email',
				'min_length' => '{field} Minimal {param} characters.'
			);
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email', $msg_vaild);
			$this->form_validation->set_rules('nama', 'Nama', 'required', $msg_vaild);
			$this->form_validation->set_rules('hp', 'HP', 'required|numeric', $msg_vaild);
			$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required', $msg_vaild);
			if ($this->input->post('passblm', TRUE)) {
				$this->form_validation->set_rules('passblm', 'Password Saat ini', 'required|callback_pass_check', $msg_vaild);
				$this->form_validation->set_rules('passbaru', 'Password Baru', 'required|min_length[8]', $msg_vaild);
				$this->form_validation->set_rules('konfpassbaru', 'Konfirmasi Password Baru', 'required|matches[passbaru]', $msg_vaild);
			}

			if ($this->form_validation->run()) {

				if (!empty($_FILES['photo'])) {
					$new_name = time() . $_FILES['photo']['name'];
					$config['upload_path']          = './uploads/foto/';
					$config['allowed_types']        = 'jpg|jpeg|png';
					$config['max_size']             = 100000;
					$config['remove_spaces']        = TRUE;
					$config['file_name'] = str_replace(' ', '_', $new_name);


					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('photo')) {
						$data = array('error' => $this->upload->display_errors());

						$this->index();
					} else {
						$file = $this->upload->data();
						$file_done = $file['file_name'];
					}
				} else {
					$file_done = $data['profile']['foto'];
				}
				$val = array(
					'email' => $this->input->post('email', TRUE),
					'nama' => $this->input->post('nama', TRUE),
					'hp' => $this->input->post('hp', TRUE),
					'Deskripsi' => $this->input->post('deskripsi', TRUE),
					'foto' => $file_done
				);

				$update = $this->Users_model->update_users(user_data('id_user'), $val);
				if ($this->input->post('passblm', TRUE)) {
					$update = $this->Users_model->update_users(user_data('id_user'), array('password' => sha1($this->input->post('passbaru', TRUE))));
				}

				m_success("Berhasil diperbarui");
				redirect('profile', 'refresh');
			} else {
				$data['_view'] = 'profile';
				$this->load->view('layouts/main', $data);
			}
		} else {
			redirect('', 'refresh');
		}
	}

	function pass_check($str)
	{
		$cek_login = $this->Users_model->find_simple_user(array("email" => user_data('email_user'), "password" => sha1($str), "status" => 'Y', 'level_user_id >=' => 3));

		if (isset($cek_login['id'])) {
			return TRUE;
		} else {
			$this->form_validation->set_message('pass_check', 'Password Saat ini Anda tidak Cocok');
			return FALSE;
		}
	}
}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */
