<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Favorit extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Favorit_model');
		$this->load->model('Produk_model');
		$this->load->model('Users_model');
		$this->load->model('Produk_fav_model');

		auth_host();

		$this->encryption->initialize(
			array(
				'cipher' => 'aes-256',
				'mode' => 'ctr',
				'key' => 'b5ad27baace26332abb1884c1fc68091'
			)
		);
	}

	public function index($id = 1)
	{
		$per_page = ($id == 1) ? 0 : $id;
		$limit = RECORDS_PER_PAGE;
		$params['limit'] = $limit;
		$params['offset'] = $per_page;

		$params2 = array('produk_favorit.user_id' => user_data('id_user'));

		$this->load->library('pagination');
		$config = $this->config();
		$config['base_url'] = site_url('favorit/index');
		$config['total_rows'] = $this->Favorit_model->get_all_produk_count($params2);
		$config['suffix'] = '?' . $_SERVER['QUERY_STRING'];
		$config['uri_segment'] = 3;
		$config['per_page'] = $limit;
		$this->pagination->initialize($config);

		$data['per_page'] = $limit;
		$data['produk'] = $this->Favorit_model->get_all_produk($params, $params2);
		$data['_script'] = 'script_favorit';
		$data['_view'] = 'favorit';
		$data['currentPage'] = '';

		$this->load->view('layouts/main', $data);
	}

	public function config()
	{
		$config['first_link']       = '&laquo; First';
		$config['last_link']        = 'Last &raquo;';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only"></span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true"></span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';

		return $config;
	}
}

/* End of file Favorit.php */
/* Location: ./application/controllers/Favorit.php */
