<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kategori_model');
		$this->load->model('Produk_paging_model');
		$this->load->model('Chat_model');
		$this->load->model('Produk_model');
		$this->load->model('Produk_fav_model');
		$this->load->model('Users_model');


		$this->encryption->initialize(
			array(
				'cipher' => 'aes-256',
				'mode' => 'ctr',
				'key' => 'b5ad27baace26332abb1884c1fc68091'
			)
		);
	}


	public function index($id = 1)
	{
		$per_page_arr = array('5', '10', '20', '50', '100');
		$per_page = ($id == 1) ? 0 : $id;
		$limit = isset($_GET['sortlimit']) && in_array($_GET['sortlimit'], $per_page_arr) ? $_GET['sortlimit'] : RECORDS_PER_PAGE;
		$params['limit'] = $limit;
		$params['offset'] = $per_page;

		$this->load->library('pagination');
		$config = $this->config();
		$config['base_url'] = site_url('produk/index');
		$config['total_rows'] = $this->Produk_paging_model->get_all_produk_count();
		$config['suffix'] = '?' . $_SERVER['QUERY_STRING'];
		$config['uri_segment'] = 3;
		$config['per_page'] = $limit;
		$this->pagination->initialize($config);

		if ($this->input->get('toko', TRUE)) {
			$prm = array(
				'u.id' => $this->input->get('toko', TRUE),
				'ku.level_user_id >=' => 3
			);
			$data['toko'] = $this->Users_model->find_user($prm);
		}

		$data['per_page'] = $limit;
		$data['produk'] = $this->Produk_paging_model->get_all_produk($params);
		$data['kategori'] = $this->Kategori_model->get_all_kategori();
		$data['currentPage'] = 'home';
		$data['_script'] = 'script_porduk';
		$data['_chat'] = true;
		$data['_view'] = 'dashboard';

		$this->load->view('layouts/main', $data);
	}

	function detail($id = '')
	{
		$data['produk'] = $this->Produk_model->getRow_produk(array('p.id' => $id));
		if (isset($data['produk']['id'])) {
			$data['foto'] = $this->Produk_model->getFoto_produk(array('produk_id' => $id));
			$data['favorite'] = $this->Produk_fav_model->findRow_fav(array('user_id' => user_data('id_user'), 'produk_id' => $id));
			$data['kategori'] = $this->Kategori_model->get_all_kategori();
			if ($data['produk']['user_id'] == user_data('id_user')) {
				$data['chat'] = $this->Chat_model->findRow_chat(array('c.user_id_penjual' => user_data('id_user'), 'c.produk_id' => $data['produk']['id']));
			} else {
				$data['chat'] = $this->Chat_model->findRow_chat(array('c.user_id_pembeli' => user_data('id_user'), 'c.user_id_penjual' => $data['produk']['user_id'], 'c.produk_id' => $data['produk']['id']));
			}

			$data['currentPage'] = 'home';
			$data['_script'] = 'script_fvrt';
			if ($data['produk']['user_id'] != user_data('id_user')) {
				$data['_chat'] = true;
			}
			$data['_view'] = 'detail_product';
			$this->load->view('layouts/main', $data);
		} else {
			// redirect('produk', 'refresh');
		}
	}

	public function config()
	{
		$config['first_link']       = '&laquo; First';
		$config['last_link']        = 'Last &raquo;';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only"></span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true"></span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';

		return $config;
	}
}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
