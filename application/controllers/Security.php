<?php

/**
 *  Copyright (c) 2017. Nur Rachmat <rachmat.nur91@gmail.com>
 *  Project : voa > File Name : Login.php 22/05/17 14:08
 *  This file is under my authorization. For the reason of code addition, modification, documentation and more information please contact me. -Nur Rachmat
 *
 */
/**
 * @package         Security
 * @subpackage      Controller
 * @author          Nur Rachmat <rachmat.nur91@gmail.com>
 * @version         0.1
 * @copyright       Copyright © 2017 Nur Rachmat <rachmat.nur91@gmail.com>
 */
class Security extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }


    /**
     *  To handle if the CRFS token expired or validation failed
     */
    function csrf_redirect()
    {
        $flash = 'Session cookie automatically reset due to expired browser session. Please try again.';
        $this->session->set_flashdata('m_error', $flash);
        redirect($this->agent->referrer(), 'refresh');
    }
}
