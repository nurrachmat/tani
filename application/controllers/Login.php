<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		$this->load->model('Users_model');
		auth_host();
	}

	public function index()
	{
		$this->load->view('layouts/login');
	}


	public function checking()
	{
		$this->load->helper('string');
		$msg_vaild = array(
			'required' => '%s Harus diisi',
			'valid_email' => '%s Harus format email'
		);
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email', $msg_vaild);
		$this->form_validation->set_rules('password', 'Password', 'required', $msg_vaild);

		if ($this->form_validation->run()) {
			$username = $this->input->post("email");
			$password = $this->input->post("password");
			$cek_login = $this->Users_model->find_simple_user(array("email" => $username, "password" => sha1($password), "status" => 'Y', 'level_user_id >=' => 3, 'email_verified' => 1));
			if (isset($cek_login['id'])) {
				$module = array('1' => 'dinas', '2' => 'dinas', '3' => 'public', '4' => 'public');
				$this->session->set_userdata('id_user', $cek_login['id']);  //ID User
				$this->session->set_userdata('nama_user', $cek_login['nama']);  //Nama User
				$this->session->set_userdata('email_user', $cek_login['email']);  //Nama User
				$this->session->set_userdata('level', $cek_login['level_user_id']);  // Group ID
				$this->session->set_userdata('foto', $cek_login['foto']);  // FOTO
				$this->session->set_userdata('module', $module[$cek_login['level_user_id']]);  //set module to access
				$this->session->set_userdata('logged_in', TRUE);

				//GENERATE TOKEN
				$TOKEN = sha1(uniqid($cek_login['email'], true));
				$this->Users_model->update_users($cek_login['id'], array('token' => $TOKEN));

				redirect('produk', 'refresh');
			} else {
				m_error("Username atau password tidak cocok dengan data kami");
				redirect('login', 'refresh');
			}
		} else {
			$this->load->view('layouts/login');
		}
	}

	function reset_password()
	{
		$this->load->library('Mailsender');

		$msg_vaild = array(
			'required' => '%s Harus diisi',
			'valid_email' => '%s Harus format email'
		);
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email', $msg_vaild);
		if ($this->form_validation->run()) {
			$email = $this->input->post('email');
			$user = $this->Users_model->find_user(array('email' => $email));

			if (isset($user['id'])) {
				// generate new password 
				$password = random_string('alnum', 6);

				// update new password 
				$result = $this->Users_model->update_users($user['id'], array("password" => sha1($password)));

				if ($result) {
					// kirim email
					$subject = "Reset Password";

					$message = "Salam hormat, " . $user['nama'] . "<br><br>Silakan masuk ke aplikasi mobile " . APP_TITLE . " menggunakan informasi akun berikut: <br>Email : " . $email . "<br>Password : " . $password . "<br><br>Terima kasih atas perhatian dan kerjasamanya.";

					$mail['subject'] = $subject;
					$mail['body'] = $message;
					$mail['mail_to'] = $email;
					$this->mailsender->set_vars($mail);
					$this->mailsender->send_mail();
					if ($result) {
						m_success('Password baru berhasil dikirim ke email : ' . $email);
					} else {
						m_error('Password baru gagal dikirim ke email : ' . $email);
					}
				} else {
					m_error('Password gagal diperbarui');
				}
			} else {
				m_error('Email belum terdaftar.');
			}

			redirect('login', 'refresh');
		} else {
			$this->index();
		}
	}

	function signup()
	{
		$this->load->library('Mailsender');

		$msg_vaild = array(
			'required' => '%s Harus diisi',
			'valid_email' => '%s Harus format email',
			'is_unique' => '%s tersebut sudah ada',
			'matches' => '%s tidak cocok dengan password',
		);
		$this->form_validation->set_rules('regis_email', 'Email', 'trim|required|valid_email|is_unique[user.email]', $msg_vaild);
		$this->form_validation->set_rules('hp', 'Nomor HP', 'trim|required|is_unique[user.hp]', $msg_vaild);
		$this->form_validation->set_rules('nmuser', 'Nama Lengkap', 'trim|required', $msg_vaild);
		$this->form_validation->set_rules('regis_password', 'Password', 'trim|required', $msg_vaild);
		$this->form_validation->set_rules('confpass', 'Konfirmasi Password', 'trim|required|matches[regis_password]', $msg_vaild);
		if ($this->form_validation->run()) {
			$email = $this->input->post('regis_email', TRUE);
			$hp = $this->input->post('hp', TRUE);
			$nama = $this->input->post('nmuser', TRUE);
			$password = $this->input->post('regis_password', TRUE);
			$email_token = md5(rand(100, 9999) . time() . "_token_tan1_m4il");

			$val = array(
				'email' => $email,
				'nama' => $nama,
				'hp' => $hp,
				'password' => sha1($password),
				'status' => 'Y',
				'kategori_user_id' => '3',
				'email_token' => $email_token,
				'created_at' => date('Y-m-d'),
			);

			$users_id = $this->Users_model->add_users($val);
			if ($users_id) {
				$subject = "Registrasi";

				$message = "Salam hormat, " . $nama . "<br><br>Kamu baru saja melakukan pendaftaran akun " . APP_TITLE . " menggunakan alamat <br>Email : " . $email . "<br><br>
				Untuk memastikan akun email ini adalah milikmu, silahkan Klik Link di bawah ini <br/>
				<a href=" . site_url('login/verified_mail/' . md5($users_id) . '/' . $email_token) . "> " . site_url('login/verified_mail/' . md5($users_id) . '/' . $email_token) . " </a>
				<br/><br/>
				Terima kasih atas perhatian dan kerjasamanya.";

				$mail['subject'] = $subject;
				$mail['body'] = $message;
				$mail['mail_to'] = $email;
				$this->mailsender->set_vars($mail);
				$result = $this->mailsender->send_mail();

				if ($result) {
					m_success("Registrasi berhasil, periksa email Anda untuk melihat informasi akun login aplikasi " . APP_TITLE);
				} else {
					m_error("Informasi akun gagal dikirim. Ada masalah saat pengiriman email");
					// send log ...
				}
			} else {
				m_error("Register anda gagal");
			}
			redirect('login', 'refresh');
		} else {
			$this->index();
		}
	}

	function verified_mail($id, $token)
	{
		if (trim($id) != '' and trim($token) != '') {
			$params = array(
				'MD5(u.id)' => $id,
				'u.email_token' => $token,
			);
			$cek_user = $this->Users_model->find_user($params);
			if (isset($cek_user['id'])) {
				$this->Users_model->update_users_parameter(array(
					'MD5(id)' => $id
				), array('email_verified' => 1));
				m_success("Email Anda telah terverifikasi. Silahkan Login");
				redirect('login', 'refresh');
			}
		} else {
			redirect('/', 'refresh');
		}
	}

	/**
	 * Logout from the session
	 */
	function logout()
	{
		$this->session->sess_destroy();
		m_success("Anda telah keluar dari aplikasi");
		redirect('login');
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
