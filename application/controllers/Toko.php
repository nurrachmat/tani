<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Toko extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		auth_host();
		$this->load->model('Users_model');

		$this->load->model('Kategori_model');
		$this->load->model('Produk_paging_model');
		$this->load->model('Produk_model');
		$this->load->model('Kecamatan_model');
		$this->load->model('Produk_fav_model');
		$this->load->model('Chat_model');


		$this->encryption->initialize(
			array(
				'cipher' => 'aes-256',
				'mode' => 'ctr',
				'key' => 'b5ad27baace26332abb1884c1fc68091'
			)
		);
	}

	public function index($id = 1)
	{
		$per_page = ($id == 1) ? 0 : $id;
		$limit = RECORDS_PER_PAGE;
		$params['limit'] = $limit;
		$params['offset'] = $per_page;

		$params2 = array('produk.user_id' => user_data('id_user'));

		$this->load->library('pagination');
		$config = $this->config();
		$config['base_url'] = site_url('toko/index');
		$config['total_rows'] = $this->Produk_paging_model->get_all_produk_count($params2);
		$config['suffix'] = '?' . $_SERVER['QUERY_STRING'];
		$config['uri_segment'] = 3;
		$config['per_page'] = $limit;
		$this->pagination->initialize($config);

		$data['per_page'] = $limit;
		$data['produk'] = $this->Produk_paging_model->get_all_produk($params, $params2);
		$data['_view'] = 'vtoko/toko';
		$data['currentPage'] = 'lapak';

		$this->load->view('layouts/main', $data);
	}

	public function add($data = array())
	{
		$this->load->helper('string');
		$msg_vaild = array(
			'required' => '%s Harus diisi',
			'min_length' => '{field} minimal {param} characters.',
			'max_length' => '{field} maksimal {param} characters.',
			'numeric' => '%s Harus angka',
		);
		$this->form_validation->set_rules('kondisi', 'Kondisi', 'trim|required', $msg_vaild);
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required', $msg_vaild);
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'trim|required', $msg_vaild);
		$this->form_validation->set_rules('nm_produk', 'Nama Produk', 'trim|required|min_length[15]|max_length[70]', $msg_vaild);
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|min_length[20]|max_length[4000]', $msg_vaild);
		$this->form_validation->set_rules('hrg_produk', 'Harga Produk', 'trim|required|numeric', $msg_vaild);
		if ($this->form_validation->run()) {

			$path = 'uploads/produk/';
			/* $path = 'uploads/produk/' . $id_insert . '/';
			if (!file_exists($path)) {
				mkdir($path, 0777, true);
			} else {
				chmod(dirname($path), 0777);
			}

			$myfile = fopen("./" . $path . "/index.html", "w") or die("Unable to open file!");
			$txt = "<html>
<head>
<title>403 Forbidden</title>
</head>
<body>

<p>Directory access is forbidden.</p>

</body>
</html>";
			fwrite($myfile, $txt);
			fclose($myfile); */

			// FOTO DEPAN
			if (!isset($_FILES['foto_dpn']['tmp_name'])) {
				m_error("File Foto Depan harus diisi");
				// redirect('toko/add', 'refresh');
			} else {
				$config['upload_path']          = './' . $path;
				$config['allowed_types']        = 'jpg|jpeg';
				$config['max_size']             = 550;
				$config['file_name']            = time() . '-' . url_title($this->input->post('nm_produk', TRUE)) . '-fr';

				$this->load->library('upload', $config, 'tmp_dpn');
				$this->tmp_dpn->initialize($config);

				if (!$this->tmp_dpn->do_upload('foto_dpn')) {
					$data['error'] = 'Foto Depan : ' . $this->tmp_dpn->display_errors();
					$error = 1;
					$this->add($data);
				} else {
					$file_dpn = $this->tmp_dpn->data();
					$error = 0;
				}
			}

			//FOTO ATAS
			if (!isset($_FILES['foto_ats']['tmp_name'])) {
				m_error("File Foto Atas harus diisi");
				redirect('toko/add', 'refresh');
			} else {
				$config['upload_path']          = './' . $path;
				$config['allowed_types']        = 'jpg|jpeg';
				$config['max_size']             = 550;
				$config['file_name']            = time() . '-' . url_title($this->input->post('nm_produk', TRUE)) . '-up';

				$this->load->library('upload', $config, 'tmp_ats');
				$this->tmp_ats->initialize($config);

				if (!$this->tmp_ats->do_upload('foto_ats')) {
					$data['error'] = 'Foto tampak Atas : ' . $this->tmp_ats->display_errors();
					$error = 1;
					$this->add($data);
				} else {
					$file_ats = $this->tmp_ats->data();
					$error = 0;
				}
			}

			//FOTO KANAN
			if (!isset($_FILES['foto_knn']['tmp_name'])) {
				m_error("File Foto Kanan harus diisi ");
				redirect('toko/add', 'refresh');
			} else {
				$config['upload_path']          = './' . $path;
				$config['allowed_types']        = 'jpg|jpeg';
				$config['max_size']             = 550;
				$config['file_name']            = time() . '-' . url_title($this->input->post('nm_produk', TRUE)) . '-rg';

				$this->load->library('upload', $config, 'tmp_knn');
				$this->tmp_knn->initialize($config);

				if (!$this->tmp_knn->do_upload('foto_knn')) {
					$data['error'] = 'Foto Tampak Kanan : ' . $this->tmp_knn->display_errors();
					$error = 1;
					$this->add($data);
				} else {
					$file_knn = $this->tmp_knn->data();
					$error = 0;
				}
			}

			//FOTO KIRI
			if (!isset($_FILES['foto_kri']['tmp_name'])) {
				m_error("File Foto Kiri harus diisi ");
				redirect('toko/add', 'refresh');
			} else {
				$config['upload_path']          = './' . $path;
				$config['allowed_types']        = 'jpg|jpeg';
				$config['max_size']             = 550;
				$config['file_name']            = time() . '-' . url_title($this->input->post('nm_produk', TRUE)) . '-lf';

				$this->load->library('upload', $config, 'tmp_kri');
				$this->tmp_kri->initialize($config);

				if (!$this->tmp_kri->do_upload('foto_kri')) {
					$data['error'] = 'Foto tampak Kiri' . $this->tmp_kri->display_errors();
					$error = 1;
					$this->add($data);
				} else {
					$file_kri = $this->tmp_kri->data();
					$error = 0;
				}
			}

			//FOTO BELAKANG
			if (!isset($_FILES['foto_blk']['tmp_name'])) {
				m_error("File Foto Belakang harus diisi");
				redirect('toko/add', 'refresh');
			} else {
				$config['upload_path']          = './' . $path;
				$config['allowed_types']        = 'jpg|jpeg';
				$config['max_size']             = 550;
				$config['file_name']            = time() . '-' . url_title($this->input->post('nm_produk', TRUE)) . '-bc';

				$this->load->library('upload', $config, 'tmp_blk');
				$this->tmp_blk->initialize($config);

				if (!$this->tmp_blk->do_upload('foto_blk')) {
					$data['error'] = 'Foto tampak Belakang : ' . $this->tmp_blk->display_errors();
					$error = 1;
					$this->add($data);
				} else {
					$file_blk = $this->tmp_blk->data();
					$error = 0;
				}
			}

			//TIDAK ADA ERROR
			if ($error == 0) {
				$val = array(
					'nama_produk' => $this->input->post('nm_produk', TRUE),
					'deskripsi' => $this->input->post('deskripsi', TRUE),
					'harga' => $this->input->post('hrg_produk', TRUE),
					'kondisi' =>  $this->input->post('kondisi', TRUE),
					'status' => 'Y',
					'kecamatan_id' => $this->input->post('kecamatan', TRUE),
					'kategori_id' => $this->input->post('kategori', TRUE),
					'created_at' => date('Y-m-d'),
					'user_id' => user_data('id_user')
				);

				$id_insert = $this->Produk_model->add_produk($val);

				$val_foto_dpn = array(
					'foto' => $file_dpn['file_name'],
					'url_foto' => $path,
					'featured_img' => 'Y',
					'produk_id' => $id_insert,
				);
				$this->Produk_model->add_produk_foto($val_foto_dpn);

				$val_foto_ats = array(
					'foto' => $file_ats['file_name'],
					'url_foto' => $path,
					'featured_img' => NULL,
					'produk_id' => $id_insert,
				);
				$this->Produk_model->add_produk_foto($val_foto_ats);

				$val_foto_knn = array(
					'foto' => $file_knn['file_name'],
					'url_foto' => $path,
					'featured_img' => NULL,
					'produk_id' => $id_insert,
				);
				$this->Produk_model->add_produk_foto($val_foto_knn);

				$val_foto_kri = array(
					'foto' => $file_kri['file_name'],
					'url_foto' => $path,
					'featured_img' => NULL,
					'produk_id' => $id_insert,
				);
				$this->Produk_model->add_produk_foto($val_foto_kri);

				$val_foto_blk = array(
					'foto' => $file_blk['file_name'],
					'url_foto' => $path,
					'featured_img' => NULL,
					'produk_id' => $id_insert,
				);
				$this->Produk_model->add_produk_foto($val_foto_blk);


				redirect('toko', 'refresh');
			}
		} else {
			$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
			$data['kategori'] = $this->Kategori_model->get_all_kategori();
			$data['_script'] = 'vtoko/script';
			$data['_view'] = 'vtoko/add';
			$data['currentPage'] = 'lapak';
			$this->load->view('layouts/main', $data);
		}
	}

	public function ubah($id, $slug = '')
	{
		$data['produk'] = $this->Produk_model->getRow_produk(array('p.id' => $id, 'p.user_id' => user_data('id_user')));
		if (isset($data['produk']['id'])) {
			$msg_vaild = array(
				'required' => '%s Harus diisi',
				'min_length' => '{field} minimal {param} characters.',
				'max_length' => '{field} maksimal {param} characters.',
				'numeric' => '%s Harus angka',
			);
			$this->form_validation->set_rules('kondisi', 'Kondisi', 'trim|required', $msg_vaild);
			$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required', $msg_vaild);
			$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'trim|required', $msg_vaild);
			$this->form_validation->set_rules('nm_produk', 'Nama Produk', 'trim|required|min_length[15]|max_length[70]', $msg_vaild);
			$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|min_length[20]|max_length[4000]', $msg_vaild);
			$this->form_validation->set_rules('hrg_produk', 'Harga Produk', 'trim|required|numeric', $msg_vaild);
			if ($this->form_validation->run()) {
				$val = array(
					'nama_produk' => $this->input->post('nm_produk', TRUE),
					'deskripsi' => $this->input->post('deskripsi', TRUE),
					'harga' => $this->input->post('hrg_produk', TRUE),
					'kondisi' =>  $this->input->post('kondisi', TRUE),
					'status' => 'Y',
					'kecamatan_id' => $this->input->post('kecamatan', TRUE),
					'kategori_id' => $this->input->post('kategori', TRUE),
					'created_at' => date('Y-m-d'),
					'user_id' => user_data('id_user')
				);

				$this->Produk_model->update_produk($id, $val);
				redirect('produk/detail/' . $data['produk']['id'] . '/' . url_title($data['produk']['nama_produk']), 'refresh');
			} else {
				$data['foto'] = $this->Produk_model->find_produk_foto(array('p.produk_id' => $id));
				$data['kecamatan'] = $this->Kecamatan_model->get_all_kecamatan();
				$data['kategori'] = $this->Kategori_model->get_all_kategori();
				$data['_script'] = 'vtoko/script';
				$data['_view'] = 'vtoko/edit';
				$data['currentPage'] = 'lapak';
				$this->load->view('layouts/main', $data);
			}
		} else {
			redirect('produk', 'refresh');
		}
	}

	function liaotianwo()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$en_id = $this->input->post('_token_validate_fv');
			$id = $this->encryption->decrypt($en_id);
			$produk = $this->Produk_model->get_produk($id);
			$user = $this->Users_model->find_user(array('u.id' => user_data('id_user')));
			if (isset($produk['id'])) {
				$chat = $this->Chat_model->findRow_chat(array('c.user_id_pembeli' => user_data('id_user'), 'c.user_id_penjual' => $produk['user_id'], 'c.produk_id' => $produk['id']));
				$detail_chat = $this->Chat_model->find_chat_detail(array('c.id' => $chat['id']));
				$data['result'] = true;
				$data['chat'] = $chat;
				$data['data'] = $detail_chat;
			} else {
				$data['result'] = true;
			}
			echo json_encode($data);
		} else {
			redirect('toko', 'refresh');
		}
	}


	function fvrt_porduk()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$en_id = $this->input->post('_token_validate_fv');
			$id = $this->encryption->decrypt($en_id);
			$cek_produk = $this->Produk_model->get_produk($id);
			if (isset($cek_produk['id'])) {
				$cek_fav = $this->Produk_fav_model->findRow_fav(array('user_id' => user_data('id_user'), 'produk_id' => $cek_produk['id']));

				if (isset($cek_fav['id'])) {
					$params = array(
						'produk_id' => $cek_produk['id'],
						'user_id' => user_data('id_user'),
					);
					$this->Produk_fav_model->delete_fav($params);
					$this->Produk_model->update_fav_produk($cek_produk['id'], 'minus');
					$Nproduk = $this->Produk_model->get_produk($id);
					$data['result'] = true;
					$data['favorit'] = 2;
					$data['c_fav'] = $Nproduk['favorit'];
					$data['message'] = 'Produk tidak menjadi favorite';
				} else {
					$val = array(
						'produk_id' => $cek_produk['id'],
						'user_id' => user_data('id_user'),
						'tanggal' => date('Y-m-d'),
					);
					$this->Produk_fav_model->add_fav($val);
					$this->Produk_model->update_fav_produk($cek_produk['id'], 'add');
					$Nproduk = $this->Produk_model->get_produk($id);
					$data['result'] = true;
					$data['favorit'] = 1;
					$data['c_fav'] = $Nproduk['favorit'];
					$data['message'] = 'Produk Menjadi Favorite';
				}
			} else {
				$data['result'] = false;
				$data['message'] = 'Anda Melakukan Pelanggaran UU ITE';
			}
			echo json_encode($data);
		} else {
			redirect('/', 'refresh');
		}
	}

	function picture($id)
	{
		if (isset($_POST) && count($_POST) > 0) {
			$en_id = $this->input->post('_token_unique_prv');
			$id_produk = $this->encryption->decrypt($en_id);
			$produk_foto = $this->Produk_model->getRow_produk_foto(array('id' => $id));
			if (isset($produk_foto['id'])) {
				if ($produk_foto['produk_id'] == $id_produk) {
					$cek_produk = $this->Produk_model->get_produk($id_produk);

					$path = 'uploads/produk/';
					$config['upload_path']          = './' . $path;
					$config['allowed_types']        = 'jpg|jpeg';
					$config['max_size']             = 550;
					$config['file_name']			= $produk_foto['foto'];

					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('foto_produk')) {
						$error = $this->upload->display_errors();
						$this->session->set_flashdata('error', $error);
						redirect('toko/ubah/' . $produk_foto['produk_id'] . '/' . url_title($cek_produk['nama_produk']), 'refresh');
					} else {
						$file = $this->upload->data();
						$val = array(
							'foto' => $file['file_name'],
							'url_foto' => $path,
						);
						$this->Produk_model->update_produk_foto($id, $val);
						redirect('toko/ubah/' . $produk_foto['produk_id'], 'refresh');
					}
				} else {
					redirect('/toko/ubah/' . $produk_foto['produk_id'], 'refresh');
				}
			} else {
				redirect('/toko', 'refresh');
			}
		} else {
			redirect('/', 'refresh');
		}
	}

	function storeliao()
	{
		if (isset($_POST) && count($_POST) > 0) {
			$id = $this->input->post('_token_validate_fv', TRUE);
			$produk = $this->Produk_model->get_produk_md5($id);

			$uid_chat = $this->input->post('_token_private_prd', TRUE);
			$isi_chat = $this->input->post('isi_cht', TRUE);
			$user = $this->Users_model->find_user(array('u.id' => user_data('id_user')));
			$cek_chat = $this->Chat_model->findRow_chat(array('uuid' => $uid_chat));
			$chat_id = $cek_chat['id'];

			$pembeli = $cek_chat['user_id_pembeli'];
			if (!isset($cek_chat['id'])) {
				$pembeli = user_data('id_user');
				$val_c = array(
					'uuid' => $uid_chat,
					'tanggal' => date('Y-m-d H:i:s'),
					'user_id_pembeli' => user_data('id_user'),
					'user_id_penjual' => $produk['user_id'],
					'produk_id' => $produk['id'],
				);
				$chat_id = $this->Chat_model->add_chat($val_c);
			}

			$val = array(
				'chat_id' => $chat_id,
				'isi_text' => $isi_chat,
				'tanggal' => date('Y-m-d H:i:s'),
				'user_id' => user_data('id_user'),
			);

			$this->Chat_model->add_chat_detail($val);

			$data['result'] = true;
			$data['sender'] = user_data('id_user');
			$data['recived'] =  (($produk['user_id'] <> user_data('id_user')) ? user_data('id_user') : $pembeli);
			$data['nmsender'] = (isset($cek_chat['nama']) ? $cek_chat['nama'] : $user['nama']);
			$data['produk'] = $produk['id'];
			$data['nmproduk'] = $produk['nama_produk'];
			$data['tanggal'] = date('Y-m-d H:i:s');
			$data['penjual'] = $produk['user_id'];
			$data['DD8444fc9ee1bb'] = md5($produk['id']);
			$data['nmpenjual'] = $produk['nama'];

			echo json_encode($data);
		} else {
			redirect('toko', 'refresh');
		}
	}

	public function config()
	{
		$config['first_link']       = '&laquo; First';
		$config['last_link']        = 'Last &raquo;';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only"></span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true"></span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';

		return $config;
	}
}

/* End of file Toko.php */
/* Location: ./application/controllers/Toko.php */
