<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		authenticate();
	}

	function index()
	{
		redirect('auth/login', 'refresh');
	}

	public function login()
	{

		$this->load->view('login');
	}

	public function forget_password()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		if ($this->form_validation->run()) {
			// get value from email input
			$email = $this->input->post('email', TRUE);

			$this->load->model('Users_model');

			$data['dbo_user'] = $this->Users_model->find_simple_user(array("email" => $email));
			if (isset($data['dbo_user']['id'])) {
				// generate new password 
				$password = random_string('alnum', 6);

				// get value nama
				$nama = $data['dbo_user']['nama'];

				// update new password 
				$dbo_user = $this->Users_model->update_dbo_user($data['dbo_user']['id'], array("password" => sha1($password)));

				if ($dbo_user) {
					// kirim email
					$subject = "Reset Password";

					$message = "Salam hormat, " . $nama . "<br><br>Password akun " . APP_TITLE . " berhasil diperbarui oleh sistem pada " . date('d-m-Y H:i:s') . ".<br><br>Silakan masuk ke aplikasi <a href='" . base_url() . "'>" . APP_TITLE . "</a> menggunakan informasi akun berikut: <br>Email : " . $email . "<br>Password : " . $password . "<br><br>Terima kasih atas perhatian dan kerjasamanya.";

					$result = $this->_send_email($email, $nama, $subject, $message);
					if ($result) {
						m_success("Reset password berhasil, periksa email Anda untuk melihat informasi akun login aplikasi " . APP_TITLE);
					} else {
						m_error("Password baru gagal dikirim. Ada masalah saat pengiriman email. Silakan reset password lagi.");
						// send log ...
					}
				} else {
					m_error("Reset password gagal, pastikan data yang Anda isi valid");
				}
			} else {
				m_error("Email tidak cocok dengan data kami");
			}
			redirect('auth/forget_password');
		} else {
			$this->load->view('forget_password');
		}
	}

	private function _send_email($email = null, $nama = null, $subject = null, $message = null)
	{
		// Konfigurasi email
		$config = [
			'mailtype'  => 'html',
			'charset'   => 'utf-8',
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_user' => EMAIL_ADDRESS_SENDER,
			'smtp_pass' => EMAIL_PASSWORD_SENDER,
			'smtp_port' => 465,
			'crlf'      => "\r\n",
			'newline'   => "\r\n"
		];

		// Load library email dan konfigurasinya
		$this->load->library('email', $config);

		// Email dan nama pengirim
		$this->email->from(EMAIL_ADDRESS_SENDER, EMAIL_NAME_SENDER);

		// Email penerima
		$this->email->to($email); // Ganti dengan email tujuan kamu

		// Lampiran email, isi dengan url/path file
		//$this->email->attach('link');

		// Subject email
		$this->email->subject(APP_TITLE . " " . APP_OWNER . " - " . $subject);

		// Isi email
		$this->email->message($message);

		// Tampilkan pesan sukses atau error
		if ($this->email->send()) {
			return true;
		} else {
			return false;
		}
	}

	public function verify()
	{
		$this->load->model('Users_model', 'User_model');
		$msg_vaild = array(
			'required' => '%s Harus diisi',
			'valid_email' => '%s Harus format email'
		);
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email', $msg_vaild);
		$this->form_validation->set_rules('password', 'Password', 'required', $msg_vaild);

		if ($this->form_validation->run()) {
			$username = $this->input->post("email");
			$password = $this->input->post("password");
			$cek_login = $this->User_model->find_simple_user(array("email" => $username, "password" => sha1($password), "status" => 'Y'));
			if (isset($cek_login['id'])) {
				$module = array('1' => 'dinas', '2' => 'dinas', '3' => 'public', '4' => 'public');
				$this->session->set_userdata('id_user', $cek_login['id']);  //ID User
				$this->session->set_userdata('nama_user', $cek_login['nama']);  //Nama User
				$this->session->set_userdata('email_user', $cek_login['email']);  //Nama User
				$this->session->set_userdata('level', $cek_login['level_user_id']);  // Group ID
				$this->session->set_userdata('nama_kategori_user', $cek_login['nama_kategori_user']);  // Group ID
				$this->session->set_userdata('foto', $cek_login['foto']);  // FOTO
				$this->session->set_userdata('module', $module[$cek_login['level_user_id']]);  //set module to access
				$this->session->set_userdata('logged_in', TRUE);

				redirect(get_module() . '/dashboard', 'refresh');
			} else {
				m_error("Username atau password tidak cocok dengan data kami");
				redirect('auth/login', 'refresh');
			}
		} else {
			$this->login();
		}
	}

	/**
	 * Logout from the session
	 */
	function logout()
	{
		$this->session->sess_destroy();
		unset(
			$_SESSION['id_user'],
			$_SESSION['nama_user'],
			$_SESSION['email_user'],
			$_SESSION['level'],
			$_SESSION['foto'],
			$_SESSION['module'],
			$_SESSION['logged_in']
		);
		m_success("Anda telah keluar dari aplikasi");
		redirect('auth/login');
	}
}
