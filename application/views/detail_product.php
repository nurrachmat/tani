<section class="hero hero-normal">
	<div class="container">
		<div class="row">
			<?php
			$this->load->view('layouts/sidebar');
			echo '<div class="col-lg-9">';
			$this->load->view('layouts/search');
			echo '</div>';
			?>
		</div>
	</div>
</section> <!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section set-bg" data-setbg="<?= base_url() ?>assets/img/breadcrumb.jpg">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="breadcrumb__text">
					<h2><?= $produk['nama_kategori'] ?></h2>
					<div class="breadcrumb__option">
						<a href="<?= site_url('produk') ?>">Home</a>
						<a href="<?= site_url('produk?kategori=' . $produk['kategori_id']) ?>"><?= $produk['nama_kategori'] ?></a>
						<span><?= $produk['nama_produk'] ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Breadcrumb Section End -->

<!-- Product Details Section Begin -->
<section class="product-details spad">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<div class="product__details__pic">
					<div class="product__details__pic__item">
						<img class="product__details__pic__item--large" src="<?= base_url() ?>uploads/produk/<?= $produk['foto'] ?>" alt="<?= $produk['nama_produk'] ?>">
					</div>
					<div class="product__details__pic__slider owl-carousel">
						<?php
						foreach ($foto as $val) {
							echo '<img data-imgbigurl="' . base_url() . 'uploads/produk/' . $val['foto'] . '" src="' . base_url() . 'uploads/produk/' . $val['foto'] . '" alt="">';
						} ?>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="product__details__text">
					<h3>
						<?php
						echo $produk['nama_produk'];

						if (isset($favorite['id'])) {
							$txt = "text-danger";
						} else {
							$txt = "";
						}
						?>
						<a href="javascript:void(0)" id="btn_heart" class="heart-icon <?= $txt ?> ">
							<span class="fa fa-heart"></span>
						</a> <span class="badge-heart"><?= $produk['favorit'] ?></span>
						<?php
						if ($produk['user_id'] == user_data('id_user')) { ?>
							<a href="<?= site_url('toko/ubah/' . $produk['id'] . '/' . url_title($produk['nama_produk'])) ?>" class="btn " title="Ubah"><span class="fa fa-edit"></span></a>
						<?php } ?>
					</h3>
					<div class="product__details__rating">
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star-half-o"></i>
						<!-- <span>(18 reviews)</span> -->
					</div>
					<div class="product__details__price">Rp. <?= format_angka($produk['harga']) ?>,-</div>
					<p><?= $produk['deskripsi'] ?></p>
					<input type="hidden" id="_token_private_prd" value="<?= (isset($chat['uuid']) ? $chat['uuid'] : '0') ?>">
					<ul>
						<li><b>Ketersediaan</b> <span><?= (isset($produk['status']) ? 'Masih Ada' : 'Sudah Habis') ?></span></li>
						<li><b>Kondisi</b> <span><?= kondisi_produk($produk['kondisi']) ?></span></li>
						<li><b>Kecamatan</b> <span><?= $produk['nama_kecamatan'] ?></span></li>
						<li><b>Kabupaten</b> <span><?= $produk['nama_kotakabupaten'] ?>, <?= $produk['nama_provinsi'] ?></span></li>
						<li><b>Lapak</b> <span><a href="<?= site_url('produk?toko=' . $produk['user_id']) ?>"><?= $produk['nama_penjual'] ?></a></span></li>
						<li><b>Telp/HP</b> <span><?= $produk['hp'] ?></span></li>
						<li><b>Weight</b> <span>0.5 kg</span></li>
						<li><b>Post Date</b> <span><?= date("d F Y", strtotime($produk['created_at'])) ?></span></li>
					</ul>
				</div>
			</div>

		</div>
	</div>
</section>
<!-- Product Details Section End -->
