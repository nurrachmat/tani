<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title><?php echo APP_TITLE . " - " . APP_OWNER; ?></title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <link rel="icon" href="<?php echo site_url('resources/img/icon.ico'); ?>" type="image/x-icon" />

  <!-- Fonts and icons -->
  <script src="<?php echo site_url('resources/js/plugin/webfont/webfont.min.js'); ?>"></script>
  <script>
    WebFont.load({
      google: {
        "families": ["Lato:300,400,700,900"]
      },
      custom: {
        "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"],
        urls: ['<?php echo site_url('resources/css/fonts.min.css'); ?>']
      },
      active: function() {
        sessionStorage.fonts = true;
      }
    });
  </script>

  <!-- CSS Files -->
  <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo site_url('resources/css/atlantis.min.css'); ?>">

  <?php if (isset($_usedselect2) && $_usedselect2 == true) { ?>
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo site_url('resources/css/select2.min.css'); ?>">
  <?php } ?>

  <!-- CSS Just for demo purpose, don't include it in your project -->
  <!-- <link rel="stylesheet" href="resources/css/demo.css"> -->
</head>

<body>
  <div class="wrapper overlay-sidebar">
    <div class="main-panel">
      <div class="content">
        <div class="panel-header bg-primary-gradient">
          <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
              <div>
                <h2 class="text-white pb-2 fw-bold">Login</h2>
                <h5 class="text-white op-7 mb-2"><?php echo APP_TITLE_LONG; ?> (<?php echo APP_TITLE; ?>)</h5>
              </div>
              <div class="ml-md-auto py-2 py-md-0">
                <a href="<?php echo site_url(); ?>" class="btn btn-white btn-border btn-round mr-2">Beranda</a>
                <a href="#" class="btn btn-secondary btn-round">Lupa Password</a>
              </div>
            </div>
          </div>
        </div>
        <div class="page-inner mt--5">
          <div class="row row-card-no-pd mt--2">
            <div class="col-sm-6 col-md-6">
              <div class="card card-stats card-round">
                <div class="card-body ">
                  <div class="row">
                    <div class="col-12">
                      <?php
                      if (!is_null($this->session->flashdata('m_error'))) { ?>
                        <div class="clearfix"></div>
                        <div class="alert alert-danger alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <?php echo $this->session->flashdata('m_error'); ?>
                        </div>
                      <?php } ?>

                      <?php if (!is_null($this->session->flashdata('m_success'))) {
                        echo "<div class=\"clearfix\"></div>";
                        echo alert_success($this->session->flashdata('m_success'), TRUE);
                      } ?>

                      <?php echo form_open('auth/verify'); ?>
                      <div class="form-group">
                        <label for="email2">Email</label>
                        <input type="text" name="email" class="form-control" placeholder="Email" required>
                        <span class="text-danger"><?php echo form_error('email'); ?></span>
                      </div>
                      <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control border border-primary" placeholder="Password" required>
                        <span class="text-danger"><?php echo form_error('password'); ?></span>
                      </div>
                      <div class="form-group">
                        <button class="btn btn-success">Login</button>
                      </div>
                      <?php echo form_close(); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
          <div class="copyright ml-auto">
            2021, <?php echo APP_OWNER; ?> - Theme: <i class="fa fa-heart heart text-danger"></i> by <a href="https://www.themekita.com">ThemeKita</a>
          </div>
        </div>
      </footer>
    </div>


    <!--   Core JS Files   -->
    <script src="<?php echo site_url('resources/js/core/jquery.3.2.1.min.js'); ?>"></script>
    <script src="<?php echo site_url('resources/js/core/popper.min.js'); ?>"></script>
    <script src="<?php echo site_url('resources/js/core/bootstrap.min.js'); ?>"></script>

    <!-- jQuery UI -->
    <script src="<?php echo site_url('resources/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js'); ?>"></script>
    <script src="<?php echo site_url('resources/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js'); ?>"></script>

    <!-- jQuery Scrollbar -->
    <script src="<?php echo site_url('resources/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js'); ?>"></script>

    <!-- Chart JS -->
    <script src="<?php echo site_url('resources/js/plugin/chart.js/chart.min.js'); ?>"></script>

    <!-- jQuery Sparkline -->
    <script src="<?php echo site_url('resources/js/plugin/jquery.sparkline/jquery.sparkline.min.js'); ?>"></script>

    <!-- Chart Circle -->
    <script src="<?php echo site_url('resources/js/plugin/chart-circle/circles.min.js'); ?>"></script>

    <!-- Bootstrap Notify -->
    <script src="<?php echo site_url('resources/js/plugin/bootstrap-notify/bootstrap-notify.min.js'); ?>"></script>

    <!-- jQuery Vector Maps -->
    <script src="<?php echo site_url('resources/js/plugin/jqvmap/jquery.vmap.min.js'); ?>"></script>
    <script src="<?php echo site_url('resources/js/plugin/jqvmap/maps/jquery.vmap.world.js'); ?>"></script>

    <!-- Sweet Alert -->
    <script src="<?php echo site_url('resources/js/plugin/sweetalert/sweetalert.min.js'); ?>"></script>

    <!-- Atlantis JS -->
    <script src="<?php echo site_url('resources/js/atlantis.min.js'); ?>"></script>

    <?php if (isset($_usedtable) && $_usedtable == true) { ?>
      <!-- Datatables -->
      <script src="<?php echo site_url('resources/js/plugin/datatables/datatables.min.js'); ?>"></script>

      <script>
        $(document).ready(function() {
          $('#basic-datatables').DataTable({});

          $('#multi-filter-sele   ct').DataTable({
            "pageLength": 5,
            initComplete: function() {
              this.api().columns().every(function() {
                var column = this;
                var select = $('<select class="form-control"><option value=""></option></select>')
                  .appendTo($(column.footer()).empty())
                  .on('change', function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                      $(this).val()
                    );

                    column
                      .search(val ? '^' + val + '$' : '', true, false)
                      .draw();
                  });

                column.data().unique().sort().each(function(d, j) {
                  select.append('<option value="' + d + '">' + d + '</option>')
                });
              });
            }
          });

          // Add Row
          // $('#add-row').DataTable({
          //     "pageLength": 5,
          // });

          // var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

          // $('#addRowButton').click(function() {
          //     $('#add-row').dataTable().fnAddData([
          //         $("#addName").val(),
          //         $("#addPosition").val(),
          //         $("#addOffice").val(),
          //         action
          //     ]);
          //     $('#addRowModal').modal('hide');

          // });
        });
      </script>
    <?php } ?>

    <?php if (isset($_usedselect2) && $_usedselect2 == true) { ?>
      <!-- Select2 -->
      <script src="<?php echo site_url('resources/js/select2.full.min.js'); ?>"></script>
      <script>
        //Initialize Select2 Elements
        $('.select2').select2()
      </script>
    <?php } ?>

</body>

</html>