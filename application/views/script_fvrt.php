<script>
	$(document).ready(function() {
		$("#btn_heart").click(function() {
			const base_url = '<?= site_url() ?>';
			const user = '<?= user_data('id_user') ?>';

			if (user == '') {
				window.location.href = base_url + 'login';
			} else {
				$.ajax({
					url: "<?php echo site_url('toko/fvrt_porduk') ?>",
					type: "POST",
					data: {
						_token_validate_fv: $("#_token_unique_prv").val(),
					},
					dataType: "JSON",
					success: function(data) {
						if (data.result == true) {
							if (data.favorit == 1) {
								$("#btn_heart").addClass("text-danger");
							} else {
								$("#btn_heart").removeClass("text-danger");
							}
							$('.badge-heart').html(data.c_fav);
						} else {
							alert(data.message);
						}
					},
					error: function(e) {
						alert(e.status);
					}
				});
			}
		});
	});

	function D3DRG6W() {
		$.ajax({
			url: "<?php echo site_url('toko/liaotianwo') ?>",
			type: "POST",
			data: {
				_token_validate_fv: $("#_token_unique_prv").val(),
			},
			dataType: "JSON",
			success: function(data) {
				if (data.result == true) {
					let detail = data.data;
					detail.forEach((item) => {
						if (item.user_id == data.chat.user_id_pembeli) {
							$('.msg-page').append(`<div class="sender-chats"><div class = "sender-msg"><p> ${item.isi_text} </p><span class = "time" > ${item.tanggal} </span></div></div>`);
						} else {
							$('.msg-page').append(`<div class="received-chats"><div class = "received-msg"><div class = "received-msg-inbox" ><p> ${item.isi_text} </p><span class = "time" > ${item.tanggal} </span> </div> </div> </div>`);
						}
					});
				} else {
					alert(data.message);
				}
			},
			error: function(e) {
				alert(e.status);
			}
		});
	}
</script>
