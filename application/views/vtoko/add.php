<section class="hero">
	<center>
		<h3>Pasang Produk Anda</h3>
	</center>
	<div class="container">
		<?php if (!is_null($this->session->flashdata('m_error'))) {
			echo "<div class=\"clearfix\"></div>";
			echo alert_danger($this->session->flashdata('m_error'), TRUE);
		} ?>

		<?php if (validation_errors()) {
			echo "<div class=\"clearfix\"></div>";
			echo alert_danger(validation_errors(), TRUE);
		} ?>

		<?= form_open_multipart('toko/add') ?>
		<div class="row">
			<div class="col-md-3 col-xs-12" style="margin-bottom: 12px;">
				<label class="col-md-12">Kategori</label>
				<div class="col-md-12">
					<select name="kategori" class="form-control">
						<?php
						foreach ($kategori as $val) {
							if (set_value('kategori') == $val['id']) {
								echo '<option value="' . $val['id'] . '" selected >' . $val['nama_kategori'] . '</option>';
							} else {
								echo '<option value="' . $val['id'] . '" >' . $val['nama_kategori'] . '</option>';
							}
						}
						?>
					</select>
					<span class="text-danger"><?= form_error('kategori') ?></span>
				</div>
			</div>


			<div class="col-md-4">
				<label class="col-md-12">Kecamatan</label>
				<div class="col-md-12">
					<select name="kecamatan" class="form-control">
						<?php
						foreach ($kecamatan as $val) {
							if (set_value('kecamatan') == $val['id']) {
								echo '<option value="' . $val['id'] . '" selected >' . $val['nama_kotakabupaten'] . ' - ' . $val['nama_kecamatan'] . '</option>';
							} else {
								echo '<option value="' . $val['id'] . '" >' . $val['nama_kotakabupaten'] . ' - ' . $val['nama_kecamatan'] . '</option>';
							}
						}
						?>
					</select>
					<span class="text-danger"><?= form_error('kecamatan') ?></span>
				</div>
			</div>

			<div class="col-md-3">
				<label class="col-md-12">Kondisi</label>
				<div class="col-md-12">
					<select name="kondisi" class="form-control">
						<?php
						foreach (kondisi_produk() as $key => $val) {
							if (set_value('kondisi') == $key) {
								echo '<option value="' . $key . '" selected >' . $val . '</option>';
							} else {
								echo '<option value="' . $key . '" >' . $val . '</option>';
							}
						}
						?>
					</select>
					<span class="text-danger"><?= form_error('kondisi') ?></span>
				</div>
			</div>

		</div>
		<div class="form-group">
			<label class="col-md-12">Nama Produk <span class="count_nm">(0 / 70)</span></label>
			<div class="col-md-12">
				<input type="text" name="nm_produk" id="nm_produk" class="form-control" maxlength="70" required />
			</div>
			<span class="text-danger" id="info_nm"><?= form_error('nm_produk') ?></span>
		</div>

		<div class="form-group">
			<label class="col-md-12">Harga Produk</label>
			<div class="col-md-12">
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Rp.</span>
					<input type="text" name="hrg_produk" class="form-control" placeholder="Harga Produk" aria-label="Harga" aria-describedby="basic-addon1">
				</div>
			</div>
			<span class="text-danger" id="info_hrg"><?= form_error('hrg_produk') ?></span>
		</div>

		<div class="form-group">
			<label class="col-md-12">Deskripsik <span class="count_desc">(0 / 4000)</span></label>
			<div class="col-md-12">
				<textarea class="form-control" name="deskripsi" rows="5" id="deskripsi" maxlength="4000" required></textarea>
			</div>
			<span class="text-danger" id="info_desc"><?= form_error('deskripsi') ?></span>
		</div>

		<style>
			img {
				max-width: 180px;
				max-height: 180px;
			}
		</style>

		<div class="form-group">
			<label class="col-md-12">Foto Depan .jpg .jpeg</label>
			<div class="row">
				<div class="col-md-4">
					<img class="blah" src="<?= base_url('assets/img/icon_img.png') ?>" alt="your image" />
				</div>
				<div class="col-md-8">
					<input type="file" name="foto_dpn" class="form-control img_file" required />
					<span class="text-danger file_error"></span>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-12">Foto tampak atas .jpg .jpeg</label>
			<div class="row">
				<div class="col-md-4">
					<img class="blah" src="<?= base_url('assets/img/icon_img.png') ?>" alt="your image" />
				</div>
				<div class="col-md-8">
					<input type="file" name="foto_ats" class="form-control img_file" required />
					<span class="text-danger file_error"></span>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-12">Foto tampak kanan .jpg .jpeg</label>
			<div class="row">
				<div class="col-md-4">
					<img class="blah" src="<?= base_url('assets/img/icon_img.png') ?>" alt="your image" />
				</div>
				<div class="col-md-8">
					<input type="file" name="foto_knn" class="form-control img_file" required />
					<span class="text-danger file_error"></span>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-12">Foto tampak kiri .jpg .jpeg</label>
			<div class="row">
				<div class="col-md-4">
					<img class="blah" src="<?= base_url('assets/img/icon_img.png') ?>" alt="your image" />
				</div>
				<div class="col-md-8">
					<input type="file" name="foto_kri" class="form-control img_file" required />
					<span class="text-danger file_error"></span>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-12">Foto tampak Belakang .jpg .jpeg</label>
			<div class="row">
				<div class="col-md-4">
					<img class="blah" src="<?= base_url('assets/img/icon_img.png') ?>" alt="your image" />
				</div>
				<div class="col-md-8">
					<input type="file" name="foto_blk" class="form-control img_file" required />
					<span class="text-danger file_error"></span>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<button type="submit" id="btn_sub" class="btn pasang" disabled>Pasang Produk Sekarang</button>
			</div>
		</div>

		<?= form_close() ?>
	</div>
</section>
