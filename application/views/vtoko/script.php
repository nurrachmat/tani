<script>
	$(document).ready(function() {
		let nmproduk = $('#nm_produk').val().trim();
		let deskripsi = $('#deskripsi').val();
		$(".count_nm").html(`(${nmproduk.length} / 70)`);
		$(".count_desc").html(`(${deskripsi.length} / 4000)`);
		if (nmproduk.length < 15 && deskripsi.length < 20) {
			$("#info_nm").html("Nama Produk minimal 15 Karakter");
			$("#btn_sub").prop("disabled", true);
		} else if (nmproduk.length > 70) {
			$("#info_nm").html("Nama Produk maksimal 70 Karakter");
		} else {
			$("#info_nm").html("");
			$("#btn_sub").prop("disabled", false);
		}

		$("#nm_produk").keyup(function() {
			let nmproduk = $('#nm_produk').val().trim();
			let deskripsi = $('#deskripsi').val();
			$(".count_nm").html(`(${nmproduk.length} / 70)`);
			if (nmproduk.length < 15 && deskripsi.length < 20) {
				$("#info_nm").html("Nama Produk minimal 15 Karakter");
				$("#btn_sub").prop("disabled", true);
			} else if (nmproduk.length > 70) {
				$("#info_nm").html("Nama Produk maksimal 70 Karakter");
			} else {
				$("#info_nm").html("");
			}
		});

		$("#deskripsi").keyup(function() {
			let deskripsi = $('#deskripsi').val();
			let nmproduk = $('#nm_produk').val().trim();

			$(".count_desc").html(`(${deskripsi.length} / 4000)`);
			if (nmproduk.length < 15 && deskripsi.length < 20) {
				$("#info_desc").html("Deskripsi minimal 20 Karakter");
				$("#btn_sub").prop("disabled", true);
			} else if (deskripsi.length > 4000) {
				$("#info_desc").html("Deskripsi maksimal 4000 Karakter");
			} else {
				$("#info_desc").html("");
				$("#btn_sub").prop("disabled", false);
			}
		});
	});

	$(".img_file").change(function() {
		let input = this;
		if (input.files && input.files[0]) {
			var pathFile = input.files[0].name;
			var ekstensiOk = /(\.jpg|\.jpeg)$/i;

			if (!ekstensiOk.exec(pathFile)) {
				$(input).parents('.form-group').find('.file_error').html('Silakan upload file yang memiliki ekstensi .jpeg/.jpg');
				alert('Silakan upload file yang memiliki ekstensi .jpeg/.jpg');
				$(input).parents('.row').find('.blah').attr('src', '../assets/img/icon_img.png');

			} else if (input.files[0].size >= 550000) {
				$(input).parents('.form-group').find('.file_error').html('Ukuran File terlalu besar, Maksimum File 500 KB');
				alert('Ukuran File terlalu besar, Maksimum File 500 KB');
				$(input).parents('.row').find('.blah').attr('src', '../assets/img/icon_img.png');

			} else {
				var reader = new FileReader();
				reader.onload = function(e) {
					$(input).parents('.row').find('.blah')
						.attr('src', e.target.result);
				};

				reader.readAsDataURL(input.files[0]);
				$(input).parents('.form-group').find('.file_error').html('');
			}
		}
	});
</script>
