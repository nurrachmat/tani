<section class="hero">
	<div class="container">
		<div class="row">
			<?php
			if (count($produk) >  0) {
				echo '
				<div class="col-md-12">
					<center><a href="' . site_url('toko/add') . '" class="btn pasang" >
						<i class="fa fa-plus"></i> Tambah Produk
					</a></center>
					<hr/>
				</div>
				';
				foreach ($produk as $val) { ?>
					<div class="col-md-3 col-sm-6 mix oranges fresh-meat">
						<div class="featured__item">
							<div class="featured__item__pic set-bg" data-setbg="<?= base_url('uploads/produk/' . $val['foto']) ?>">
								<ul class="featured__item__pic__hover">
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-retweet"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
								</ul>
							</div>
							<div class="featured__item__text">
								<h6>
									<a href="<?= site_url('produk/detail/' . $val['id'] . '/' . url_title($val['nama_produk'])) ?>">
										<?= $val['nama_produk']; ?>
									</a>
								</h6>
								<h6 class="marker"><i class="fa fa-map-marker"></i> <?= $val['nama_kecamatan'] ?></h6>
								<h5>Rp. <?= format_angka($val['harga']); ?>,- </h5>
							</div>
						</div>
					</div>
				<?php }
			} else { ?>
				<div class="zeroprd">
					<img src="<?= base_url('assets/img/boxbin.png') ?>">
					<p class="title">Anda belum menjual panen </p>
					<p class="subtitle">Jual Panen terbaik anda DISINI</p>
					<a class="btn pasang" href="<?= site_url('toko/add') ?>">
						Pasang Produk</a>
				</div>
			<?php } ?>
		</div>
		<?php echo $this->pagination->create_links(); ?>
	</div>
</section>
