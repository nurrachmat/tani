<section class="hero">
	<div class="container">
		<div class="d-flex justify-content-between flex-wrap">
			<h3>Ubah Produk Anda</h3>
			<a href="<?= site_url('produk/detail/' . $produk['id'] . '/' . url_title($produk['nama_produk'])) ?>" class="btn btn-xs btn-primary">
				<i class="fa fa-eye"></i> Lihat Produk
			</a>
		</div>

		<hr />

		<?php if (!is_null($this->session->flashdata('m_error'))) {
			echo "<div class=\"clearfix\"></div>";
			echo alert_danger($this->session->flashdata('m_error'), TRUE);
		} ?>

		<?= form_open_multipart('toko/ubah/' . $produk['id']) ?>
		<div class="row">
			<div class="col-md-3 col-xs-12" style="margin-bottom: 12px;">
				<label class="col-md-12">Kategori</label>
				<div class="col-md-12">
					<select name="kategori" class="form-control">
						<?php
						foreach ($kategori as $val) {
							if (set_value('kategori', $produk['kategori_id']) == $val['id']) {
								echo '<option value="' . $val['id'] . '" selected >' . $val['nama_kategori'] . '</option>';
							} else {
								echo '<option value="' . $val['id'] . '" >' . $val['nama_kategori'] . '</option>';
							}
						}
						?>
					</select>
					<span class="text-danger"><?= form_error('kategori') ?></span>
				</div>
			</div>


			<div class="col-md-4">
				<label class="col-md-12">Kecamatan</label>
				<div class="col-md-12">
					<select name="kecamatan" class="form-control">
						<?php
						foreach ($kecamatan as $val) {
							if (set_value('kecamatan', $produk['kecamatan_id']) == $val['id']) {
								echo '<option value="' . $val['id'] . '" selected >' . $val['nama_kotakabupaten'] . ' - ' . $val['nama_kecamatan'] . '</option>';
							} else {
								echo '<option value="' . $val['id'] . '" >' . $val['nama_kotakabupaten'] . ' - ' . $val['nama_kecamatan'] . '</option>';
							}
						}
						?>
					</select>
					<span class="text-danger"><?= form_error('kecamatan') ?></span>
				</div>
			</div>

			<div class="col-md-3">
				<label class="col-md-12">Kondisi</label>
				<div class="col-md-12">
					<select name="kondisi" class="form-control">
						<?php
						$kds = array("N" => "Baru", "O" => "Bekas");
						foreach ($kds as $key => $val) {
							if (set_value('kondisi', $produk['kondisi']) == $key) {
								echo '<option value="' . $key . '" selected >' . $val . '</option>';
							} else {
								echo '<option value="' . $key . '" >' . $val . '</option>';
							}
						}
						?>
					</select>
					<span class="text-danger"><?= form_error('kondisi') ?></span>
				</div>
			</div>

		</div>
		<div class="form-group">
			<label class="col-md-12">Nama Produk <span class="count_nm">(0 / 70)</span></label>
			<div class="col-md-12">
				<input type="text" name="nm_produk" id="nm_produk" class="form-control" maxlength="70" value="<?= set_value('nm_produk', $produk['nama_produk']) ?>" required />
			</div>
			<span class="text-danger" id="info_nm"><?= form_error('nm_produk') ?></span>
		</div>

		<div class="form-group">
			<label class="col-md-12">Harga Produk</label>
			<div class="col-md-12">
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Rp.</span>
					<input type="text" name="hrg_produk" class="form-control" placeholder="Harga Produk" value="<?= set_value('nm_produk', $produk['harga']) ?>" aria-label="Harga" aria-describedby="basic-addon1">
				</div>
			</div>
			<span class="text-danger" id="info_hrg"><?= form_error('hrg_produk') ?></span>
		</div>

		<div class="form-group">
			<label class="col-md-12">Deskripsik <span class="count_desc">(0 / 4000)</span></label>
			<div class="col-md-12">
				<textarea class="form-control" name="deskripsi" rows="5" id="deskripsi" maxlength="4000" required><?= set_value('nm_produk', $produk['deskripsi']) ?></textarea>
			</div>
			<span class="text-danger" id="info_desc"><?= form_error('deskripsi') ?></span>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				<button type="submit" id="btn_sub" class="btn pasang" disabled>Ubah Produk</button>
			</div>
		</div>

		<?= form_close() ?>

		<hr />

		<style>
			img {
				max-width: 180px;
				max-height: 180px;
			}
		</style>

		<?php
		if (!is_null($this->session->flashdata('error'))) {
			echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
		}
		foreach ($foto as $val) {
			echo form_open_multipart('toko/picture/' . $val['id']);
			$en_token = $this->encryption->encrypt($produk['id']);
			echo '<input type="hidden" name="_token_unique_prv" id="_token_unique_prv" value="' . $en_token . '" > ';

			echo '
				<div class="form-group row">
					<label class="col-md-12">Foto <b>.jpg .jpeg</b></label>
					<div class="col-md-4">
						<img class="blah" src="' . base_url('uploads/produk/' . $val['foto']) . '" alt="your image" />
					</div>
					<div class="col-md-4">
						<input type="file" name="foto_produk" class="form-control img_file" required />
						<span class="text-danger file_error"></span>
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-primary" ><i class="fa fa-edit"></i> Ubah Foto</button>
					</div>
				</div>
			';
			echo form_close();
		}
		?>
	</div>
</section>
