<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/loginstyle.css">
	<title>DPKP - Dinas Pertanian Provinsi Sumatera Selatan</title>
</head>

<body>
	<header class="header">
		<nav class="navbar navbar-light bg-light">
			<a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url() ?>assets/img/logo.png" alt=""> LOG IN</a>
		</nav>
	</header>
	<div id="logreg-forms">
		<?= form_open('login/checking', array('class' => "form-signin")) ?>
		<h1 class="h3 mb-3 font-weight-normal text-center">Log in</h1>
		<?php
		if (validation_errors()) {
			echo '
			<div class="alert alert-danger">
				' . validation_errors() . '
			</div>
			';
		}
		if (!is_null($this->session->flashdata('m_error'))) {
			echo "<div class=\"clearfix\"></div>";
			echo alert_danger($this->session->flashdata('m_error'), TRUE);
		} elseif (!is_null($this->session->flashdata('m_success'))) {
			echo "<div class=\"clearfix\"></div>";
			echo alert_success($this->session->flashdata('m_success'), TRUE);
		} ?>

		<div class="input-group form-group">
			<div class="input-group-prepend">
				<span class="input-group-text bg-white"><i class="fa fa-user"></i></span>
			</div>
			<input type="text" name="email" class="form-control" placeholder="username">
		</div>
		<div class="input-group form-group">
			<div class="input-group-prepend">
				<span class="input-group-text bg-white"><i class="fa fa-key"></i></span>
			</div>
			<input type="password" name="password" class="form-control" placeholder="password">
		</div>
		<button class="btn btn-success btn-block" type="submit"><i class="fa fa-sign-in"></i> Log in</button>
		<hr>
		<!-- <p>Don't have an account!</p>  -->
		<div class="text-black">
			Don't have an account?
			<a href="#" id="btn-signup"> Sign up </a> <br />
			<a href="#" id="forgot_pswd">Forgot password?</a>
		</div>
		<?= form_close() ?>

		<?= form_open('login/reset_password', array('class' => "form-reset")); ?>
		<h1 class="h3 mb-3 font-weight-normal text-center">Reset Password</h1>
		<input type="email" name="email" id="resetEmail" class="form-control" placeholder="Email address" required autofocus>
		<button type="submit" class="btn btn-primary btn-block">Reset Password</button>
		<a href="#" id="cancel_reset"><i class="fa fa-angle-left"></i> Kembali</a>
		<?= form_close() ?>


		<?= form_open('login/signup', array('class' => "form-signup")); ?>
		<h1 class="h3 mb-3 font-weight-normal text-center">Daftar</h1>
		<div class="form-group">
			<input type="text" name="nmuser" id="user-name" class="form-control" placeholder="Nama Lengkap" value="<?= set_value('nmuser') ?>" required autofocus>
		</div>
		<div class="form-group">
			<input type="email" name="regis_email" id="regis_email" class="form-control" placeholder="Email address" value="<?= set_value('regis_email') ?>" required>
		</div>

		<div class="form-group">
			<input type="number" name="hp" id="user-hp" class="form-control" placeholder="Nomor HP" value="<?= set_value('hp') ?>" required>
		</div>

		<div class="form-group">
			<input type="password" name="regis_password" id="regis_password" class="form-control" placeholder="Password" required>
		</div>
		<div class="form-group">
			<input type="password" name="confpass" id="user-repeatpass" class="form-control" placeholder="Konfirmasi Password" required>
		</div>
		<div class="form-group">
			<button class="btn btn-primary btn-block" type="submit"><i class="fa fa-user-plus"></i> Daftar</button>
			<a href="#" id="cancel_signup"><i class="fa fa-angle-left"></i> Kembali</a>
		</div>
		<?= form_close() ?>

		<br>

	</div>
	<p style="text-align:center">
		<a href="http://bit.ly/2RjWFM"></a>
	</p>

	<?php
	$this->load->view('layouts/footer');
	?>
	<script src="<?= base_url() ?>assets/js/jquery-3.3.1.min.js"></script>
	<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/js/login.js"></script>

</body>

</html>
