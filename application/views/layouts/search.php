<div class="hero__search">
	<div class="hero__search__form">
		<?= form_open('produk', array("method" => "GET")); ?>
		<div class="hero__search__categories">
			Semua Kategori
		</div>
		<input type="text" name="serachproduct" placeholder="What do yo u need?">
		<button type="submit" class="site-btn">SEARCH</button>
		</form>
	</div>
	<div class="hero__search__phone">
		<div class="hero__search__phone__icon">
			<i class="fa fa-phone"></i>
		</div>
		<div class="hero__search__phone__text">
			<h5>+65 11.188.888</h5>
			<span>support 24/7 time</span>
		</div>
	</div>
</div>
