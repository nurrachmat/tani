<!DOCTYPE html>
<html lang="zxx">

<head>
	<meta charset="UTF-8">
	<meta name="description" content="TANI Corps">
	<meta name="keywords" content="pertanian, perkebunan, beras, ikan, teh, kopi, sawit">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="<?= base_url() ?>assets/img/favicon.png" rel="icon">
	<title>Tani | Berbelanjan dari Petani Langsung</title>

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

	<!-- Css Styles -->
	<link href="<?= base_url() ?>assets/img/favicon.png" rel="icon">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/elegant-icons.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/nice-select.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery-ui.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/slicknav.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/chatbox.css" type="text/css">
</head>

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Humberger Begin -->
	<div class="humberger__menu__overlay"></div>
	<div class="humberger__menu__wrapper">
		<div class="humberger__menu__logo">
			<a href="#"><img src="<?= base_url() ?>assets/img/logo.png" alt=""></a>
		</div>
		<div class="humberger__menu__widget">
			<div class="header__top__right__auth">
				<?php
				if (user_data('nama_user')) {
					echo '<a href="#"><i class="fa fa-user"></i>' . user_data('nama_user') . '</a>';
				} else {
					echo '<a href="#"><i class="fa fa-user"></i>Login</a>';
				} ?>
			</div>
		</div>
		<nav class="humberger__menu__nav mobile-menu">
			<ul>
				<li class="active"><a href="./index.html">Home</a></li>
				<li><a href="./contact.html">Contact</a></li>
			</ul>
		</nav>
		<div id="mobile-menu-wrap"></div>
		<div class="header__top__right__social">
			<a href="#"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
			<a href="#"><i class="fa fa-linkedin"></i></a>
			<a href="#"><i class="fa fa-pinterest-p"></i></a>
		</div>
		<div class="humberger__menu__contact">
			<ul>
				<li><i class="fa fa-envelope"></i> hello@tani.com</li>
				<li>Berbelanja dari Petani Langsung</li>
			</ul>
		</div>
	</div>
	<!-- Humberger End -->

	<!-- Header Section Begin -->
	<header class="header">
		<div class="header__top">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6">
						<div class="header__top__left">
							<ul>
								<li><i class="fa fa-envelope"></i> hello@tani.com</li>
								<li>Berbelanja dari Petani Langsung</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-6 col-md-6">
						<div class="header__top__right">
							<div class="header__top__right__social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-linkedin"></i></a>
								<a href="#"><i class="fa fa-pinterest-p"></i></a>
							</div>

							<div class="header__top__right__auth">
								<?php
								$produk_id_md5 = (isset($produk['id']) ? md5($produk['id']) : '');
								echo '<input type="hidden" name="_token_unique_prv" id="_token_unique_prv" value="' . $produk_id_md5 . '" > ';
								if (user_data('nama_user')) {
									echo '
									<div class="header__top__right__language">
										<div><i class="fa fa-user"></i> ' . user_data('nama_user') . '</div>
										<span class="arrow_carrot-down"></span>
										<ul>
											<li><a href="' . site_url('profile') . '">Profile Saya</a></li>
											<li><a href="' . site_url('favorit') . '">Favorit Saya</a></li>
											<li><a href="' . site_url('logout') . '">Log Out</a></li>
										</ul>
									</div>';
								} else {
									echo '<a href="' . site_url('login') . '"><i class="fa fa-user"></i>Login</a>';
								} ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<div class="header__logo">
						<a href="<?= base_url() ?>"><img src="<?= base_url() ?>assets/img/logo.png" alt=""></a>
						<input type="hidden" id="_token_t4n1_dinamic" value="<?= (isset($chat['uuid']) ? $chat['uuid'] : '0') ?>">
					</div>
				</div>
				<div class="col-lg-6">
					<nav class="header__menu">
						<ul>
							<li <?= ((isset($currentPage) and $currentPage == 'home') ? 'class="active"' : '') ?>><a href="<?= site_url('produk') ?>">Home</a></li>
							<?php
							if (user_data('id_user')) {
								echo '<li ' . ((isset($currentPage) and $currentPage == 'lapak') ? 'class="active"' : '') . ' ><a href="' . site_url('toko') . '">Lapak Anda</a></li>';
							} ?>
							<li><a href="./contact.html">Contact</a></li>
						</ul>
					</nav>
				</div>
			</div>
			<div class="humberger__open">
				<i class="fa fa-bars"></i>
			</div>
		</div>
	</header>
	<!-- Header Section End -->

	<!-- BODY -->
	<?php if (isset($_view) && $_view)
		$this->load->view($_view);
	?>
	<!-- END BODY -->


	<?php
	$this->load->view('layouts/footer');
	?>

	<?php
	if (isset($_chat) && $_chat == true) {
		$this->load->view('chat');
	}
	?>

	<!-- Js Plugins -->
	<script src="<?= base_url() ?>assets/js/jquery-3.3.1.min.js"></script>
	<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/js/jquery.nice-select.min.js"></script>
	<script src="<?= base_url() ?>assets/js/jquery-ui.min.js"></script>
	<script src="<?= base_url() ?>assets/js/jquery.slicknav.js"></script>
	<script src="<?= base_url() ?>assets/js/mixitup.min.js"></script>
	<script src="<?= base_url() ?>assets/js/owl.carousel.min.js"></script>
	<script src="<?= base_url() ?>assets/js/moment/min/moment.min.js"></script>
	<script src="<?= base_url() ?>assets/js/main.js"></script>
	<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase.js"></script>
	<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-app.js"></script>

	<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-database.js"></script>

	<?php if (isset($_script) && $_script)
		$this->load->view($_script);
	?>

	<script>
		var firebaseConfig = {
			apiKey: "AIzaSyDiTxccVMLSRIaF5drYSQr0UJ8gMSpbujA",
			authDomain: "dpkp-176ff.firebaseapp.com",
			databaseURL: "https://dpkp-176ff-default-rtdb.firebaseio.com/",
			projectId: "dpkp-176ff",
			storageBucket: "dpkp-176ff.appspot.com",
			messagingSenderId: "514866310715",
			appId: "1:514866310715:android:f36b5087f819d7562f761e",
		};
		firebase.initializeApp(firebaseConfig);
		var database = firebase.database();

		$(document).ready(function() {
			$('.chat-toggle').click(function() {
				const base_url = '<?= site_url() ?>';
				const user = '<?= user_data('id_user') ?>';

				if (user == '') {
					window.location.href = base_url + 'login';
				} else {
					$('.chat-main').slideToggle("slow");
					$('.chat-toggle').toggle();
				}
			});

			$('.chevron-down').click(function() {
				$('.chat-main').slideToggle("slow");
				$('.chat-toggle').toggle();
			});

			const c13h47i5 = "<?= user_data('id_user') ?>";
			let private_msg = $("#_token_private_prd").val();
			if (private_msg === '0') {
				private_msg = database.ref('messages_detail').push().key;
				$("#_token_private_prd").val(private_msg);
			}

			// console.log(private_msg);
			$('#bth-cht-plane').click(function() {
				var d = new Date();
				var n = d.getTime();
				let input_cht = $('#input_cht').val();
				let private_msg = $("#_token_t4n1_dinamic").val();
				if (private_msg === '0') {
					private_msg = database.ref('messages_detail').push().key;
					$("#_token_t4n1_dinamic").val(private_msg);
				}
				console.log(private_msg);
				if (input_cht.trim() != "") {
					$.ajax({
						url: "<?php echo site_url('toko/storeliao') ?>",
						type: "POST",
						data: {
							_token_validate_fv: $("#_token_unique_prv").val(),
							_token_private_prd: private_msg,
							isi_cht: input_cht,
						},
						dataType: "JSON",
						success: function(data) {
							if (data.result == true) {
								database.ref('messages_detail/' + private_msg + '/' + n).set({
									message: input_cht,
									penjualId: data.penjual,
									senderId: data.sender,
									recivedId: data.recived,
									produkId: data.produk,
								});

								$('#input_cht').val('');
								console.log(data);
								let path_front = data.penjual + '_' + data.recived + '_' + data.produk;
								database.ref('messages_front/' + path_front).set({
									chatID: private_msg,
									lastMessage: input_cht,
									nmBuy: data.nmpenjual,
									nmSell: data.nmsender,
									nmProduk: data.nmproduk,
									DD8444fc9ee1bb: data.DD8444fc9ee1bb,
									timestamp: n
								});

							}
						}
					});
				}

			});

			// detailchat(private_msg)


			database.ref('messages_front').on('child_added', function(data) {
				let isi = data.key;
				let sp_isi = isi.split("_");
				let active = '';
				if (data.val().chatID == private_msg) {
					active = 'active-list';
				} else {
					active = '';
				}
				if (sp_isi[0] == c13h47i5) {
					$('.chat-left-list').append(`<button class="all-group ${active} " onclick="detailchat('${data.val().chatID}')">
					<div class="group-img">
						<img src="https://i.pinimg.com/originals/f1/da/a7/f1daa70c9e3343cebd66ac2342d5be3f.jpg">
					</div>
					<div class="chatContent">
						<p class="chatName"> ${data.val().nmSell} </p>
						<p class="listprd">${data.val().nmProduk}</p>
						<span class="css-r3d">Pembeli</span>
					</div>
				</button>`);
				}
				if (sp_isi[1] == c13h47i5) {
					$('.chat-left-list').append(`<button class="all-group ${active}" onclick="detailchat('${data.val().chatID}')">
					<div class="group-img">
						<img src="https://i.pinimg.com/originals/f1/da/a7/f1daa70c9e3343cebd66ac2342d5be3f.jpg">
					</div>
					<div class="chatContent">
						<p class="chatName">${data.val().nmBuy}</p>
						<p class="listprd">${data.val().nmProduk}</p>
						<span class="css-dpa5md">Penjual</span>
					</div>
				</button>`);
				}
			});
		});
	</script>

	<script>
		var listener = '';

		$(".more").click(function() {
			$(".complete").toggle();
			$(".teaser").toggle();
			if ($('.complete').is(':visible')) {
				$(".more").text('Singkat..');
			} else {
				$(".more").text('Selengkapnya...');
			}
		});

		function detailchat(params) {
			$('.chat-chat').html('');
			$("#_token_t4n1_dinamic").val(params);
			$('.chat-home').hide(500);
			$('.chat-play').show();

			$('.all-group').removeClass('active-list');

			database.ref('messages_front').orderByChild('chatID').equalTo(params).on("value", function(snapshot) {
				snapshot.forEach(function(datas) {
					let akar = datas.key;
					let split_akar = akar.split("_");
					if (split_akar[0] == '<?= user_data('id_user') ?>') {
						$('.active_chat h4').html(datas.val().nmSell);
					} else {
						$('.active_chat h4').html(datas.val().nmBuy);
					}
					$('#prdk_nm').html(datas.val().nmProduk);
					$("#_token_unique_prv").val(datas.val().DD8444fc9ee1bb);
				});
			});

			if (listener) {
				database.ref('messages_detail/' + params).off('child_added', listener);
			}

			listener = database.ref('messages_detail/' + params).on('child_added', function(data) {
				let time = parseInt(data.key);
				let datenew = moment(time).format('DD-MM-YYYY HH:mm:ss');
				if (data.val().senderId == '<?= user_data('id_user') ?>') {
					$('.chat-chat').append(`<div class="sender-chats"><div class = "sender-msg"><p> ${data.val().message} </p><span class = "time" > ${datenew} </span></div></div>`);
					$('#goumai_yonghu').val();
					// console.log(data.val().recivedId + ' beli');
				} else {
					$('.chat-chat').append(`<div class="received-chats"><div class = "received-msg"><div class = "received-msg-inbox" ><p> ${data.val().message} </p><span class = "time" > ${datenew} </span> </div> </div> </div>`);
					$('#xiaoshou_yonghu').val(data.val().penjualId);
					// console.log(data.val().penjualId + ' jual');
				}
				var objDiv = document.getElementById("msg-page-scrl");
				objDiv.scrollTop = objDiv.scrollHeight;
			});
		}
	</script>

</body>

</html>
