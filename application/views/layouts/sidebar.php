<div class="col-lg-3">
	<div class="hero__categories">
		<div class="hero__categories__all">
			<i class="fa fa-bars"></i>
			<span>Semua Kategori</span>
		</div>
		<ul>
			<?php
			foreach ($kategori as $val) {
				echo '<li class="' . (($this->input->get('kategori') == $val['id']) ? 'active' : '') . '" ><a href="' . site_url('produk') . '?kategori=' . $val['id'] . '">' . $val['nama_kategori'] . '</a></li>';
			}
			?>
		</ul>
	</div>
</div>
