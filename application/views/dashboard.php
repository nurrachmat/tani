<section class="hero">
	<div class="container">
		<div class="row">
			<?php
			$this->load->view('layouts/sidebar');
			?>

			<div class="col-lg-9">
				<?php
				$this->load->view('layouts/search');
				?>

				<?php
				if ($this->input->get('toko') && count($produk) > 0) { ?>
					<div class="profile-header">
						<div class="avatar">
							<img src="<?= base_url('assets/img/user-Icon.jpg') ?>" class="image">
						</div>
						<div class="profile-info">
							<h4><?= $toko['nama']; ?></h4>
							<h5><i class="fa fa-phone text-success"></i> <?= $toko['hp'] ?></h5>
							<span class="teaser">
								Deskripsi : <?= str_replace(['<p>', '</p>'], '', substr($toko['deskripsi'], 0, 150)) ?>
							</span>

							<span class="complete"> Deskripsi : <?= $toko['deskripsi'] ?></span>

							<?= (strlen($toko['deskripsi']) > 150 ? '<span class="more">Selengkapnya...</span>' : '') ?>
						</div>
					</div>
				<?php } ?>

				<div class="row featured__filter">

					<?php
					if (count($produk) >  0) {
						foreach ($produk as $val) {
							$cek_fav = $this->Produk_fav_model->findRow_fav(array('produk_id' => $val['id'], 'user_id' => user_data('id_user')));

							if (isset($cek_fav['id'])) {
								$text = 'text-danger';
							} else {
								$text = '';
							}
					?>
							<div class="col-lg-3 col-md-4 col-sm-6 mix oranges fresh-meat">
								<div class="featured__item">
									<div class="featured__item__pic set-bg" data-setbg="<?= base_url('uploads/produk/' . $val['foto']) ?>">
										<ul class="featured__item__pic__hover">
											<li><a href="javascript:void(0)" title="Favorit" class="heart-icon"><i class="fa fa-heart <?= $text ?>"></i></a></li>
											<li><a href="<?= site_url('produk/detail/' . $val['id'] . '/' . url_title($val['nama_produk'])) ?>" title="Detail barang"><i class="fa fa-binoculars"></i></a></li>
										</ul>
									</div>
									<?php
									$en_token = $this->encryption->encrypt($val['id']);
									echo '<input type="hidden" name="_B45s22R46" class="_B45s22R46" value="' . $en_token . '"> ';
									?>
									<div class="featured__item__text">
										<h6>
											<a href="<?= site_url('produk/detail/' . $val['id'] . '/' . url_title($val['nama_produk'])) ?>">
												<?= $val['nama_produk']; ?>
											</a>
										</h6>
										<h6 class="marker"><i class="fa fa-map-marker"></i> <?= $val['nama_kecamatan'] ?></h6>
										<h5>Rp. <?= format_angka($val['harga']); ?>,- </h5>
									</div>
								</div>
							</div>
						<?php }
					} else { ?>
						<h2 class="notfound"><i class="fa fa-search"></i> Produk tidak ditemukan </h2>
					<?php } ?>
				</div>
				<?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</div>
</section>
