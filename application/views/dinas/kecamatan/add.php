<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Kecamatan</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Lokasi</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('dinas/kecamatan/index'); ?>">Kecamatan</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Tambah Data</h4>
						</div>
					</div>
					<?php echo form_open('dinas/kecamatan/add'); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="kotakabupaten_id"><span class="text-danger">*</span>Nama Kota/Kabupaten</label>
										<select name="kotakabupaten_id" class="form-control select2">
											<option value="">Pilih nama kota/kabupaten</option>
											<?php
											foreach ($all_kotakabupaten as $kotakabupaten) {
												$selected = ($kotakabupaten['id'] == $this->input->post('kotakabupaten_id')) ? ' selected="selected"' : "";

												echo '<option value="' . $kotakabupaten['id'] . '" ' . $selected . '>' . $kotakabupaten['nama_provinsi'] . ' - ' . $kotakabupaten['nama_kotakabupaten'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('kotakabupaten_id'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="nama_kecamatan"><span class="text-danger">*</span>Nama Kecamatan</label>
										<input type="text" class="form-control" id="nama_kecamatan" name="nama_kecamatan" placeholder="Isi nama kecamatan" value="<?php echo $this->input->post('nama_kecamatan'); ?>">
										<span class="text-danger"><?php echo form_error('nama_kecamatan'); ?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-action">
						<button class="btn btn-success"><i class="fa fa-check"></i> Save</button>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>