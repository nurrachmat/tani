<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Pemeriksaan Qurban</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Tambah Qurban</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Tambah Qurban</h4>
						</div>
					</div>
					<?= validation_errors() ?>
					<?php echo form_open_multipart('dinas/cekqurban/add'); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="tgl"><span class="text-danger">*</span>Tgl</label>
										<input type="date" name="tgl" value="<?php echo set_value('tgl', date('Y-m-d')); ?>" class="form-control" id="tgl" required />
										<span class="text-danger"><?php echo form_error('tgl'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="kecamatan"><span class="text-danger">*</span>Kecamatan</label>
										<select name="kecamatan" class="form-control select2" required>
											<?php
											foreach ($kecamatan as $v) {
												$selected = ($v['id'] == set_value('kecamatan')) ? ' selected="selected"' : "";

												echo '<option value="' . $v['id'] . '" ' . $selected . '>' . $v['nama_kecamatan'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('kecamatan'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="nama"><span class="text-danger">*</span>Nama</label>
										<input type="text" name="nama" value="<?php echo set_value('nama'); ?>" class="form-control" id="nama" required />
										<span class="text-danger"><?php echo form_error('nama'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="hp">Hp</label>
										<input type="text" name="hp" value="<?php echo set_value('hp'); ?>" class="form-control" id="hp" />
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="alamat"><span class=" text-danger">*</span>Alamat</label>
										<textarea name="alamat" class="form-control" id="alamat" required><?php echo set_value('alamat'); ?></textarea>
										<span class="text-danger"><?php echo form_error('alamat'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="sapi">Sapi</label>
										<input type="number" min="0" name="sapi" value="<?php echo set_value('sapi', 0); ?>" class="form-control" id="sapi" />
									</div>
								</div>

								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="kerbau">Kerbau</label>
										<input type="number" min="0" name="kerbau" value="<?php echo set_value('kerbau', 0); ?>" class="form-control" id="kerbau" />
									</div>
								</div>

								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="kambing">Kambing</label>
										<input type="number" min="0" name="kambing" value="<?php echo set_value('kambing', 0); ?>" class="form-control" id="kambing" />
									</div>
								</div>

								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="domba">Domba</label>
										<input type="number" min="0" name="domba" value="<?php echo set_value('domba', 0); ?>" class="form-control" id="domba" />
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-12">
									<div class="form-group">
										<label for="ket">Ket</label>
										<input type="text" name="ket" value="<?php echo set_value('ket'); ?>" class="form-control" id="ket" />
									</div>
								</div>
							</div>
						</div>
						<div class="row clearfix">
							<div class="card-action">
								<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
