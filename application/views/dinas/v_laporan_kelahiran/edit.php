<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Laporan Kelahiran</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Laporan Kelahiran</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Ubah Laporan Kelahiran</h4>
						</div>
					</div>
					<?= validation_errors() ?>
					<?php echo form_open_multipart('dinas/laporan_kelahiran/edit/' . $laporan_kelahiran['id']); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="telppetugas" class="col-md-4 control-label"><span class="text-danger">*</span>Telp Petugas</label>
										<input type="number" name="telppetugas" value="<?php echo set_value('telppetugas', $laporan_kelahiran['telppetugas']); ?>" class="form-control" id="telppetugas" required />
										<span class="text-danger"><?php echo form_error('telppetugas'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="nmpeternak" class="col-md-4 control-label"><span class="text-danger">*</span>Nama Peternak</label>
										<input type="text" name="nmpeternak" value="<?php echo set_value('nmpeternak', $laporan_kelahiran['nmpeternak']); ?>" class="form-control" id="nmpeternak" required />
										<span class="text-danger"><?php echo form_error('nmpeternak'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="idpeternak" class="col-md-4 control-label"><span class="text-danger">*</span>ID Peternak</label>
										<input type="text" name="idpeternak" value="<?php echo set_value('idpeternak', $laporan_kelahiran['idpeternak']); ?>" class="form-control" id="idpeternak" required />
										<span class="text-danger"><?php echo form_error('idpeternak'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="hppeternak" class="col-md-6 control-label"><span class="text-danger">*</span>HP Peternak</label>
										<input type="number" name="hppeternak" value="<?php echo set_value('hppeternak', $laporan_kelahiran['hppeternak']); ?>" class="form-control" id="hppeternak" required />
										<span class="text-danger"><?php echo form_error('hppeternak'); ?></span>
									</div>
								</div>
								<div class="col-md-8 col-lg-8">
									<div class="form-group">
										<label for="lokasi" class="col-md-6 control-label"><span class="text-danger">*</span>Lokasi</label>
										<input type="text" name="lokasi" value="<?php echo set_value('lokasi', $laporan_kelahiran['lokasi']); ?>" class="form-control" id="lokasi" required />
										<span class="text-danger"><?php echo form_error('lokasi'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="idenhewan" class="col-md-3 control-label"><span class="text-danger">*</span>Identifikasi Hewan</label>
										<input type="text" name="idenhewan" value="<?php echo set_value('idenhewan', $laporan_kelahiran['idenhewan']); ?>" class="form-control" id="idenhewan" required />
										<span class="text-danger"><?php echo form_error('idenhewan'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="nmpedet" class="col-md-6 control-label"><span class="text-danger">*</span>Nama Pedet</label>
										<input type="text" name="nmpedet" value="<?php echo set_value('nmpedet', $laporan_kelahiran['nmpedet']); ?>" class="form-control" id="nmpedet" required />
										<span class="text-danger"><?php echo form_error('nmpedet'); ?></span>
									</div>
								</div>
								<div class="col-md-2 col-lg-2">
									<div class="form-group">
										<label for="jk" class="col-md-2 control-label"><span class="text-danger">*</span>Jenis Kelamin</label>
										<select name="jk" class="form-control" required>
											<?php
											$jk = array('Jantan', 'Betina');
											foreach ($jk as $key => $value) {
												if (set_value('jk', $laporan_kelahiran['jk']) == $value) {
													echo '<option value="' . $value . '" selected >' . $value . '</option>';
												} else {
													echo '<option value="' . $value . '" >' . $value . '</option>';
												}
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('jk'); ?></span>
									</div>
								</div>
								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="tgllahir" class="col-md-3 control-label"><span class="text-danger">*</span>Tanggal Lahir</label>
										<input type="date" name="tgllahir" value="<?php echo set_value('tgllahir', $laporan_kelahiran['tgllahir']); ?>" class="form-control" id="tgllahir" required />
										<span class="text-danger"><?php echo form_error('tgllahir'); ?></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row clearfix">
							<div class="card-action">
								<button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> Ubah</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
