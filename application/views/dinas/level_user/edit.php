<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Level User</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Lokasi</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('dinas/level_user/index'); ?>">Level user</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Ubah Data</h4>
						</div>
					</div>
					<?php echo form_open('dinas/level_user/edit/' . $level_user['id']); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="nama_level"><span class="text-danger">*</span>Nama level_user</label>
										<input type="text" class="form-control" id="nama_level" name="nama_level" placeholder="Isi nama level_user" value="<?php echo ($this->input->post('nama_level') ? $this->input->post('nama_level') : $level_user['nama_level']); ?>">
										<span class="text-danger"><?php echo form_error('nama_level'); ?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-action">
						<button class="btn btn-success"><i class="fa fa-check"></i> Save</button>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>