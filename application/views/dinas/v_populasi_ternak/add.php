<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Populasi Ternak</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Populasi Ternak</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Tambah Populasi Ternak</h4>
						</div>
					</div>
					<?= validation_errors() ?>
					<?php echo form_open_multipart('dinas/populasi_ternak/add'); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="tgl"><span class="text-danger">*</span>Tanggal</label>
										<input type="date" name="tgl" value="<?php echo set_value('tgl', date('Y-m-d')); ?>" class="form-control" id="tgl" required />
										<span class="text-danger"><?php echo form_error('tgl'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="kecamatan"><span class="text-danger">*</span>Kecamatan</label>
										<select name="kecamatan" class="form-control select2" required>
											<?php
											foreach ($kecamatan as $v) {
												$selected = ($v['id'] == set_value('kecamatan')) ? ' selected="selected"' : "";

												echo '<option value="' . $v['id'] . '" ' . $selected . '>' . $v['nama_kecamatan'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('kecamatan'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="sapi_perah" class="col-md-4 control-label"><span class="text-danger">*</span>Sapi Perah</label>
										<input type="number" name="sapi_perah" value="<?php echo set_value('sapi_perah', 0); ?>" class="form-control" id="sapi_perah" />
										<span class="text-danger"><?php echo form_error('sapi_perah'); ?></span>
									</div>
								</div>
								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="sapi_potong" class="col-md-4 control-label"><span class="text-danger">*</span>Sapi Potong</label>
										<input type="number" name="sapi_potong" value="<?php echo set_value('sapi_potong', 0); ?>" class="form-control" id="sapi_potong" />
										<span class="text-danger"><?php echo form_error('sapi_potong'); ?></span>
									</div>
								</div>
								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="kerbau" class="col-md-4 control-label"><span class="text-danger">*</span>Kerbau</label>
										<input type="number" name="kerbau" value="<?php echo set_value('kerbau', 0); ?>" class="form-control" id="kerbau" />
										<span class="text-danger"><?php echo form_error('kerbau'); ?></span>
									</div>
								</div>
								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="kambing" class="col-md-4 control-label"><span class="text-danger">*</span>Kambing</label>
										<input type="number" name="kambing" value="<?php echo set_value('kambing', 0); ?>" class="form-control" id="kambing" />
										<span class="text-danger"><?php echo form_error('kambing'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="domba" class="col-md-4 control-label"><span class="text-danger">*</span>Domba</label>
										<input type="number" name="domba" value="<?php echo set_value('domba', 0); ?>" class="form-control" id="domba" />
										<span class="text-danger"><?php echo form_error('domba'); ?></span>
									</div>
								</div>
								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="kuda" class="col-md-4 control-label"><span class="text-danger">*</span>Kuda</label>
										<input type="number" name="kuda" value="<?php echo set_value('kuda', 0); ?>" class="form-control" id="kuda" />
										<span class="text-danger"><?php echo form_error('kuda'); ?></span>
									</div>
								</div>
								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="babi" class="col-md-4 control-label"><span class="text-danger">*</span>babi</label>
										<input type="number" name="babi" value="<?php echo set_value('babi', 0); ?>" class="form-control" id="babi" />
										<span class="text-danger"><?php echo form_error('babi'); ?></span>
									</div>
								</div>
								<div class="col-md-3 col-lg-3">
									<div class="form-group">
										<label for="ayam_buras" class="col-md-4 control-label"><span class="text-danger">*</span>Ayam Buras</label>
										<input type="number" name="ayam_buras" value="<?php echo set_value('ayam_buras', 0); ?>" class="form-control" id="ayam_buras" />
										<span class="text-danger"><?php echo form_error('ayam_buras'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="ayam_pedaging" class="col-md-4 control-label"><span class="text-danger">*</span>Ayam Pedaging</label>
										<input type="number" name="ayam_pedaging" value="<?php echo set_value('ayam_pedaging', 0); ?>" class="form-control" id="ayam_pedaging" />
										<span class="text-danger"><?php echo form_error('ayam_pedaging'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="ayam_petelur" class="col-md-4 control-label"><span class="text-danger">*</span>Ayam Petelur</label>
										<input type="number" name="ayam_petelur" value="<?php echo set_value('ayam_petelur', 0); ?>" class="form-control" id="ayam_petelur" />
										<span class="text-danger"><?php echo form_error('ayam_petelur'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="itik" class="col-md-4 control-label"><span class="text-danger">*</span>Itik</label>
										<input type="number" name="itik" value="<?php echo set_value('itik', 0); ?>" class="form-control" id="itik" />
										<span class="text-danger"><?php echo form_error('itik'); ?></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row clearfix">
							<div class="card-action">
								<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
