<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Luas Areal Produksi</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Luas Areal Produksi</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Tambah Luas Areal Produksi</h4>
						</div>
					</div>
					<?= validation_errors() ?>
					<?php echo form_open_multipart('dinas/luas_areal_produksi/add'); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="tahun"><span class="text-danger">*</span>Tahun</label>
										<select name="tahun" class="form-control select2">
											<?php
											for ($i = '2020'; $i <= date('Y'); $i++) {
												echo '<option value="' . $i . '" >' . $i . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('tahun'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="kelurahan"><span class="text-danger">*</span>Kelurahan</label>
										<select name="kelurahan" class="form-control select2" required>
											<option value="">-- Pilih Kelurahan --</option>
											<?php
											foreach ($kelurahan as $v) {
												$selected = ($v['id'] == set_value('kelurahan')) ? ' selected="selected"' : "";

												echo '<option value="' . $v['id'] . '" ' . $selected . '>' . $v['nama_kelurahan'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('kelurahan'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="komoditi"><span class="text-danger">*</span>Komoditi</label>
										<select name="komoditi" class="form-control select2">
											<?php
											$array_komoditi = [
												"Karet", "Kelapa Sawit", "Kelapa", "Pinang"
											];
											foreach ($array_komoditi as $row) {
												$selected = (set_value('status_rabies') == $value ? 'selected' : null);
												echo "<option $selected>" . $row . "</option>";
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('komoditi'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="tbm" class="col-md-4 control-label"><span class="text-danger">*</span>TBM (Ha)</label>
										<input type="text" name="tbm" value="<?php echo set_value('tbm', 0); ?>" class="form-control" id="tbm" />
										<span class="text-danger"><?php echo form_error('tbm'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="tm" class="col-md-4 control-label"><span class="text-danger">*</span>TM (Ha)</label>
										<input type="text" name="tm" value="<?php echo set_value('tm', 0); ?>" class="form-control" id="tm" />
										<span class="text-danger"><?php echo form_error('tm'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="tt_tr" class="col-md-4 control-label"><span class="text-danger">*</span>TT/TR (Ha)</label>
										<input type="text" name="tt_tr" value="<?php echo set_value('tt_tr', 0); ?>" class="form-control" id="tt_tr" />
										<span class="text-danger"><?php echo form_error('tt_tr'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="produksi" class="col-md-4 control-label"><span class="text-danger">*</span>Produksi (Ton/Thn)</label>
										<input type="text" name="produksi" value="<?php echo set_value('produksi', 0); ?>" class="form-control" id="produksi" />
										<span class="text-danger"><?php echo form_error('produksi'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="produktifitas" class="col-md-4 control-label"><span class="text-danger">*</span>Produktifitas (Ton/Ha/Thn)</label>
										<input type="text" name="produktifitas" value="<?php echo set_value('produktifitas', 0); ?>" class="form-control" id="produktifitas" />
										<span class="text-danger"><?php echo form_error('produktifitas'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="krt" class="col-md-4 control-label"><span class="text-danger">*</span>KRT</label>
										<input type="text" name="krt" value="<?php echo set_value('krt', 0); ?>" class="form-control" id="krt" />
										<span class="text-danger"><?php echo form_error('krt'); ?></span>
									</div>
								</div>

								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="kt" class="col-md-4 control-label"><span class="text-danger">*</span>KT</label>
										<input type="text" name="kt" value="<?php echo set_value('kt', 0); ?>" class="form-control" id="kt" />
										<span class="text-danger"><?php echo form_error('kt'); ?></span>
									</div>
								</div>
							</div>
						</div>
						<div class="card-action">
							<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
						</div>

						<?php echo form_close(); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
