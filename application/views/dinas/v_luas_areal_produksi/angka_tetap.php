<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Luas Areal dan Produksi Tanaman Perkebunan - Angka Tetap</title>
	<!-- Css Styles -->
	<link href="<?= base_url() ?>assets/img/favicon.png" rel="icon">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/elegant-icons.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/nice-select.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery-ui.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.carousel.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/slicknav.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/chatbox.css" type="text/css">
</head>

<body class="text-center">
	<h4>Angka Tetap</h4>
	<h4>Luas Areal dan Produksi Tanaman Perkebunan</h4>
	<h4><?= $kotakabupaten['nama_kotakabupaten']; ?> Tahun <?= $tahun; ?></h4>
	<br>
	<table class="table text-justify">
		<tr>
			<td width="20%">Kota</td>
			<td>: <?= $kotakabupaten['nama_kotakabupaten']; ?></td>
		</tr>
		<tr>
			<td>Komoditi</td>
			<td>: Karet, Kelapa sawit, Kelapa dan Pinang</td>
		</tr>
	</table>
	<table class="display table table-striped table-bordered">
		<thead>
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">Komoditi</th>
				<th colspan="4">Luas Areal (Ha)</th>
				<th colspan="2">Produksi (Ton/Ha/Thn)</th>
				<th rowspan="2">KRT</th>
			</tr>
			<tr>
				<th>TBM</th>
				<th>TM</th>
				<th>TTM/TR</th>
				<th>JUMLAH</th>
				<th>TOTAL</th>
				<th>Rata-rata</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if (count($luas_areal_produksi_angka_tetap) > 0) {
				$no = 1;
				foreach ($luas_areal_produksi_angka_tetap as $k) { ?>
					<tr>
						<td><?php echo $no; ?></td>
						<td><?php echo $k['komoditi']; ?></td>
						<td><?php echo $k['tbm']; ?></td>
						<td><?php echo $k['tm']; ?></td>
						<td><?php echo $k['tt_tr']; ?></td>
						<td><?php echo $k['jumlah']; ?></td>
						<td><?php echo $k['produksi']; ?></td>
						<td><?php echo $k['produktifitas']; ?></td>
						<td><?php echo $k['krt']; ?></td>
					</tr>
			<?php $no++;
				}
			} else {
				echo "<tr><td colspan='9'>Belum ada data</td></tr>";
			} ?>
		</tbody>
	</table>
	<h5 class="text-justify">Keterangan:</h5>
	<table class="table text-justify">
		<tr>
			<td width="20%">TBM</td>
			<td>: Tanaman Belum Menghasilkan</td>
		</tr>
		<tr>
			<td>TM</td>
			<td>: Tanaman Menghasilkan</td>
		</tr>
		<tr>
			<td>TTM/TR</td>
			<td>: Tanaman Tidak Menghasilkan/Tanaman Rusak</td>
		</tr>
		<tr>
			<td>KRT</td>
			<td>: Kepala Rumah Tangga</td>
		</tr>
	</table>
</body>

</html>
