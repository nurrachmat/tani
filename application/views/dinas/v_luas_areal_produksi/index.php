<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Luas Areal Produksi</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Luas Areal Produksi</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<!-- <h4 class="card-title">Add Row</h4> -->

							<a href="<?php echo site_url('dinas/luas_areal_produksi/add'); ?>" class="btn btn-primary btn-round ml-auto">
								<i class="fa fa-plus"></i>
								Tambah
							</a>
						</div>
					</div>
					<div class="card-body">
						<div class="jumbotron py-3">
							<h4 class="border-bottom">Filter Tanggal</h4>
							<?= form_open(''); ?>
							<div class="row">
								<div class="col-md-3 col-lg-3">
									<select name="tahun" class="form-control select2">
										<option value="">-- Pilih Tahun --</option>
										<?php

										for ($i = '2020'; $i <= date('Y'); $i++) {
											$selected = (set_value('tahun') == $i ? 'selected' : null);
											echo "<option value='" . $i . "' $selected >" . $i . '</option>';
										}
										?>
									</select>
								</div>
								<div class="col-md-3 col-lg-3">
									<select name="kotakabupaten" class="form-control select2" required>
										<option value="all">-- Semua kotakabupaten --</option>
										<?php
										foreach ($kotakabupaten as $v) {
											$selected = ($v['id'] == set_value('kotakabupaten')) ? ' selected="selected"' : "";

											echo '<option value="' . $v['id'] . '" ' . $selected . '>' . $v['nama_kotakabupaten'] . '</option>';
										}
										?>
									</select>
								</div>
								<div class="col-md-6 col-lg-6">
									<button type="submit" class="btn btn-primary btn-round"><i class="fa fa-search"></i> Cari</button>
									<?php
									if ($this->input->post('tahun') != null) {
										echo '<a href="' . base_url('dinas/luas_areal_produksi/angka_tetap/') . '' . $this->input->post("tahun") . '/' . $this->input->post("kotakabupaten") . '" target="_blank" class="btn btn-success btn-round"><i class="fa fa-file"></i> Angka Tetap</a>';
									}
									?>

								</div>
							</div>
							<?= form_close(); ?>
						</div>
						<div class="table-responsive">
							<?php
							if (!is_null($this->session->flashdata('m_error'))) { ?>
								<div class="clearfix"></div>
								<div class="alert alert-danger alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<?php echo $this->session->flashdata('m_error'); ?>
								</div>
							<?php } ?>

							<?php if (!is_null($this->session->flashdata('m_success'))) {
								echo "<div class=\"clearfix\"></div>";
								echo alert_success($this->session->flashdata('m_success'), TRUE);
							} ?>

							<table id="basic-datatables" class="display table table-striped table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Tahun</th>
										<th>Kelurahan</th>
										<th>Komoditi</th>
										<th>TBM</th>
										<th>TM</th>
										<th>TT/TR</th>
										<th>Produksi</th>
										<th>Produktifitas</th>
										<th>KRT</th>
										<th>KT</th>
										<th>#</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Tahun</th>
										<th>Kelurahan</th>
										<th>Komoditi</th>
										<th>TBM</th>
										<th>TM</th>
										<th>TT/TR</th>
										<th>Produksi</th>
										<th>Produktifitas</th>
										<th>KRT</th>
										<th>KT</th>
										<th>#</th>
									</tr>
								</tfoot>
								<tbody>
									<?php
									$no = 1;
									foreach ($luas_areal_produksi as $k) { ?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $k['tahun']; ?></td>
											<td><?php echo $k['nama_kelurahan']; ?></td>
											<td><?php echo $k['komoditi']; ?></td>
											<td><?php echo $k['tbm']; ?></td>
											<td><?php echo $k['tm']; ?></td>
											<td><?php echo $k['tt_tr']; ?></td>
											<td><?php echo $k['produksi']; ?></td>
											<td><?php echo $k['produktifitas']; ?></td>
											<td><?php echo $k['krt']; ?></td>
											<td><?php echo $k['kt']; ?></td>
											<td>
												<div class="form-button-action">
													<a href="<?php echo site_url('dinas/luas_areal_produksi/edit/' . $k['id']); ?>">
														<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Ubah">
															<i class="fa fa-edit"></i>
														</button>
													</a>
													<a href="<?php echo site_url('dinas/luas_areal_produksi/remove/' . $k['id']); ?>">
														<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Hapus">
															<i class="fa fa-trash"></i>
														</button>
													</a>
												</div>
											</td>
										</tr>
									<?php $no++;
									} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
