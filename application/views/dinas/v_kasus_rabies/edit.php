<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Kasus Rabies</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Kasus Rabies</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Ubah Kasus Rabies</h4>
						</div>
					</div>
					<?= validation_errors() ?>
					<?php echo form_open_multipart('dinas/kasus_rabies/edit/' . $kasus_rabies['id']); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="tgl"><span class="text-danger">*</span>Tahun</label>
										<select name="tahun" class="form-control">
											<?php
											for ($i = '2020'; $i <= date('Y'); $i++) {
												if (set_value('tahun', $kasus_rabies['tahun']) == $i) {
													echo '<option value="' . $i . '" selected >' . $i . '</option>';
												} else {
													echo '<option value="' . $i . '" >' . $i . '</option>';
												}
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('tgl'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="kecamatan"><span class="text-danger">*</span>Kecamatan</label>
										<select name="kecamatan" class="form-control select2" required>
											<?php
											foreach ($kecamatan as $v) {
												$selected = ($v['id'] == set_value('kecamatan', $kasus_rabies['kecamatan_id'])) ? ' selected="selected"' : "";

												echo '<option value="' . $v['id'] . '" ' . $selected . '>' . $v['nama_kecamatan'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('kecamatan'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="status_rabies" class="col-md-4 control-label"><span class="text-danger">*</span>Status Rabies</label>
										<select name="status_rabies" class="form-control" required>
											<?php
											$sts_rabies = array('Ya', 'Tidak');
											foreach ($sts_rabies as $key => $value) {
												if (set_value('status_rabies', $kasus_rabies['status_rabies']) == $value) {
													echo '<option value="' . $value . '" selected >' . $value . '</option>';
												} else {
													echo '<option value="' . $value . '" >' . $value . '</option>';
												}
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('status_rabies'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="populasi_hpr" class="col-md-4 control-label"><span class="text-danger">*</span>Populasi HPR</label>
										<input type="number" name="populasi_hpr" value="<?php echo set_value('populasi_hpr', $kasus_rabies['populasi_hpr']); ?>" class="form-control" id="populasi_hpr" />
										<span class="text-danger"><?php echo form_error('populasi_hpr'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="real_vaks" class="col-md-4 control-label"><span class="text-danger">*</span>Realisasi Vaksin (Ekor)</label>
										<input type="number" name="real_vaks" value="<?php echo set_value('real_vaks', $kasus_rabies['real_vaks']); ?>" class="form-control" id="real_vaks" />
										<span class="text-danger"><?php echo form_error('real_vaks'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="real_elim" class="col-md-4 control-label"><span class="text-danger">*</span>Realisasi Elim (Ekor)</label>
										<input type="number" name="real_elim" value="<?php echo set_value('real_elim', $kasus_rabies['real_elim']); ?>" class="form-control" id="real_elim" />
										<span class="text-danger"><?php echo form_error('real_elim'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="jml_spec" class="col-md-4 control-label"><span class="text-danger">*</span>Jumlah Spec Rab</label>
										<input type="number" name="jml_spec" value="<?php echo set_value('jml_spec', $kasus_rabies['jml_spec']); ?>" class="form-control" id="jml_spec" />
										<span class="text-danger"><?php echo form_error('jml_spec'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="hsl_periksa" class="col-md-4 control-label"><span class="text-danger">*</span>Hasil Pemeriksaan</label>
										<select name="hsl_periksa" class="form-control" required>
											<?php
											$hsl_periksa = array('Positif', 'Negatif');
											foreach ($hsl_periksa as $key => $value) {
												if (set_value('hsl_periksa', $kasus_rabies['hsl_periksa']) == $value) {
													echo '<option value="' . $value . '" selected >' . $value . '</option>';
												} else {
													echo '<option value="' . $value . '" >' . $value . '</option>';
												}
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('hsl_periksa'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="ghtr" class="col-md-4 control-label"><span class="text-danger">*</span>GHTR</label>
										<input type="number" name="ghtr" value="<?php echo set_value('ghtr', $kasus_rabies['ghtr']); ?>" class="form-control" id="ghtr" />
										<span class="text-danger"><?php echo form_error('ghtr'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="pemberian" class="col-md-4 control-label"><span class="text-danger">*</span>Pemberian</label>
										<select name="pemberian" class="form-control" required>
											<?php
											$pemberian = array('VAR', 'SAR');
											foreach ($pemberian as $key => $value) {
												if (set_value('pemberian', $kasus_rabies['pemberian']) == $value) {
													echo '<option value="' . $value . '" selected >' . $value . '</option>';
												} else {
													echo '<option value="' . $value . '" >' . $value . '</option>';
												}
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('pemberian'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="lyssa" class="col-md-4 control-label"><span class="text-danger">*</span>Lyssa</label>
										<input type="text" name="lyssa" value="<?php echo set_value('lyssa', $kasus_rabies['lyssa']); ?>" class="form-control" id="lyssa" />
										<span class="text-danger"><?php echo form_error('lyssa'); ?></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row clearfix">
							<div class="card-action">
								<button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> Ubah</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
