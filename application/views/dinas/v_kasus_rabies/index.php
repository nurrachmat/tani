<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Kasus Rabies</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Kasus Rabies</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<!-- <h4 class="card-title">Add Row</h4> -->

							<a href="<?php echo site_url('dinas/kasus_rabies/add'); ?>" class="btn btn-primary btn-round ml-auto">
								<i class="fa fa-plus"></i>
								Tambah
							</a>
						</div>
					</div>
					<div class="card-body">
						<div class="jumbotron py-3">
							<h4 class="border-bottom">Filter Tanggal</h4>
							<?= form_open(''); ?>
							<div class="row">
								<div class="col-md-3 col-lg-3">
									<select name="tahun" class="form-control">
										<?php
										for ($i = '2020'; $i <= date('Y'); $i++) {
											echo '<option value="' . $i . '" >' . $i . '</option>';
										}
										?>
									</select>
								</div>
								<div class="col-md-3 col-lg-3">
									<select name="kecamatan" class="form-control select2" required>
										<option value="all">-- Semua Kecamatan --</option>
										<?php
										foreach ($kecamatan as $v) {
											$selected = ($v['id'] == set_value('kecamatan')) ? ' selected="selected"' : "";

											echo '<option value="' . $v['id'] . '" ' . $selected . '>' . $v['nama_kecamatan'] . '</option>';
										}
										?>
									</select>
								</div>
								<div class="col-md-3 col-lg-3">
									<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
									<button type="submit" class="btn btn-success"><i class="fa fa-download"></i> XLS</button>
								</div>
							</div>
							<?= form_close(); ?>
						</div>
						<div class="table-responsive">
							<?php
							if (!is_null($this->session->flashdata('m_error'))) { ?>
								<div class="clearfix"></div>
								<div class="alert alert-danger alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<?php echo $this->session->flashdata('m_error'); ?>
								</div>
							<?php } ?>

							<?php if (!is_null($this->session->flashdata('m_success'))) {
								echo "<div class=\"clearfix\"></div>";
								echo alert_success($this->session->flashdata('m_success'), TRUE);
							} ?>

							<table id="basic-datatables" class="display table table-striped table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Tahun</th>
										<th>Kecamatan</th>
										<th>Status Rabies</th>
										<th>Populasi Hpr</th>
										<th>Real Vaks</th>
										<th>Real Elim</th>
										<th>Jml Spec</th>
										<th>Hsl Periksa</th>
										<th>Ghtr</th>
										<th>Pemberian</th>
										<th>Lyssa</th>
										<th>#</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Tahun</th>
										<th>Kecamatan</th>
										<th>Status Rabies</th>
										<th>Populasi Hpr</th>
										<th>Real Vaks</th>
										<th>Real Elim</th>
										<th>Jml Spec</th>
										<th>Hsl Periksa</th>
										<th>Ghtr</th>
										<th>Pemberian</th>
										<th>Lyssa</th>
										<th>#</th>
									</tr>
								</tfoot>
								<tbody>
									<?php
									$no = 1;
									foreach ($kasus_rabies as $k) { ?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $k['tahun']; ?></td>
											<td><?php echo $k['nama_kecamatan']; ?></td>
											<td><?php echo $k['status_rabies']; ?></td>
											<td><?php echo $k['populasi_hpr']; ?></td>
											<td><?php echo $k['real_vaks']; ?></td>
											<td><?php echo $k['real_elim']; ?></td>
											<td><?php echo $k['jml_spec']; ?></td>
											<td><?php echo $k['hsl_periksa']; ?></td>
											<td><?php echo $k['ghtr']; ?></td>
											<td><?php echo $k['pemberian']; ?></td>
											<td><?php echo $k['lyssa']; ?></td>
											<td>
												<div class="form-button-action">
													<a href="<?php echo site_url('dinas/kasus_rabies/edit/' . $k['id']); ?>">
														<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Ubah">
															<i class="fa fa-edit"></i>
														</button>
													</a>
													<a href="<?php echo site_url('dinas/kasus_rabies/remove/' . $k['id']); ?>">
														<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Hapus">
															<i class="fa fa-trash"></i>
														</button>
													</a>
												</div>
											</td>
										</tr>
									<?php $no++;
									} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
