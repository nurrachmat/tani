<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Users</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="<?php echo site_url('dinas/dashboard'); ?>">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Users</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Users</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <!-- <h4 class="card-title">Add Row</h4> -->

                            <a href="<?php echo site_url('dinas/users/add'); ?>" class="btn btn-primary btn-round ml-auto">
                                <i class="fa fa-plus"></i>
                                Tambah
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            if (!is_null($this->session->flashdata('m_error'))) { ?>
                                <div class="clearfix"></div>
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <?php echo $this->session->flashdata('m_error'); ?>
                                </div>
                            <?php } ?>

                            <?php if (!is_null($this->session->flashdata('m_success'))) {
                                echo "<div class=\"clearfix\"></div>";
                                echo alert_success($this->session->flashdata('m_success'), TRUE);
                            } ?>
                            <table id="basic-datatables" class="display table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Hp</th>
                                        <th>Kategori User</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Hp</th>
                                        <th>Kategori User</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($users as $k) { ?>
                                        <tr>
                                            <td><?php echo $k['id']; ?></td>
                                            <td><?php echo $k['nama']; ?></td>
                                            <td><?php echo $k['email']; ?></td>
                                            <td><?php echo $k['hp']; ?></td>
                                            <td><?php echo $k['nama_kategori_user']; ?></td>
                                            <td><?php echo status_aktif($k['status']); ?></td>
                                            <td>
                                                <div class="form-button-action">
                                                    <a href="<?php echo site_url('dinas/users/edit/' . $k['id']); ?>">
                                                        <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Ubah">
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                    </a>
                                                    <?php
                                                    if ($k['status'] == 'Y') {
                                                    ?>
                                                        <a href="<?php echo site_url('dinas/users/softdelete/' . $k['id'] . '/N'); ?>">
                                                            <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger btn-lg" data-original-title="Nonaktifkan">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </a>
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <a href="<?php echo site_url('dinas/users/softdelete/' . $k['id'] . '/Y'); ?>">
                                                            <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-success btn-lg" data-original-title="Aktifkan">
                                                                <i class="fas fa-undo"></i>
                                                            </button>
                                                        </a>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>