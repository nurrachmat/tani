<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-3">

				<!-- Profile Image -->
				<div class="box box-primary">
					<div class="box-body box-profile">
						<img class="profile-user-img img-responsive img-circle" src="<?php echo ($users['foto'] ? base_url('resources/img/' . $users['foto']) : base_url('resources/img/user2-160x160.jpg')); ?>" alt="User profile picture">

						<h3 class="profile-username text-center"><?php echo $users['nama']; ?></h3>

						<p class="text-muted text-center"><?php echo level($users['level']); ?> <?php echo ($users['level'] >= 4 ? status_alumni($users_detail['status_alumni_id']) : null); ?></p>

						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<i class="fa fa-inbox margin-r-5"></i> <?php echo $users['email']; ?>
							</li>
							<li class="list-group-item">
								<i class="fa fa-phone margin-r-5"></i> <?php echo $users['hp']; ?>
							</li>
							<li class="list-group-item">
								<i class="fa fa-map margin-r-5"></i> <?php echo $users['alamat']; ?>
							</li>
							<li class="list-group-item">
								<i class="fa fa-calendar margin-r-5"></i> <?php echo konversi_tanggal($users['created_at']); ?>
							</li>
							<?php if ($users['level'] == 2) { ?>
								<li class="list-group-item">
									<i class="fa fa-map margin-r-5"></i> <?php echo $users_dinas['nama_kotakabupaten']; ?>
								</li>
							<?php } ?>
						</ul>

						<!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->


			</div>
			<!-- /.col -->

			<?php if ($users['level'] >= 4) { ?>
				<div class="col-md-9">
					<!-- About Me Box -->
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Sekolah</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="col-lg-4">
									<strong><i class="fa fa-building margin-r-5"></i> Sekolah</strong>
									<p class="text-muted"><?php echo $users_detail['nama_sekolah']; ?></p>
									<hr>
								</div>
								<div class="col-lg-4">
									<strong><i class="fa fa-book margin-r-5"></i> Jurusan</strong>
									<p class="text-muted"><?php echo $users_detail['nama_jurusan']; ?></p>
									<hr>
								</div>
								<div class="col-lg-4">
									<strong><i class="fa fa-map margin-r-5"></i> Kota/Kabupaten</strong>
									<p class="text-muted"><?php echo $users_detail['nama_kotakabupaten']; ?></p>
									<hr>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4">
									<strong><i class="fa fa-credit-card margin-r-5"></i> NISN</strong>
									<p class="text-muted"><?php echo $users_detail['nisn']; ?></p>
									<hr>
								</div>
								<div class="col-lg-8">
									<strong><i class="fa fa-calendar margin-r-5"></i> Periode</strong>
									<p><?php echo $users_detail['tahun_masuk'] . " s.d " . $users_detail['tahun_lulus']; ?></p>
								</div>
							</div>

						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<?php if ($users_detail['status_alumni_id'] == 'B') { ?>
								<li class="active"><a href="#bekerja" data-toggle="tab">Bekerja</a></li>
							<?php } ?>
							<?php if ($users_detail['status_alumni_id'] == 'C') { ?>
								<li class="active"><a href="#belum-cari-pekerjaan" data-toggle="tab">Belum/Cari Pekerjaan</a></li>
							<?php } ?>
							<?php if ($users_detail['status_alumni_id'] == 'K') { ?>
								<li class="active"><a href="#kuliah" data-toggle="tab">Kuliah</a></li>
							<?php } ?>
							<?php if ($users_detail['status_alumni_id'] == 'L') { ?>
								<li class="active"><a href="#lulus-kuliah" data-toggle="tab">Lulus Kuliah</a></li>
							<?php } ?>
							<?php if ($users_detail['status_alumni_id'] == 'W') { ?>
								<li class="active"><a href="#wirausaha" data-toggle="tab">Wirausaha</a></li>
							<?php } ?>
						</ul>
						<div class="tab-content">
							<!-- active  -->
							<?php if ($users_detail['status_alumni_id'] == 'B') { ?>
								<div class="tab-pane active" id="bekerja">
									<div class="box-body">
										<strong><i class="fa fa-building margin-r-5"></i> Perusahaan</strong>
										<p class="text-muted"><?php echo $users_bekerja['nama_tempat_kerja']; ?></p>
										<hr>

										<strong><i class="fa fa-map margin-r-5"></i> Alamat</strong>
										<p class="text-muted"><?php echo $users_bekerja['alamat_perusahaan']; ?></p>
										<hr>

										<strong><i class="fa fa-calendar margin-r-5"></i> Kota/Kabupaten</strong>
										<p><?php echo $users_bekerja['kota']; ?></p>

									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.tab-pane -->
							<?php } ?>
							<?php if ($users_detail['status_alumni_id'] == 'C') { ?>
								<div class="tab-pane  active" id="belum-cari-pekerjaan">
									<div class="box-body">
										<strong><i class="fa fa-building margin-r-5"></i> Alasan belum/sedang mencari pekerjaan</strong>
										<p class="text-muted"><?php echo $users_belum_bekerja['alasan']; ?></p>
										<hr>

									</div>
								</div>
								<!-- /.tab-pane -->
							<?php } ?>
							<?php if ($users_detail['status_alumni_id'] == 'K') { ?>
								<div class="tab-pane active" id="kuliah">
									<div class="box-body">
										<strong><i class="fa fa-building margin-r-5"></i> Nama Perguruan Tinggi</strong>
										<p class="text-muted"><?php echo $users_kuliah['nama_pt']; ?></p>
										<hr>

										<strong><i class="fa fa-book margin-r-5"></i> Jurusan/Program Studi</strong>
										<p class="text-muted"><?php echo $users_kuliah['jurusan']; ?></p>
										<hr>

										<strong><i class="fa fa-map margin-r-5"></i> Alamat</strong>
										<p class="text-muted"><?php echo $users_kuliah['alamat']; ?></p>
										<hr>

										<strong><i class="fa fa-calendar margin-r-5"></i> Periode</strong>
										<p class="text-muted"><?php echo $users_kuliah['tahun_masuk'] . " s.d " . $users_kuliah['tahun_lulus']; ?></p>
										<hr>

									</div>
								</div>
								<!-- /.tab-pane -->
							<?php } ?>
							<?php if ($users_detail['status_alumni_id'] == 'L') { ?>
								<div class="tab-pane active" id="lulus-kuliah">
									<div class="box-body">
										<strong><i class="fa fa-building margin-r-5"></i> Nama Perguruan Tinggi</strong>
										<p class="text-muted"><?php echo $users_kuliah['nama_pt']; ?></p>
										<hr>

										<strong><i class="fa fa-book margin-r-5"></i> Jurusan/Program Studi</strong>
										<p class="text-muted"><?php echo $users_kuliah['jurusan']; ?></p>
										<hr>

										<strong><i class="fa fa-map margin-r-5"></i> Alamat</strong>
										<p class="text-muted"><?php echo $users_kuliah['alamat']; ?></p>
										<hr>

										<strong><i class="fa fa-calendar margin-r-5"></i> Periode</strong>
										<p class="text-muted"><?php echo $users_kuliah['tahun_masuk'] . " s.d " . $users_kuliah['tahun_lulus']; ?></p>
										<hr>

										<?php if (isset($users_bekerja)) { ?>
											<strong><i class="fa fa-building margin-r-5"></i> Perusahaan</strong>
											<p class="text-muted"><?php echo $users_bekerja['nama_tempat_kerja']; ?></p>
											<hr>

											<strong><i class="fa fa-map margin-r-5"></i> Alamat</strong>
											<p class="text-muted"><?php echo $users_bekerja['alamat_perusahaan']; ?></p>
											<hr>

											<strong><i class="fa fa-calendar margin-r-5"></i> Kota/Kabupaten</strong>
											<p><?php echo $users_bekerja['kota']; ?></p>
										<?php } ?>

										<?php if (isset($users_belum_bekerja)) { ?>
											<strong><i class="fa fa-building margin-r-5"></i> Alasan belum/sedang mencari pekerjaan</strong>
											<p class="text-muted"><?php echo $users_belum_bekerja['alasan']; ?></p>
											<hr>
										<?php } ?>

									</div>
								</div>
								<!-- /.tab-pane -->
							<?php } ?>
							<?php if ($users_detail['status_alumni_id'] == 'W') { ?>
								<div class="tab-pane active" id="wirausaha">
									<div class="box-body">
										<strong><i class="fa fa-building margin-r-5"></i> Nama dan Jenis Usaha</strong>
										<p class="text-muted"><?php echo $users_wirausaha['nama_usaha'] . ", " . $users_wirausaha['jenis_usaha']; ?></p>
										<hr>

										<strong><i class="fa fa-users margin-r-5"></i> Jumlah Karyawan</strong>
										<p class="text-muted"><?php echo $users_wirausaha['jumlah_karyawan']; ?></p>
										<hr>

										<strong><i class="fa fa-money margin-r-5"></i> Omzet</strong>
										<p class="text-muted"><?php echo $users_wirausaha['omzet']; ?></p>
										<hr>


									</div>
								</div>
								<!-- /.tab-pane -->
							<?php } ?>
						</div>
						<!-- /.tab-content -->
					</div>
					<!-- /.nav-tabs-custom -->
				</div>
			<?php } ?>
			<!-- /.col -->
		</div>
		<!-- /.row -->

	</div>
</div>