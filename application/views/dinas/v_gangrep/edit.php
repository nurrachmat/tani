<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Gangrep</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Gangrep</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Ubah Gangrep</h4>
						</div>
					</div>
					<?= validation_errors() ?>
					<?php echo form_open_multipart('dinas/gangrep/edit/' . $gangrep['id']); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="tgl"><span class="text-danger">*</span>Tanggal</label>
										<input type="date" name="tgl" value="<?php echo set_value('tgl', $gangrep['tgl']); ?>" class="form-control" id="tgl" required />
										<span class="text-danger"><?php echo form_error('tgl'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="kecamatan"><span class="text-danger">*</span>Kecamatan</label>
										<select name="kecamatan" class="form-control select2" required>
											<?php
											foreach ($kecamatan as $v) {
												$selected = ($v['id'] == set_value('kecamatan', $gangrep['kecamatan_id'])) ? ' selected="selected"' : "";

												echo '<option value="' . $v['id'] . '" ' . $selected . '>' . $v['nama_kecamatan'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('kecamatan'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="diagnosa"><span class="text-danger">*</span>Diagnosa</label>
										<input type="text" name="diagnosa" value="<?php echo set_value('diagnosa', $gangrep['diagnosa']); ?>" class="form-control" id="diagnosa" required />
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="pemilik"><span class="text-danger">*</span>Pemilik</label>
										<input type="text" name="pemilik" value="<?php echo set_value('pemilik', $gangrep['pemilik']); ?>" class="form-control" id="pemilik" required />
										<span class="text-danger"><?php echo form_error('pemilik'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="spesies"><span class="text-danger">*</span>Spesies</label>
										<input type="text" name="spesies" value="<?php echo set_value('spesies', $gangrep['spesies']); ?>" class="form-control" id="spesies" required />
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="tgl_obat">Tanggal Obat</label>
										<input type="date" name="tgl_obat" value="<?php echo set_value('tgl_obat', $gangrep['tgl_obat']); ?>" class="form-control" id="tgl_obat" />
										<span class="text-danger"><?php echo form_error('tgl_obat'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="obat">Obat</label>
										<input type="text" name="obat" value="<?php echo set_value('obat', $gangrep['obat']); ?>" class="form-control" id="obat" />
									</div>
								</div>
							</div>

						</div>
						<div class="row clearfix">
							<div class="card-action">
								<button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> Ubah</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
