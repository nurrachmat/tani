<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Users</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Users</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('dinas/users/index'); ?>">User</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Tambah Data</h4>
						</div>
					</div>
					<?php echo form_open_multipart('dinas/users/add'); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="kategori_user_id"><span class="text-danger">*</span>Kategori</label>
										<select name="kategori_user_id" class="form-control select2">
											<option value="">Pilih kategori user</option>
											<?php
											foreach ($all_kategori_user as $kategori_user) {
												$selected = ($kategori_user['id'] == $this->input->post('kategori_user_id')) ? ' selected="selected"' : "";

												echo '<option value="' . $kategori_user['id'] . '" ' . $selected . '>' . $kategori_user['nama_level'] . ' - ' . $kategori_user['nama_kategori_user'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('kategori_user_id'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="nama"><span class="text-danger">*</span>Nama</label>
										<input type="text" name="nama" value="<?php echo $this->input->post('nama'); ?>" class="form-control" id="nama" />
										<span class="text-danger"><?php echo form_error('nama'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="email"><span class="text-danger">*</span>Email</label>
										<input type="text" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email" />
										<span class="text-danger"><?php echo form_error('email'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="hp"><span class="text-danger">*</span>Hp</label>
										<input type="text" name="hp" value="<?php echo $this->input->post('hp'); ?>" class="form-control" id="hp" />
										<span class="text-danger"><?php echo form_error('hp'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="foto"><span class="text-danger"></span>Foto</label>
										<input type="file" name="foto" class="form-control" value="<?php echo $this->input->post('foto'); ?>" id="foto" />
										<span class="text-danger"><?php echo form_error('foto'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="password"><span class="text-danger">*</span>Password</label>
										<input type="password" name="password" value="<?php echo $this->input->post('password'); ?>" class="form-control" id="password" />
										<span class="text-danger"><?php echo form_error('password'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="deskripsi"><span class="text-danger"></span>Deskripsi (Host)</label>
										<input type="text" name="deskripsi" value="<?php echo $this->input->post('deskripsi'); ?>" class="form-control" id="deskripsi" />
										<span class="text-danger"><?php echo form_error('deskripsi'); ?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-action">
						<button class="btn btn-success"><i class="fa fa-check"></i> Save</button>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>