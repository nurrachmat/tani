<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Kategori</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Lokasi</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('dinas/kategori/index'); ?>">Kategori</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Tambah Data</h4>
						</div>
					</div>
					<?php echo form_open_multipart('dinas/kategori/add'); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="parent"><span class="text-danger"></span>Parent</label>
										<select name="parent" class="form-control select2">
											<option value="">Pilih parent</option>
											<?php
											foreach ($all_kategori as $kategori) {
												$selected = ($kategori['id'] == $this->input->post('parent')) ? ' selected="selected"' : "";

												echo '<option value="' . $kategori['id'] . '" ' . $selected . '>' . $kategori['nama_kategori'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('parent'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="nama_kategori"><span class="text-danger">*</span>Nama Kategori</label>
										<input type="text" class="form-control" id="nama_kategori" name="nama_kategori" placeholder="Isi nama kategori" value="<?php echo $this->input->post('nama_kategori'); ?>">
										<span class="text-danger"><?php echo form_error('nama_kategori'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="deskripsi"><span class="text-danger">*</span>Deskripsi</label>
										<input type="text" class="form-control" id="deskripsi" name="deskripsi" placeholder="Isi deskripsi kategori" value="<?php echo $this->input->post('deskripsi'); ?>">
										<span class="text-danger"><?php echo form_error('deskripsi'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="foto"><span class="text-danger"></span>Foto</label>
										<input type="file" name="foto" class="form-control" value="<?php echo $this->input->post('foto'); ?>" id="foto" />
										<span class="text-danger"><?php echo form_error('foto'); ?></span>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="card-action">
						<button class="btn btn-success"><i class="fa fa-check"></i> Save</button>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>