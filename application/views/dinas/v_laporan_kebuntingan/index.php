<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Laporan Kebuntungan</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Laporan Kebuntungan</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<!-- <h4 class="card-title">Add Row</h4> -->

							<a href="<?php echo site_url('dinas/laporan_kebuntingan/add'); ?>" class="btn btn-primary btn-round ml-auto">
								<i class="fa fa-plus"></i>
								Tambah
							</a>
						</div>
					</div>
					<div class="card-body">

						<div class="table-responsive">
							<?php
							if (!is_null($this->session->flashdata('m_error'))) { ?>
								<div class="clearfix"></div>
								<div class="alert alert-danger alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<?php echo $this->session->flashdata('m_error'); ?>
								</div>
							<?php } ?>

							<?php if (!is_null($this->session->flashdata('m_success'))) {
								echo "<div class=\"clearfix\"></div>";
								echo alert_success($this->session->flashdata('m_success'), TRUE);
							} ?>

							<table id="basic-datatables" class="display table table-striped table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Tgl PKB</th>
										<th>ID Peternak</th>
										<th>Nama Peternak</th>
										<th>Identifikasi Gewan</th>
										<th>Spesies</th>
										<th>Umur Kebunting</th>
										<th>Telp Petugas</th>
										<th>Ket</th>
										<th>#</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Tgl PKB</th>
										<th>ID Peternak</th>
										<th>Nama Peternak</th>
										<th>Identifikasi Gewan</th>
										<th>Spesies</th>
										<th>Umur Kebunting</th>
										<th>Telp Petugas</th>
										<th>Ket</th>
										<th>#</th>
									</tr>
								</tfoot>
								<tbody>
									<?php
									$no = 1;
									foreach ($laporan_kebuntingan as $l) { ?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $l['tglpkb']; ?></td>
											<td><?php echo $l['idpeternak']; ?></td>
											<td><?php echo $l['nmpeternak']; ?></td>
											<td><?php echo $l['idenhewan']; ?></td>
											<td><?php echo $l['spesies']; ?></td>
											<td><?php echo $l['umurbunting']; ?></td>
											<td><?php echo $l['telppetugas']; ?></td>
											<td><?php echo $l['ket']; ?></td>
											<td>
												<div class="form-button-action">
													<a href="<?php echo site_url('dinas/laporan_kebuntingan/edit/' . $l['id']); ?>">
														<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Ubah">
															<i class="fa fa-edit"></i>
														</button>
													</a>
													<a href="<?php echo site_url('dinas/laporan_kebuntingan/remove/' . $l['id']); ?>">
														<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Hapus">
															<i class="fa fa-trash"></i>
														</button>
													</a>
												</div>
											</td>
										</tr>
									<?php $no++;
									} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
