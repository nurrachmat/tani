<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Laporan Kebuntingan</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Laporan Kebuntingan</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Tambah Laporan Kebuntingan</h4>
						</div>
					</div>
					<?= validation_errors() ?>
					<?php echo form_open_multipart('dinas/laporan_kebuntingan/add'); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="tglpkb" class="col-md-6 control-label"><span class="text-danger">*</span>Tanggal PKB</label>
										<input type="date" name="tglpkb" value="<?php echo set_value('tglpkb', date('Y-m-d')); ?>" class="form-control" id="tglpkb" required />
										<span class="text-danger"><?php echo form_error('tglpkb'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="idpeternak" class="col-md-6 control-label"><span class="text-danger">*</span>ID Peternak</label>
										<input type="text" name="idpeternak" value="<?php echo set_value('idpeternak'); ?>" class="form-control" id="idpeternak" required />
										<span class="text-danger"><?php echo form_error('idpeternak'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="nmpeternak" class="col-md-6 control-label"><span class="text-danger">*</span>Nama Peternak</label>
										<input type="text" name="nmpeternak" value="<?php echo set_value('nmpeternak'); ?>" class="form-control" id="nmpeternak" required />
										<span class="text-danger"><?php echo form_error('idpeternak'); ?></span>
									</div>
								</div>

								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="idenhewan" class="col-md-6 control-label"><span class="text-danger">*</span>Identifikasi Hewan</label>
										<input type="text" name="idenhewan" value="<?php echo set_value('idenhewan'); ?>" class="form-control" id="idenhewan" required />
										<span class="text-danger"><?php echo form_error('idenhewan'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="spesies" class="col-md-6 control-label"><span class="text-danger">*</span>spesies</label>
										<input type="text" name="spesies" value="<?php echo set_value('spesies'); ?>" class="form-control" id="spesies" required />
										<span class="text-danger"><?php echo form_error('spesies'); ?></span>
									</div>
								</div>

								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="umurbunting" class="col-md-6 control-label"><span class="text-danger">*</span>Umur Kebuntingan</label>
										<input type="number" name="umurbunting" value="<?php echo set_value('umurbunting'); ?>" class="form-control" id="umurbunting" required />
										<span class="text-danger"><?php echo form_error('umurbunting'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="telppetugas" class="col-md-6 control-label"><span class="text-danger">*</span>Telp Petugas</label>
										<input type="number" name="telppetugas" value="<?php echo set_value('telppetugas'); ?>" class="form-control" id="telppetugas" required />
										<span class="text-danger"><?php echo form_error('telppetugas'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="ket" class="col-md-3 control-label"><span class="text-danger">*</span>Keterangan</label>
										<input type="text" name="ket" value="<?php echo set_value('ket'); ?>" class="form-control" id="ket" required />
										<span class="text-danger"><?php echo form_error('ket'); ?></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row clearfix">
							<div class="card-action">
								<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
