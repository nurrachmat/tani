<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Kota/Kabupaten</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Lokasi</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('dinas/kotakabupaten/index'); ?>">Kota/Kabupaten</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Ubah Data</h4>
						</div>
					</div>
					<?php echo form_open('dinas/kotakabupaten/edit/' . $kotakabupaten['id']); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="provinsi_id"><span class="text-danger">*</span>Nama Provinsi</label>
										<select name="provinsi_id" class="form-control select2">
											<option value="">Pilih nama provinsi</option>
											<?php
											foreach ($all_provinsi as $provinsi) {
												$selected = ($provinsi['id'] == $kotakabupaten['provinsi_id']) ? ' selected="selected"' : "";

												echo '<option value="' . $provinsi['id'] . '" ' . $selected . '>' . $provinsi['nama_provinsi'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('provinsi_id'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="nama_kotakabupaten"><span class="text-danger">*</span>Nama Kota/Kabupaten</label>
										<input type="text" class="form-control" id="nama_kotakabupaten" name="nama_kotakabupaten" placeholder="Isi nama kota/kabupaten" value="<?php echo ($this->input->post('nama_kotakabupaten') ? $this->input->post('nama_kotakabupaten') : $kotakabupaten['nama_kotakabupaten']); ?>">
										<span class="text-danger"><?php echo form_error('nama_kotakabupaten'); ?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-action">
						<button class="btn btn-success"><i class="fa fa-check"></i> Save</button>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>