<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Kategori User</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Lokasi</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('dinas/kategori_user/index'); ?>">Kategori user</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Tambah Data</h4>
						</div>
					</div>
					<?php echo form_open('dinas/kategori_user/add'); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="level_user_id"><span class="text-danger">*</span>Nama Level</label>
										<select name="level_user_id" class="form-control select2">
											<option value="">Pilih nama level user</option>
											<?php
											foreach ($all_level_user as $level_user) {
												$selected = ($level_user['id'] == $this->input->post('level_user_id')) ? ' selected="selected"' : "";

												echo '<option value="' . $level_user['id'] . '" ' . $selected . '>' . $level_user['nama_level'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('level_user_id'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="nama_kategori_user"><span class="text-danger">*</span>Nama Kota/Kabupaten</label>
										<input type="text" class="form-control" id="nama_kategori_user" name="nama_kategori_user" placeholder="Isi nama kategori user" value="<?php echo $this->input->post('nama_kategori_user'); ?>">
										<span class="text-danger"><?php echo form_error('nama_kategori_user'); ?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-action">
						<button class="btn btn-success"><i class="fa fa-check"></i> Save</button>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>