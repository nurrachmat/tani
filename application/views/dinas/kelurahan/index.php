<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Kelurahan</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Lokasi</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Kelurahan</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<!-- <h4 class="card-title">Add Row</h4> -->

							<a href="<?php echo site_url('dinas/kelurahan/add'); ?>" class="btn btn-primary btn-round ml-auto">
								<i class="fa fa-plus"></i>
								Tambah
							</a>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<?php
							if (!is_null($this->session->flashdata('m_error'))) { ?>
								<div class="clearfix"></div>
								<div class="alert alert-danger alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<?php echo $this->session->flashdata('m_error'); ?>
								</div>
							<?php } ?>

							<?php if (!is_null($this->session->flashdata('m_success'))) {
								echo "<div class=\"clearfix\"></div>";
								echo alert_success($this->session->flashdata('m_success'), TRUE);
							} ?>
							<table id="basic-datatables" class="display table table-striped table-hover">
								<thead>
									<tr>
										<th>ID</th>
										<th>Nama Kelurahan</th>
										<th>Nama Kecamatan</th>
										<th>Nama Kota/Kabupaten</th>
										<th>Nama Provinsi</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>ID</th>
										<th>Nama Kelurahan</th>
										<th>Nama Kecamatan</th>
										<th>Nama Kota/Kabupaten</th>
										<th>Nama Provinsi</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
								<tbody>
									<?php foreach ($kelurahan as $k) { ?>
										<tr>
											<td><?php echo $k['id']; ?></td>
											<td><?php echo $k['nama_kelurahan']; ?></td>
											<td><?php echo $k['nama_kecamatan']; ?></td>
											<td><?php echo $k['nama_kotakabupaten']; ?></td>
											<td><?php echo $k['nama_provinsi']; ?></td>
											<td>
												<div class="form-button-action">
													<a href="<?php echo site_url('dinas/kelurahan/edit/' . $k['id']); ?>">
														<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Ubah">
															<i class="fa fa-edit"></i>
														</button>
													</a>
													<a href="<?php echo site_url('dinas/kelurahan/remove/' . $k['id']); ?>">
														<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Hapus">
															<i class="fa fa-times"></i>
														</button>
													</a>
												</div>
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
