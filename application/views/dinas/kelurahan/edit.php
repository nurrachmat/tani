<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Kelurahan</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Lokasi</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="<?php echo site_url('dinas/kelurahan/index'); ?>">Kelurahan</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Ubah Data</h4>
						</div>
					</div>
					<?php echo form_open('dinas/kelurahan/edit/' . $kelurahan['id']); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="kecamatan_id"><span class="text-danger">*</span>Nama Kecamatan</label>
										<select name="kecamatan_id" class="form-control select2">
											<option value="">Pilih nama kecamatan</option>
											<?php
											foreach ($all_kecamatan as $kecamatan) {
												$selected = ($kecamatan['id'] == $kelurahan['kecamatan_id']) ? ' selected="selected"' : "";

												echo '<option value="' . $kecamatan['id'] . '" ' . $selected . '>' . $kecamatan['nama_kotakabupaten'] . ' - ' . $kecamatan['nama_kecamatan'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('kecamatan_id'); ?></span>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="nama_kelurahan"><span class="text-danger">*</span>Nama Kelurahan</label>
										<input type="text" class="form-control" id="nama_kelurahan" name="nama_kelurahan" placeholder="Isi nama kecamatan" value="<?php echo ($this->input->post('nama_kelurahan') ? $this->input->post('nama_kelurahan') : $kelurahan['nama_kelurahan']); ?>">
										<span class="text-danger"><?php echo form_error('nama_kelurahan'); ?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-action">
						<button class="btn btn-success"><i class="fa fa-check"></i> Save</button>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
