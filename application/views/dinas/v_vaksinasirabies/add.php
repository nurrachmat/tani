<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Vaksinasi Rabies</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Vaksinasi Rabies</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Tambah Vaksinasi Rabies</h4>
						</div>
					</div>
					<?= validation_errors() ?>
					<?php echo form_open_multipart('dinas/vaksinasirabies/add'); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="tgl"><span class="text-danger">*</span>Tanggal</label>
										<input type="date" name="tgl" value="<?php echo set_value('tgl', date('Y-m-d')); ?>" class="form-control" id="tgl" required />
										<span class="text-danger"><?php echo form_error('tgl'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="kecamatan"><span class="text-danger">*</span>Kecamatan</label>
										<select name="kecamatan" class="form-control select2" required>
											<?php
											foreach ($kecamatan as $v) {
												$selected = ($v['id'] == set_value('kecamatan')) ? ' selected="selected"' : "";

												echo '<option value="' . $v['id'] . '" ' . $selected . '>' . $v['nama_kecamatan'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('kecamatan'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="pemilik"><span class="text-danger">*</span>Pemilik</label>
										<input type="text" name="pemilik" value="<?php echo set_value('pemilik'); ?>" class="form-control" id="pemilik" required />
										<span class="text-danger"><?php echo form_error('pemilik'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="no_batch"><span class="text-danger">*</span>No Batch</label>
										<input type="number" name="no_batch" value="<?php echo set_value('no_batch'); ?>" class="form-control" id="no_batch" required />
										<span class="text-danger"><?php echo form_error('no_batch'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="jenis_hpr"><span class="text-danger">*</span>Jenis HPR</label>
										<select class="form-control" name="jenis_hpr" required>
											<?php
											$jenis = array('Anjing', 'Kucing', 'Kera');
											foreach ($jenis as $key => $value) {
												echo '<option value="' . $value . '" >' . $value . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('jenis_hpr'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="jk"><span class="text-danger">*</span>Jenis Kelaamin</label>
										<select class="form-control" name="jk" required>
											<?php
											$jenis = array('Jantan', 'Betina');
											foreach ($jenis as $key => $value) {
												echo '<option value="' . $value . '" >' . $value . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('jk'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="jumlah"><span class="text-danger">*</span>Jumlah</label>
										<input type="number" name="jumlah" value="<?php echo set_value('jumlah'); ?>" class="form-control" id="jumlah" required />
										<span class="text-danger"><?php echo form_error('jumlah'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-12 col-lg-12">
									<div class="form-group">
										<label for="ket">Keterangan</label>
										<textarea class="form-control" name="ket"><?= set_value('ket') ?></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="row clearfix">
							<div class="card-action">
								<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
