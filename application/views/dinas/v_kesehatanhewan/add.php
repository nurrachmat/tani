<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Kesehatan Hewan</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Kesehatan Hewan</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Tambah Pelayanan Hewan</h4>
						</div>
					</div>
					<?= validation_errors() ?>
					<?php echo form_open_multipart('dinas/kesehatanhewan/add'); ?>
					<div class="card-body">
						<div class="box-body">
							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="tgl"><span class="text-danger">*</span>Tanggal</label>
										<input type="date" name="tgl" value="<?php echo set_value('tgl', date('Y-m-d')); ?>" class="form-control" id="tgl" required />
										<span class="text-danger"><?php echo form_error('tgl'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="kecamatan"><span class="text-danger">*</span>Kecamatan</label>
										<select name="kecamatan" class="form-control select2" required>
											<?php
											foreach ($kecamatan as $v) {
												$selected = ($v['id'] == set_value('kecamatan')) ? ' selected="selected"' : "";

												echo '<option value="' . $v['id'] . '" ' . $selected . '>' . $v['nama_kecamatan'] . '</option>';
											}
											?>
										</select>
										<span class="text-danger"><?php echo form_error('kecamatan'); ?></span>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="pemilik"><span class="text-danger">*</span>Pemilik</label>
										<input type="text" name="pemilik" value="<?php echo set_value('pemilik'); ?>" class="form-control" id="pemilik" required />
										<span class="text-danger"><?php echo form_error('pemilik'); ?></span>
									</div>
								</div>
								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="jenis"><span class="text-danger">*</span>Jenis Hewan</label>
										<input type="text" name="jenis" value="<?php echo set_value('jenis'); ?>" class="form-control" id="jenis" required />
									</div>
								</div>

								<div class="col-md-4 col-lg-4">
									<div class="form-group">
										<label for="jml_hewan"><span class="text-danger">*</span>Jumlah Hewan</label>
										<input type="text" name="jml_hewan" value="<?php echo set_value('jml_hewan'); ?>" class="form-control" id="jml_hewan" required />
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="gejala">Gejala Klinis</label>
										<input type="text" name="gejala" value="<?php echo set_value('gejala'); ?>" class="form-control" id="gejala" />
										<span class="text-danger"><?php echo form_error('gejala'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="diagnosa">Diagnosa</label>
										<input type="text" name="diagnosa" value="<?php echo set_value('diagnosa'); ?>" class="form-control" id="diagnosa" />
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="pengobatan">Penanganan/Pengobatan</label>
										<input type="text" name="pengobatan" value="<?php echo set_value('pengobatan'); ?>" class="form-control" id="pengobatan" />
										<span class="text-danger"><?php echo form_error('pengobatan'); ?></span>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label for="ket">Keterangan (Jumlah Tindakan)</label>
										<input type="text" name="ket" value="<?php echo set_value('ket'); ?>" class="form-control" id="ket" />
									</div>
								</div>
							</div>

						</div>
						<div class="row clearfix">
							<div class="card-action">
								<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
