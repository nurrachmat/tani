<div class="content">
	<div class="page-inner">
		<div class="page-header">
			<h4 class="page-title">Kesehatan Hewan</h4>
			<ul class="breadcrumbs">
				<li class="nav-home">
					<a href="<?php echo site_url('dinas/dashboard'); ?>">
						<i class="flaticon-home"></i>
					</a>
				</li>
				<li class="separator">
					<i class="flaticon-right-arrow"></i>
				</li>
				<li class="nav-item">
					<a href="#">Kesehatan Hewan</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<!-- <h4 class="card-title">Add Row</h4> -->

							<a href="<?php echo site_url('dinas/kesehatanhewan/add'); ?>" class="btn btn-primary btn-round ml-auto">
								<i class="fa fa-plus"></i>
								Tambah
							</a>
						</div>
					</div>
					<div class="card-body">
						<div class="jumbotron py-3">
							<h4 class="border-bottom">Filter Tanggal</h4>
							<?= form_open(''); ?>
							<div class="row">
								<div class="col-md-3 col-lg-3">
									<input type="date" name="tgl1" id="tgl1" class="form-control" value="<?= set_value('tgl1', date('Y-m-d')) ?>">
								</div>
								<div class="col-md-3 col-lg-3">
									<input type="date" name="tgl2" id="tgl2" class="form-control" value="<?= set_value('tgl2', date('Y-m-d')) ?>">
								</div>
								<div class="col-md-3 col-lg-3">
									<select name="kecamatan" class="form-control select2" required>
										<option value="all">-- Semua Kecamatan --</option>
										<?php
										foreach ($kecamatan as $v) {
											$selected = ($v['id'] == set_value('kecamatan')) ? ' selected="selected"' : "";

											echo '<option value="' . $v['id'] . '" ' . $selected . '>' . $v['nama_kecamatan'] . '</option>';
										}
										?>
									</select>
								</div>
								<div class="col-md-3 col-lg-3">
									<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
									<button type="submit" class="btn btn-success"><i class="fa fa-download"></i> XLS</button>
								</div>
							</div>
							<?= form_close(); ?>
						</div>
						<div class="table-responsive">
							<?php
							if (!is_null($this->session->flashdata('m_error'))) { ?>
								<div class="clearfix"></div>
								<div class="alert alert-danger alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<?php echo $this->session->flashdata('m_error'); ?>
								</div>
							<?php } ?>

							<?php if (!is_null($this->session->flashdata('m_success'))) {
								echo "<div class=\"clearfix\"></div>";
								echo alert_success($this->session->flashdata('m_success'), TRUE);
							} ?>

							<table id="basic-datatables" class="display table table-striped table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Tanggal</th>
										<th>Kecamatan</th>
										<th>Pemilik</th>
										<th>Jenis Hewan</th>
										<th>Jml Hewan</th>
										<th>Gejala</th>
										<th>Diagnosa</th>
										<th>Pengobatan</th>
										<th>Ket</th>
										<th>#</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Tanggal</th>
										<th>Kecamatan</th>
										<th>Pemilik</th>
										<th>Jenis Hewan</th>
										<th>Jml Hewan</th>
										<th>Gejala</th>
										<th>Diagnosa</th>
										<th>Pengobatan</th>
										<th>Ket</th>
										<th>#</th>
									</tr>
								</tfoot>
								<tbody>
									<?php
									$no = 1;
									foreach ($khewan as $k) { ?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo format_tgl($k['tgl'], 'd-m-Y'); ?></td>
											<td><?php echo $k['nama_kecamatan']; ?></td>
											<td><?php echo $k['pemilik']; ?></td>
											<td><?php echo $k['jenis']; ?></td>
											<td><?php echo $k['jml_hewan']; ?></td>
											<td><?php echo $k['gejala']; ?></td>
											<td><?php echo $k['diagnosa']; ?></td>
											<td><?php echo $k['pengobatan']; ?></td>
											<td><?php echo $k['ket']; ?></td>
											<td>
												<div class="form-button-action">
													<a href="<?php echo site_url('dinas/kesehatanhewan/edit/' . $k['id']); ?>">
														<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Ubah">
															<i class="fa fa-edit"></i>
														</button>
													</a>
													<a href="<?php echo site_url('dinas/kesehatanhewan/remove/' . $k['id']); ?>">
														<button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Hapus">
															<i class="fa fa-trash"></i>
														</button>
													</a>
												</div>
											</td>
										</tr>
									<?php $no++;
									} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
