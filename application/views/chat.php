<div class="chat-room">
	<div class="chat-main">
		<div class="chat-row">
			<div class="chat-left">
				<div class="chat-top">
					<h4>Chat</h4>
				</div>

				<div class="chat-left-list">

				</div>
			</div>
			<div class="chat-right">
				<div class="chat-home" <?= (isset($produk['nama_penjual']) ? 'style="display: none;"' : '') ?>>
					<div class="welcome-chat">
						<img src="http://localhost:8084/tani/assets/img/boxbin.png">
						<p class="title">Selamat Datang di fitur Chat</p>
					</div>
				</div>
				<div class="chat-play" <?= (isset($produk['nama_penjual']) ? '' : 'style="display: none;"') ?>>
					<div class="chat-header">
						<div class="chat-header-img">
							<img src="http://nicesnippets.com/demo/man01.png" class="image">
						</div>
						<div class="active_chat">
							<input type="hidden" id="goumai_yonghu" value="">
							<h4><?= (isset($produk['nama_penjual']) ? $produk['nama_penjual'] : '') ?></h4>
							<h6>Online</h6>
						</div>
						<div class="header-icons">
							<a href="tel:<?= (isset($produk['hp']) ? $produk['hp'] : '') ?>"><i class="fa fa-phone"></i></a>
							<div class="chevron-down"><i class="fa fa-chevron-down"></i></div>
						</div>
					</div>
					<div class="chat-page">
						<div class="msg-inbox">
							<div class="chats">
								<div class="msg-page" id="msg-page-scrl">
									<div class="produk_info_chat">
										<div class="chat_produk_img">
											<input type="hidden" id="xiaoshou_yonghu" value="<?= (isset($produk['user_id']) ? $produk['user_id'] : '') ?>">
											<img class="" src="<?= (isset($produk['foto']) ? base_url() . 'uploads/produk/' . $produk['foto'] : '') ?>" alt="<?= (isset($produk['nama_produk']) ? $produk['nama_produk'] : '')  ?>">
										</div>
										<div class="produk_chat_detail">
											<div class="produk_nm_chat">
												<a href="<?= (isset($produk['id']) ? site_url('produk/detail/' . $produk['id'] . '/' . url_title($produk['nama_produk'])) : '#') ?>" id="prdk_nm" target="_blank"><?= (isset($produk['nama_produk']) ? $produk['nama_produk'] : '')  ?></a>
											</div>
											<input type="hidden" id="chanpin" value="<?= (isset($produk['id']) ? format_angka($produk['id']) : '') ?>">
											<div class="produk_hrg_chat">Rp. <?= (isset($produk['harga']) ? format_angka($produk['harga']) : '') ?></div>
										</div>
									</div>

									<div class="chat-chat"></div>
								</div>
							</div>
						</div>

						<div class="msg-bottom">
							<div class="bottom-icon">
								<div class="image-upload">
									<label for="file-input">
										<i class="fa fa-camera"></i>
									</label>

									<input type="file" id="file-input" accept="image/png,image/jpeg,image/jpg">
								</div>
							</div>
							<div class="chat-group">
								<textarea name="input_cht" id="input_cht" class="chat_input" value="" placeholder="write message...." required></textarea>
								<div class="input-icon-append" id="bth-cht-plane">
									<span class="input-group-text group_text_chat">
										<i class="fa fa-paper-plane"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="chat-toggle">
		<i class="fa fa-comments-o"></i> Chats
	</div>
</div>
