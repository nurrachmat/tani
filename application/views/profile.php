<div class="container">
	<div class="contact-form spad">
		<div class="header-profile">
			<h4 class="">Profile Saya</h4>
			<div>Kelola informasi profil Anda untuk mengontrol, melindungi dan mengamankan akun</div>
		</div>
		<hr />

		<?php if (!is_null($this->session->flashdata('m_success'))) {
			echo "<div class=\"clearfix\"></div>";
			echo alert_success($this->session->flashdata('m_success'), TRUE);
		} ?>

		<?= form_open_multipart(); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="blog__details__author">
						<div class="blog__details__author__pic">
							<?php
							if (isset($profile['foto'])) {
								$foto = $profile['foto'];
							} else {
								$foto = 'user.png';
							}
							?>
							<img src="<?= base_url('uploads/foto/' . $foto) ?>" alt="">
						</div>
						<div class="blog__details__author__text">
							<input type="file" name="photo" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<label class="col-md-2">Email <span class="text-danger">*</span></label>
					<div class="col-md-8">
						<input type="text" name="email" class="form-control" placeholder="Email" value="<?= set_value('email', $profile['email']) ?>" required>
						<span class="text-danger"><?= form_error('email') ?></span>
					</div>
				</div>
				<div class="row">
					<label class="col-md-2">Nama <span class="text-danger">*</span></label>
					<div class="col-md-8">
						<input type="text" name="nama" class="form-control" placeholder="Nama" value="<?= set_value('nama', $profile['nama']) ?>" required>
						<span class="text-danger"><?= form_error('nama') ?></span>
					</div>
				</div>

				<div class="row">
					<label class="col-md-2">HP <span class="text-danger">*</span></label>
					<div class="col-md-8">
						<input type="text" name="hp" class="form-control" placeholder="HP" value="<?= set_value('hp', $profile['hp']) ?>" required>
						<span class="text-danger"><?= form_error('hp') ?></span>
					</div>
				</div>

				<div class="row">
					<label class="col-md-2">Password Saat ini </label>
					<div class="col-md-8">
						<input type="password" name="passblm" class="form-control" placeholder="Password Saat ini">
						<span class="text-danger"><?= form_error('passblm') ?></span>
					</div>
				</div>


				<div class="row">
					<label class="col-md-2">Password Baru</label>
					<div class="col-md-8">
						<input type="password" name="passbaru" class="form-control" placeholder="Password Baru">
						<span class="text-danger"><?= form_error('passbaru') ?></span>
					</div>
				</div>

				<div class="row">
					<label class="col-md-2">Konfirmasi Password Baru</label>
					<div class="col-md-8">
						<input type="password" name="konfpassbaru" class="form-control" placeholder="Konfirmasi Password Baru">
						<span class="text-danger"><?= form_error('konfpassbaru') ?></span>
					</div>
				</div>

				<div class="row">
					<label class="col-md-2">Deskripsi <span class="text-danger">*</span></label>
					<div class="col-md-8">
						<textarea class="form-control" name="deskripsi"><?= set_value('deskripsi', $profile['deskripsi']) ?></textarea>
						<span class="text-danger"><?= form_error('deskripsi') ?></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<button type="submit" class="btn btn-block btn-primary"><i class="fa fa-edit"></i> Ubah Profile</button>
		</div>
		<?= form_close() ?>
	</div>
</div>
